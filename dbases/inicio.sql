-- Genero la base de datos aula
CREATE DATABASE aula;

-- Usamos la base de datos aula
USE aula;

-- Creo mi primera tabla
CREATE TABLE aula(
  puesto int,
  nombre varchar(127),
  apellidos varchar(127)
  );

-- Introducir datos
INSERT INTO aula VALUES ('708','Rub�n','Toca');

-- Varios datos en una sola instrucci�n
INSERT INTO aula VALUES 
  ('700','David','Blanco'),
  ('701','Sa�l','S�inz'),
  ('702','Willy',''),
  ('703','Carmen','Palacios');
INSERT INTO aula VALUES
  ('706','Jes�s','Aguirre'),
  ('704','Antonio','Flores'),
  ('705','Fran','Garrido'),
  ('707','Ana','Gaubeca'),
  ('709','Cristian','Delgado'),
  ('710','Luc�a','Quiroga'),
  ('711','Agust�n','Guardo'),
  ('712','Silvia','G�mez'),
  ('713','Anuska','Sanmamed'),
  ('714','Chema','Cagigas');

-- Consultar los datos de la tabla
SELECT * FROM aula ORDER BY puesto DESC;

SELECT * FROM aula WHERE puesto='708';
-- Borramos el resultado de la consulta anterior
DELETE FROM aula WHERE puesto='708';
INSERT INTO aula VALUES ('708','Rub�n','Toca');
SELECT * FROM aula WHERE nombre='Willy';

-- Actualizamos el apellido del registro con nombre Willy
UPDATE aula set apellidos='�lvarez' WHERE nombre='Willy';

-- Gesti�n de tablas
  -- Ver tablas
-- SHOW TABLES;
  -- Borrar tabla
-- DROP TABLE aula;

-- B�squedas
SELECT nombre, apellidos FROM  aula WHERE puesto='710';
