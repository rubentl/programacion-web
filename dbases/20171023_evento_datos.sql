﻿CREATE DATABASE evento;
USE evento;
CREATE TABLE `aficiones` (
  `id_aficion` int(11) NOT NULL,
  `aficion` varchar(64) DEFAULT NULL,
  `descripcion` text,
  `tipo` varchar(31) DEFAULT NULL
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_spanish_ci;
CREATE TABLE `alumnos` (
  `id_alumno` int(11) NOT NULL,
  `nombre` varchar(31) DEFAULT NULL,
  `apellidos` varchar(63) DEFAULT NULL,
  `email` varchar(63) DEFAULT NULL
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_spanish_ci;
CREATE TABLE `disfrutan` (
  `id_alumno` int(11) DEFAULT NULL,
  `id_aficion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `aficiones` (`id_aficion`, `aficion`, `descripcion`, `tipo`) VALUES
(1, 'Tenis', NULL, 'Deporte'),
(2, 'Candy Crash', NULL, NULL),
(3, 'Cine', NULL, 'Arte'),
(4, 'Ping Pong', NULL, 'Deporte'),
(5, 'Idiomas', NULL, 'Cultura'),
(6, 'Viajar', NULL, 'Cultura'),
(7, 'Dibujar', NULL, 'Cultura');
INSERT INTO `alumnos` (`id_alumno`, `nombre`, `apellidos`, `email`) VALUES
(1, 'Antonio', NULL, NULL),
(2, 'Carmen', NULL, NULL),
(3, 'Fran', NULL, NULL),
(4, 'Rubén', NULL, NULL),
(5, 'Lucía', 'Quiroga', NULL),
(6, 'Agustín', NULL, NULL),
(7, 'Anuska', NULL, NULL),
(8, 'Chema', NULL, NULL),
(9, 'Emma', NULL, NULL);
INSERT INTO `disfrutan` (`id_alumno`, `id_aficion`) VALUES
(9, 1),
(9, 2),
(8, 3),
(8, 4),
(7, 5),
(6, 6),
(5, 7);
ALTER TABLE `aficiones` ADD PRIMARY KEY (`id_aficion`);
ALTER TABLE `alumnos` ADD PRIMARY KEY (`id_alumno`);
ALTER TABLE `aficiones`
  MODIFY `id_aficion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
ALTER TABLE `alumnos`
  MODIFY `id_alumno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;