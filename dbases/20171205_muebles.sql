DROP DATABASE muebles;
CREATE DATABASE muebles;
USE muebles;
CREATE TABLE tipos_muebles(
  tipo varchar(15),
  PRIMARY KEY(tipo)
  );
INSERT INTO tipos_muebles (tipo) VALUES ('Silla'),
  ('Aparador'),('Mesa'),('Armario'),('Cama');
CREATE TABLE clientes(
  dni varchar(15),
  nombre varchar(127),
  tlf varchar(31),
  PRIMARY KEY(dni),
  INDEX(nombre),
  INDEX(tlf)
  );
INSERT INTO clientes VALUES 
  ('11111111A','Juan','666777888');
CREATE TABLE muebles(
  id_mueble int AUTO_INCREMENT,
  dni varchar(15) NOT NULL,
  tipo varchar(15) NOT NULL,
  presupuesto float,
  PRIMARY KEY(id_mueble),
  FOREIGN KEY (dni) REFERENCES clientes(dni),
  FOREIGN KEY (tipo) REFERENCES tipos_muebles(tipo)
  );
INSERT INTO muebles (dni,tipo)
  VALUES ('11111111A','Armario');
CREATE TABLE estados(
  estado varchar(15),
  PRIMARY KEY(estado)
  );
INSERT INTO estados (estado) VALUES ('Depositado'),
  ('Reparando'),('Reparado'),('Entregado'),('Pagado');
CREATE TABLE estan(
  fecha datetime,
  estado varchar(15) NOT NULL,
  id_mueble int NOT NULL,
  FOREIGN KEY (estado) REFERENCES estados(estado),
  FOREIGN KEY (id_mueble) REFERENCES muebles(id_mueble)
  );
INSERT INTO estan (fecha,estado,id_mueble)
  VALUES(NOW(),'Depositado',1);
CREATE TABLE tipo_reparaciones(
  tipo varchar(31),
  PRIMARY KEY(tipo)
  );
INSERT INTO tipo_reparaciones VALUES 
  ('Pintura'),('Reconstrucción'),
  ('Tratamiento'),('Tapizado');
CREATE TABLE reparaciones(
  id_reparacion int AUTO_INCREMENT,
  fecha_inicio datetime,
  fecha_terminacion datetime,
  comentarios text,
  tipo_reparacion varchar(31) NOT NULL,
  id_mueble int NOT NULL,
  PRIMARY KEY (id_reparacion),
  FOREIGN KEY(tipo_reparacion) 
    REFERENCES tipo_reparaciones(tipo),
  FOREIGN KEY(id_mueble) REFERENCES muebles(id_mueble),
  INDEX(fecha_inicio),
  INDEX(fecha_terminacion)
  );
INSERT INTO reparaciones 
  (fecha_inicio,tipo_reparacion,id_mueble)
  VALUES (NOW(),'Pintura',1);
CREATE TABLE tipos_materiales(
  tipo varchar(31),
  PRIMARY KEY(tipo)
  );
INSERT INTO tipos_materiales VALUES ('Mano de obra'),
  ('Pintura'),('Cera'),('Clavos'),('Madera');
CREATE TABLE materiales(
  id_material int AUTO_INCREMENT,
  nombre varchar(31),
  tipo_material varchar(31) NOT NULL,
  precio float,
  cantidad float,
  unidades varchar(31),
  fabricante varchar(31),
  referencia varchar(31),
  proveedor varchar(31),
  PRIMARY KEY(id_material),
  UNIQUE(fabricante,referencia),
  FOREIGN KEY(tipo_material) 
    REFERENCES tipos_materiales(tipo)
  );
INSERT INTO materiales 
  (nombre,tipo_material,cantidad,unidades,precio,proveedor,fabricante,referencia) VALUES
  ('Pintura verde','Pintura',5,'Kg',3.5,'Pinturas Paco','Titan','RL56VRD');
INSERT INTO materiales 
  (nombre,tipo_material,cantidad,unidades,precio,proveedor,fabricante,referencia) VALUES
  ('Pintura amarilla','Pintura',5,'Kg',5.5,'Pinturas Paco','Titan','RL56AMR');
INSERT INTO materiales 
  (nombre,tipo_material,cantidad,unidades,precio,proveedor,fabricante,referencia) VALUES
  ('Barniz','Pintura',5,'Kg',8.7,'Pinturas Paco','Titan','RL56BNZ');
INSERT INTO materiales 
  (nombre,tipo_material,unidades,precio,proveedor,fabricante,referencia) VALUES
  ('Anuska','Mano de obra','Horas',30,'Anuska','Anuska','Anuska');
CREATE TABLE utilizan(
  id_reparacion int,
  id_material int,
  cantidad float,
  FOREIGN KEY(id_reparacion) REFERENCES reparaciones(id_reparacion),
  FOREIGN KEY(id_material) REFERENCES materiales(id_material)
  );
INSERT INTO clientes VALUES ('22222222B','Pedro','666888777'),('33333333C','Lucas','666789789');
INSERT INTO muebles (dni,tipo) VALUE ('22222222B','Mesa'),('33333333C','Silla'),('11111111A','Aparador');
INSERT INTO reparaciones (fecha_inicio,fecha_terminacion,comentarios,tipo_reparacion,id_mueble)
  VALUES (NOW()-INTERVAL 3 DAY,NOW(),'Polilla','Tratamiento',2);
INSERT INTO reparaciones (fecha_inicio,fecha_terminacion,comentarios,tipo_reparacion,id_mueble)
  VALUES (NOW()-INTERVAL 4 DAY,NOW(),'Humedad','Tratamiento',3);
INSERT INTO reparaciones (fecha_inicio,comentarios,tipo_reparacion,id_mueble)
  VALUES (NOW()-INTERVAL 4 DAY,'Amarilla','Pintura',3);
INSERT INTO reparaciones (comentarios,tipo_reparacion,id_mueble)
  VALUES ('Barnizado','Pintura',3);
INSERT INTO utilizan VALUES (1,1,3),(1,4,2),(2,4,2),(3,4,2),(4,4,1),(5,4,.5);
INSERT INTO utilizan VALUES (5,3,.2);

SELECT dni, nombre FROM clientes WHERE clientes.nombre='Juan';
SELECT * FROM (
  SELECT dni, nombre FROM clientes WHERE clientes.nombre='Juan'
  ) c1 JOIN muebles USING(dni);
SELECT * FROM (
  SELECT * FROM (
  SELECT dni, nombre FROM clientes WHERE clientes.nombre='Juan'
  ) c1 JOIN muebles USING(dni)
  ) c2 JOIN reparaciones USING(id_mueble);

SELECT * FROM(
  SELECT * FROM (
  SELECT * FROM (
  SELECT dni, nombre FROM clientes WHERE clientes.nombre='Juan'
  ) c1 JOIN muebles USING(dni)
  ) c2 LEFT JOIN reparaciones USING(id_mueble)
  ) c3 LEFT JOIN utilizan USING(id_reparacion);

SELECT c4.nombre, c4.tipo, materiales.tipo_material, materiales.precio, materiales.cantidad, materiales.precio*c4.cantidad AS importe
  FROM (
  SELECT * FROM(
  SELECT * FROM (
  SELECT * FROM (
  SELECT dni, nombre FROM clientes WHERE clientes.nombre='Juan'
  ) c1 JOIN muebles USING(dni)
  ) c2 LEFT JOIN reparaciones USING(id_mueble)
  ) c3 LEFT JOIN utilizan USING(id_reparacion)
  ) c4 LEFT JOIN materiales USING(id_material);

SELECT SUM(c5.importe) precio_total FROM (
  SELECT c4.nombre, c4.tipo, materiales.tipo_material, materiales.precio, materiales.cantidad, materiales.precio*c4.cantidad AS importe
  FROM (
  SELECT * FROM(
  SELECT * FROM (
  SELECT * FROM (
  SELECT dni, nombre FROM clientes WHERE clientes.nombre='Juan'
  ) c1 JOIN muebles USING(dni)
  ) c2 LEFT JOIN reparaciones USING(id_mueble)
  ) c3 LEFT JOIN utilizan USING(id_reparacion)
  ) c4 LEFT JOIN materiales USING(id_material)
  ) c5;

SELECT muebles.id_mueble FROM muebles WHERE muebles.dni = (
  SELECT dni FROM clientes WHERE clientes.nombre='Juan'
  );
SELECT * FROM reparaciones WHERE reparaciones.id_mueble IN(
  SELECT muebles.id_mueble FROM muebles WHERE muebles.dni = (
  SELECT dni FROM clientes WHERE clientes.nombre='Juan'
  )
  );

SELECT * FROM materiales WHERE materiales.nombre='Barniz';

SELECT * FROM (
  SELECT * FROM materiales WHERE materiales.nombre='Barniz'
  ) c1 JOIN utilizan USING(id_material);

SELECT * FROM (
  SELECT * FROM (
  SELECT * FROM materiales WHERE materiales.nombre='Barniz'
  ) c1 JOIN utilizan USING(id_material)
  ) c2 JOIN reparaciones USING (id_reparacion);

SELECT * FROM (
  SELECT * FROM (
  SELECT * FROM (
  SELECT materiales.nombre, materiales.id_material FROM materiales WHERE materiales.nombre='Barniz'
  ) c1 JOIN utilizan USING(id_material)
  ) c2 JOIN reparaciones USING (id_reparacion)
  ) c3 JOIN muebles USING(id_mueble);

SELECT * FROM (
  SELECT * FROM (
  SELECT * FROM (
  SELECT utilizan.id_reparacion, c1.id_material, c1.nombre FROM (
  SELECT materiales.nombre, materiales.id_material FROM materiales WHERE materiales.nombre='Barniz'
  ) c1 JOIN utilizan USING(id_material)
  ) c2 JOIN reparaciones USING (id_reparacion)
  ) c3 JOIN muebles USING(id_mueble)
  ) c4 JOIN clientes USING(dni);