﻿<?php
if(1){	// advertencia
	/*
	
	
	
	
	
	
	
	
	
	
	
	A D V E R T E N C I A :
	
	
	SI AÚN NO HAS ESTUDIDO PHP, ESTE CÓDIGO PUEDE CONFUNDIRTE
	
	
	Entretanto, utilízalo únicamente llamándolo desde un formulario en HTML
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	*/
}
if(1){	// cabecera html
	echo '<!DOCTYPE html>
		<html lang="es">
		<head>
			<meta charset="UTF-8">
			<title>Base de datos</title>
			<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
			<style type="text/css">
				body{
					font-family:Roboto,Open Sans,Verdana,Courier New;
					text-align: center;
				}
				table,form{
					margin:20px auto;
					border: solid 1px;
				}
				td{
					text-align: left;
				}
				.titulos{
					background: lightgray;
					height: 2.5em;
				}
				error{
					display: block;
					background: pink;
					border-radius: 10px;
					padding: 10px;
				}
				pre{
					background: #eaeaea;
					border-radius:10px;
					padding: 10px;
				}
			</style>
		</head>
		<body>
	';
}
if($_POST){	// conexión
	// conexión al servidor
	$conn=mysqli_connect('localhost','root','');
	// acondicionamiento de los datos recibidos
	if(!$_POST['basedatos']) echo '<error>No ha sido seleccionada ninguna base de datos</error>';
	$basedatos=mysqli_real_escape_string($conn,$_POST['basedatos']);
	// conexión a la base de datos
	mysqli_query($conn,"USE ".$basedatos);
	echo  mysqli_error($conn);
	mysqli_query($conn,"SET NAMES 'utf8'");
	mysqli_query($conn,"SET CHARACTER SET 'utf8'"); 
}
if($conn&&$_POST['consulta']){
	$sql=mb_convert_encoding($_POST['consulta'],'UTF-8');
	if($_POST['debug']) echo '<div style="text-align:left"><pre>'.$sql.'</pre></div>';
	consulta($conn,$sql);
}
if($conn&&!$_POST['consulta']){	// tabla
	// acondicionamiento de los datos recibidos
	$tabla=mysqli_real_escape_string($conn,$_POST['tabla']);
	if($tabla){
		// campos de la tabla
		$sql="SHOW COLUMNS FROM ".$tabla;
		if($rs=mysqli_query($conn,$sql))
			while($result=mysqli_fetch_assoc($rs)) $campos[$result['Field']]=1;
	}
	else echo '<error>No ha sido seleccionada ninguna tabla</error>';
}
if($conn&&$_POST['tabla1']){
	echo '<form method="post">';
	// primer desplegable
	echo '<select name="'.$_POST['id1'].'">';
	$sql="SELECT * FROM ".$_POST['tabla1'];
	$rs=mysqli_query($conn,$sql);
	while($result=mysqli_fetch_assoc($rs))
		echo '<option value="'.$result[$_POST['id1']].'">'.$result[$_POST['campo1']].'</option>';
	echo '</select>';
	// segundo desplegable
	echo '<select name="'.$_POST['id2'].'">';
	$sql="SELECT * FROM ".$_POST['tabla2'];
	$rs=mysqli_query($conn,$sql);
	while($result=mysqli_fetch_assoc($rs))
		echo '<option value="'.$result[$_POST['id2']].'">'.$result[$_POST['campo2']].'</option>'."\n";
	echo '</select>';
	// fin formulario
	echo '<input type="hidden" 	name="basedatos"	value="'.$_POST['basedatos'].'">';
	echo '<input type="hidden" 	name="tabla" 		value="'.$_POST['tabla'].'">';
	echo '<button>Enviar</button>';
	echo '</form>';
}
if($tabla&&!$_POST['tabla1']){
	// inserción de los datos recibidos
	$sql="INSERT INTO ".$tabla;
	$sql.=" (";
	foreach($_POST as $campo=>$dato) if($campos[$campo]) 
		$sql.=mysqli_real_escape_string($conn,$campo).",";
	$sql.=") VALUES (";
	foreach($_POST as $campo=>$dato) if($campos[$campo]) 
		$sql.="'".mysqli_real_escape_string($conn,$dato)."',";
	$sql.=")";
	$sql=str_replace(',)',')',$sql);
	mysqli_query($conn,$sql);
	// listado de los registros de la tabla
	consulta($conn,"SELECT * FROM ".$tabla);
}
if($conn){	// desconexión
	mysqli_close($conn);	
} else echo '<error>No se ha establecido conexión con la base de datos</error>';
if(1){
	echo '
		</body>
		</html>
	';
}
function consulta($conn,$sql){
	echo '<table>';
	if($rs=mysqli_query($conn,$sql)){
		while($result=mysqli_fetch_assoc($rs)){
			if(!($i++%10)){
				echo '<tr class="titulos">';
				foreach($result as $campo=>$dato) echo '<td>'.$campo.'</td>';
				echo '</tr>';
			}
			echo '<tr>';
			foreach($result as $campo=>$dato) echo '<td>'.$dato.'</td>';
			echo '</tr>';
		}
	} else echo '<error>SQL incorrecto: '. mysqli_error($conn).'</error>';
	echo '</table>';	
}
?>