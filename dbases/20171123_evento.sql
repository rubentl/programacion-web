-- 01. �Cu�ntos alumnos han informado de sus aficiones?
--  C1 = p id_alumno (disfrutan)
--  R1 = gamma count(*) -> cuantos (C1) contar con * cuenta tambi�n nulos.

-- 02. �Cu�ntos lenguajes de programaci�n hemos registrado?
--  R2 = gamma count(*) -> cuantos (lenguajes)

-- 03. �Cu�ntos alumnos a�n no han informado de sus aficiones?
--  C3 = gamma count(*) -> cuantos (alumnos)
--  C3 - R1

-- 04. �Qu� alumnos han informado sobre los lenguajes que conocen?
--  R4 = pi alumnos.nombre (alumnos natural join conocen) poco eficiente
--  C4 = pi id_alumno, nombre (alumnos)
--  C41= pi id_alumno (conocen)
--  R4 = pi alumnos.nombre (C4 natural join C41) M�s eficiente al trabajar ya con proyecciones

-- 05. �Cu�ntos lenguajes no conoce ning�n alumno?
--  C5 = pi id_lenguaje (conocen)
--  C51 = pi id_lenguaje (lenguajes)
--  C52 = C51 - C5
--  R5 = gamma count(*) -> cuantos (C52)

-- 06. �Qu� lenguajes conocen los alumnos?
--  C6 = pi id_lenguaje, lenguaje (lenguajes)
--  C61 = pi id_lenguaje (conocen)
--  R6 = pi lenguajes.lenguaje (C6 natural join C61)

-- 07. �Qu� lenguajes desconocen los alumnos?
-- C7 = pi id_lenguaje, lenguaje (lenguajes)
-- C71 = pi id_lenguaje, lenguaje (conocen natural join lenguajes)
-- R7 = C7 - C71

-- 08. �Qu� alumnos saben programaci�n orientada a objetos?
--  C8 = pi id_lenguaje sigma orientado_objetos = 1 (lenguajes)
--  C81 = pi id_alumno (C8 natural join conocen)
--  R8 = pi nombre (C81 natural join alumnos)

-- 09. �Qu� lenguajes de programaci�n orientada a objetos conocen los alumnos?
--  C9 = pi id_lenguaje, lenguaje sigma orientado_objetos = 1 (lenguajes)
--  R9 = pi lenguaje (C9 natural join conocen)

-- 10. �Cu�ntos alumnos comparten alguna afici�n?
--  C10 = gamma id_aficion; count(id_aficion) -> aficion (disfrutan) 
--  C101 = pi id_aficion, id_alumno sigma aficion > 1 (C10 natural join disfrutan)
--  C102 = pi nombre (alumnos natural join C101)
--  R10 = gamma count(*) ->n_alumnos (C102)

-- 11. �Qu� alumnos comparten alguna afici�n?
--  R11 = pi nombre (alumnos natural join C101)

-- 12. �Cu�les son las aficiones compartidas?
--  C12 = gamma id_aficion, aficion; count(id_aficion) -> cuantas (aficiones natural join disfrutan)
--  R12 = pi aficion sigma cuantas > 1 (C12)
SELECT aficiones.aficion FROM (
  SELECT id_aficion, COUNT(id_aficion) aficion FROM disfrutan GROUP BY id_aficion HAVING aficion >1
  ) c1 JOIN aficiones USING (id_aficion);

-- 13. �Cu�ntos alumnos comparten cada afici�n?
--  C13 = pi id_aficion, id_alumno sigma cuantas > 1 (C12 natural join disfrutan)
--  R13 = gamma aficion;count(id_alumno) -> n_alumnos (C13 natural join aficiones)

-- 14. �Qu� alumnos comparten cada una de las aficiones compartidas?
--  R14 = pi aficion, nombre (C13 natural join aficiones natural join alumnos)

-- 15. �Qu� aficiones no son compartidas?
--  C15 = pi id_aficion sigma aficion = 1 (C10 natural join disfrutan)
--  R15 = pi aficion (C15 natural join aficiones)
SELECT aficiones.aficion FROM (
  SELECT id_aficion, COUNT(id_aficion) aficion FROM disfrutan GROUP BY id_aficion HAVING aficion = 1
  ) c1 JOIN aficiones USING (id_aficion);


-- 16. �Qu� aficiones no tiene nadie?
-- C16 = pi id_aficion sigma cuantas > 0 gamma id_aficion; count(id_aficion) -> cuantas (disfrutan)
-- C161 = pi id_aficion aficiones
-- C162 = C161 - C16
-- R16 = pi aficion (aficiones natural join C162


-- 17. �Qu� afici�n comparten todos los alumnos? ��DIVISI�N!!
-- C171 = gamma count(*) ->n_alumnos (alumnos) -- n�mero de alumnos
-- C17 = pi aficion sigma n_aficiones=9 gamma id_aficion, aficion; count(id_aficion)->n_aficiones (disfrutan natural join aficiones)
SELECT aficiones.id_aficion,aficion, COUNT(*) n_aficiones 
  FROM disfrutan NATURAL JOIN aficiones 
  group BY aficiones.id_aficion 
  HAVING n_aficiones = (SELECT COUNT(*) FROM alumnos);

-- 18. Realiza una tabla donde figuren TODOS los lenguajes de programaci�n y cu�ntos alumnos los conocen
-- R18 = gamma lenguaje; count(id_alumno)->n_alumnos (lenguajes left join conocen)

-- 19. Realiza una tabla donde figuren todas las aficiones y cu�ntos alumnos las disfrutan
-- R19 = gamma aficion; count(id_alumno)->n_alumnos (aficiones left join disfrutan)

-- 20. �Cu�ntos lenguajes y aficiones conoce y disfruta Rub�n?
-- C20 = pi id_alumno sigma nombre = 'Rub�n' (alumnos)
-- C201 = pi lenguaje sigma id_alumno = 6 (lenguajes natural join conocen) -- 6 es C20
-- C202 = pi aficion sigma id_alumno = 6 (aficiones natural join disfrutan)
-- R20 = gamma count(*)->lenguajes_aficiones (C201 union C202)

-- 21. �Cu�l es la afici�n m�s popular?
-- C21 = gamma id_aficion; count(id_aficion)->n_aficiones (disfrutan)
-- C211 = gamma max(n_aficiones)->mas_popular (C21)
-- C212 = pi id_aficion sigma n_aficiones = 5 (C21) -- 5 es C211
-- R21 = pi aficion sigma id_aficion = 12 (aficiones) -- 12 es C21

