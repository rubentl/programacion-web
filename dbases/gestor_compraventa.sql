DROP DATABASE if EXISTS gestor_compraventa;
CREATE DATABASE gestor_compraventa;
USE gestor_compraventa;
CREATE TABLE roles(
  rol varchar(15),
  PRIMARY KEY(rol)
  );
INSERT INTO roles VALUES 
  ('admin'),('user'),('guest');
CREATE TABLE usuarios(
  id_usuario int AUTO_INCREMENT,
  login varchar(127),
  passwd varchar(255),
  email varchar(127),
  nombre varchar(127),
  rol varchar(15),
  activo bool,
  PRIMARY KEY(id_usuario),
  FOREIGN KEY(rol) REFERENCES roles(rol),
  UNIQUE(login)
  );
INSERT INTO usuarios 
  (login,passwd,email,nombre,rol)
  VALUES 
  ('david',MD5('hola'),
  'david@alpeformacion.es',
  'David','admin' );
CREATE TABLE mensajes(
  id_mensaje int AUTO_INCREMENT,
  id_origen int,
  id_destino int,
  contenido text,
  fecha datetime,
  PRIMARY KEY(id_mensaje),
  FOREIGN KEY(id_origen) REFERENCES usuarios(id_usuario),
  FOREIGN KEY(id_destino) REFERENCES usuarios(id_usuario)
  );
CREATE TABLE categorias(
  categoria varchar(127),
  PRIMARY KEY(categoria)
  );
INSERT INTO categorias VALUES 
  ('Auto'),('Hogar'),('Jard�n'),
  ('Mascotas'),('Deportes');
CREATE TABLE productos(
  id_producto int AUTO_INCREMENT,
  id_vendedor int,
  id_comprador int,
  producto varchar(127),
  fecha_oferta datetime,
  fecha_compra datetime,
  precio_oferta float,
  precio_compra float,
  foto varchar(255),
  categoria varchar(127),
  descripcion text,
  localizacion varchar(127),
  PRIMARY KEY(id_producto),
  FOREIGN KEY(categoria) REFERENCES categorias(categoria),
  FOREIGN KEY(id_vendedor) REFERENCES usuarios(id_usuario),
  FOREIGN KEY(id_comprador) REFERENCES usuarios(id_usuario)
 );