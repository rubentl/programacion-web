CREATE DATABASE _prb;
USE _prb;
CREATE TABLE panel (
  n_datos int
  );
CREATE TABLE datos(
  INSTANCE datetime
  );

CREATE EVENT registrador
  ON SCHEDULE EVERY 15 SECOND 
  DO INSERT INTO datos VALUES (NOW()); 

SET GLOBAL event_scheduler = ON;

CREATE TRIGGER contador
  AFTER INSERT ON datos 
  FOR EACH ROW 
  BEGIN 
    UPDATE panel set panel.n_datos = (SELECT COUNT(*) FROM datos);
  END;

ALTER TABLE datos ADD COLUMN edad int;

DROP EVENT registrador;

UPDATE datos set edad=30 WHERE datos.INSTANCE = '2017-12-26 18:29:58';

CREATE TRIGGER comprobador
  BEFORE UPDATE ON datos
  FOR EACH ROW
  BEGIN
    IF (NEW.edad - OLD.edad > 1) THEN SET new.edad = old.edad;
      END IF ;
    if (new.edad < 18) then set new.edad = null; end if;
  END;

CREATE TABLE usuarios(
  nombre varchar(62),
  email varchar(32)
  );

CREATE TRIGGER email
  BEFORE INSERT ON usuarios
  FOR EACH ROW
  BEGIN
    if LOCATE('@', new.email) = 0 THEN 
      SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = '�Sab�as que el email debe tener una @?';
    end IF;
  END;

INSERT INTO usuarios VALUES ('pepe', 'opmail.com');
SELECT * FROM usuarios;

SELECT * FROM aficiones;
ALTER TABLE aficiones ADD COLUMN n_aficionados int;
ALTER TABLE alumnos ADD COLUMN n_aficiones int;
SELECT * FROM alumnos;

SELECT id_alumno, COUNT(*) n FROM disfrutan GROUP BY id_alumno;
SELECT id_aficion, COUNT(*) n FROM disfrutan GROUP BY id_aficion;

-- inicializaci�n del sistema con un cursor.
DROP PROCEDURE IF EXISTS aforador;
CREATE PROCEDURE aforador()
  BEGIN
    DECLARE n,id int;
    DECLARE cur1 CURSOR FOR SELECT id_alumno FROM alumnos;
    OPEN cur1;

    SELECT COUNT(*) INTO n FROM alumnos;
    
    WHILE n DO 
      SET n=n-1;
      FETCH cur1 INTO id;
      UPDATE alumnos set n_aficiones=(
        SELECT COUNT(*) FROM disfrutan WHERE id_alumno=id)
        WHERE id_alumno=id;
    END WHILE ;
    CLOSE cur1;
  END;

-- comprobaci�n del funcionamiento
CALL aforador();
SELECT * FROM alumnos;

-- actualizaci�n de datos anterior en una �nica consulta
UPDATE alumnos, (SELECT id_alumno, COUNT(*) n FROM disfrutan GROUP BY id_alumno) c1
  SET alumnos.n_aficiones=c1.n WHERE alumnos.id_alumno = c1.id_alumno;

-- nuevo procedimiento con esta subconsulta
CREATE PROCEDURE aforador_optimizado()
  BEGIN
    UPDATE alumnos, (SELECT id_alumno, COUNT(*) n FROM disfrutan GROUP BY id_alumno) c1
    SET alumnos.n_aficiones=c1.n WHERE alumnos.id_alumno = c1.id_alumno;
  END;

CALL aforador_optimizado();

-- procedimiento para saber el n�mero de alumnos por afici�n
SELECT id_aficion, COUNT(*) n FROM disfrutan GROUP BY id_aficion;

CREATE PROCEDURE alumnos_por_aficion()
  BEGIN
    DECLARE n,id int;
    DECLARE cur2 CURSOR FOR SELECT id_aficion FROM aficiones;
    OPEN cur2;

    SELECT COUNT(*) INTO n FROM aficiones;

    WHILE n DO
      SET n=n-1;
      FETCH cur2 INTO id;
      UPDATE aficiones set n_aficionados=(
        SELECT COUNT(*) FROM disfrutan WHERE id_aficion=id
        ) WHERE id_aficion=id;
    END WHILE;
    CLOSE cur2;
  END;

SELECT * FROM aficiones;
CALL alumnos_por_aficion();

-- la misma funcionalidad pero con una consulta de update
UPDATE aficiones set n_aficionados=NULL;

CREATE PROCEDURE alumnos_por_aficion_optimizado()
  BEGIN
    UPDATE aficiones, (
      SELECT id_aficion, COUNT(*) n FROM disfrutan GROUP BY id_aficion
      ) c1 set aficiones.n_aficionados=c1.n 
      WHERE aficiones.id_aficion=c1.id_aficion;
  END;
CALL alumnos_por_aficion_optimizado();

-- disparador para actualizar el dato en cadad nueva inserci�n
DROP TRIGGER aforador;
CREATE TRIGGER aforador
  AFTER INSERT 
  ON disfrutan
  FOR EACH ROW 
  BEGIN 
    DECLARE idAlumno,idAficion int;
    SET idAlumno = NEW.id_alumno;
    SET idAficion = NEW.id_aficion;
    UPDATE alumnos set n_aficiones=(
        SELECT COUNT(*) FROM disfrutan WHERE id_alumno=idAlumno)
        WHERE id_alumno=idAlumno;
    UPDATE aficiones set n_aficionados=(
        SELECT COUNT(*) FROM disfrutan WHERE id_aficion=idAficion
        ) WHERE id_aficion=idAficion;
  END;

DROP TRIGGER aforador_update;
CREATE TRIGGER aforador_update
  AFTER UPDATE ON disfrutan for each row 
  begin
    CALL aforador();
    CALL alumnos_por_aficion_optimizado();
  END;

DROP TRIGGER aforador_delete;
CREATE TRIGGER aforador_delete
  AFTER DELETE ON disfrutan for each row 
  begin
   CALL aforador();
   CALL alumnos_por_aficion_optimizado();
  end;


-- comprobar el funcionamiento
INSERT INTO disfrutan VALUES (4,3);
DELETE FROM disfrutan WHERE id_alumno=4 AND id_aficion=3;
SELECT * FROM alumnos;
SELECT * FROM aficiones;

-- para dejar constancia de los cambios realizados en disfrutan
CREATE TABLE trz_disfrutan(
  evento varchar(15),
  id_alumno int,
  id_aficion int,
  INSTANCE timestamp
  );
CREATE TRIGGER aforador_update_before
  BEFORE UPDATE 
  ON disfrutan
  FOR EACH ROW 
  INSERT INTO trz_disfrutan(evento,id_alumno,id_aficion)
    VALUES ('UPDATE', OLD.id_alumno, OLD.id_aficion);
CREATE TRIGGER aforador_delete_before
  BEFORE DELETE 
  ON disfrutan
  FOR EACH ROW 
  INSERT INTO trz_disfrutan(evento,id_alumno,id_aficion)
    VALUES ('DELETE', OLD.id_alumno, OLD.id_aficion);
CREATE TRIGGER aforador_insert_before
  BEFORE insert 
  ON disfrutan
  FOR EACH ROW 
  INSERT INTO trz_disfrutan(evento,id_alumno,id_aficion)
    VALUES ('INSERT', NEW.id_alumno, NEW.id_aficion);
SELECT * FROM trz_disfrutan;