CREATE DATABASE muebles;
USE muebles;
CREATE TABLE muebles(
  id_mueble int AUTO_INCREMENT ,
  estado varchar(15),

  PRIMARY KEY (id_mueble),
  FOREIGN KEY(estado) REFERENCES estados(estado)
  );
CREATE TABLE estados(
  estado varchar(15),
  PRIMARY KEY (estado));

INSERT INTO estados (estados.estado) VALUES ('En cola'),('En reparación'),('Reparado');