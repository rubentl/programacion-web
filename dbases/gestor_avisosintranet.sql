DROP DATABASE IF EXISTS gestor_avisosintranet;
CREATE DATABASE gestor_avisosintranet;
USE gestor_avisosintranet;
CREATE TABLE roles(
  rol varchar(15),
  PRIMARY KEY(rol)
  );
INSERT INTO roles VALUES 
  ('admin'),('user');
CREATE TABLE usuarios(
  id_usuario int AUTO_INCREMENT,
  login varchar(127),
  passwd varchar(255),
  email varchar(127),
  nombre varchar(127),
  rol varchar(15),
  activo bool,
  PRIMARY KEY(id_usuario),
  FOREIGN KEY(rol) REFERENCES roles(rol),
  UNIQUE(login)
  );
INSERT INTO usuarios 
  (login,passwd,email,nombre,rol)
  VALUES 
  ('david',MD5('hola'),
  'david@alpeformacion.es',
  'David','admin' );
CREATE TABLE prioridades(
  prioridad varchar(127),
  PRIMARY KEY(prioridad)
  );
INSERT INTO prioridades VALUES
  ('Urgente'),('Normal'),('Baja');
CREATE TABLE mensajes(
  id_mensaje int AUTO_INCREMENT,
  id_origen int,
  contenido text,
  fecha datetime,
  prioridad varchar(127),
  PRIMARY KEY(id_mensaje),
  FOREIGN KEY(id_origen) REFERENCES usuarios(id_usuario),
  FOREIGN KEY(prioridad) REFERENCES prioridades(prioridad)
  );
CREATE TABLE reciben(
  id_usuario int,
  id_mensaje int,
  fecha_recibido datetime,
  fecha_leido datetime,
  PRIMARY KEY(id_usuario,id_mensaje),
  FOREIGN KEY(id_usuario) REFERENCES usuarios(id_usuario),
  FOREIGN KEY(id_mensaje) REFERENCES mensajes(id_mensaje)
  );
CREATE TABLE grupos(
  id_grupo int AUTO_INCREMENT,
  grupo varchar(127),
  UNIQUE(grupo),
  PRIMARY KEY (id_grupo)
  );
CREATE TABLE participan(
  id_usuario int,
  id_grupo int,
  PRIMARY KEY(id_usuario,id_grupo),
  FOREIGN KEY(id_grupo) REFERENCES grupos(id_grupo),
  FOREIGN KEY(id_usuario) REFERENCES usuarios(id_usuario)
  );