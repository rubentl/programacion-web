CREATE DATABASE if NOT EXISTS evento;

USE evento;

CREATE TABLE alumnos (
  id_alumno int AUTO_INCREMENT,
  nombre varchar(31) NOT null,
  apellidos varchar(63),
  email varchar(63) NOT NULL UNIQUE,
  PRIMARY KEY (id_alumno),
  INDEX (nombre)
  );

SELECT * FROM  alumnos;
DROP TABLE alumnos;
TRUNCATE TABLE alumnos;

INSERT INTO alumnos (nombre, email) VALUES 
  ('Carmen','carmen@gmail.com'),('antonio', 'antonio@gmail.com'); 

INSERT INTO alumnos (alumnos.nombre, alumnos.apellidos) VALUES 
  ('Luc�a','Quiroga');

DROP TABLE aficiones;
CREATE TABLE aficiones (
  id_aficion int AUTO_INCREMENT ,
  aficion varchar(64) NOT NULL UNIQUE ,
  descripcion text,
  tipo varchar(31),
  PRIMARY KEY (id_aficion)
  );

SELECT * FROM aficiones;

INSERT INTO aficiones (aficion,aficiones.tipo) VALUES
  ('Tenis','deporte'),('Candy Crash','Ocio'),('Pin Pong','Deporte');

DROP TABLE disfrutan ;
CREATE TABLE disfrutan (
    id_alumno int,
    id_aficion int,
  FOREIGN KEY (id_alumno ) REFERENCES alumnos(id_alumno),
  FOREIGN KEY (id_aficion ) REFERENCES aficiones(id_aficion),
  UNIQUE(id_alumno, id_aficion)
  );

SELECT id_alumno FROM alumnos WHERE alumnos.nombre='Emma';

INSERT INTO disfrutan (disfrutan.id_alumno,disfrutan.id_aficion) VALUES
  (3,4);

INSERT INTO aficiones (aficiones.aficion) VALUES ('Candy Crash');

INSERT INTO disfrutan (disfrutan.id_alumno,disfrutan.id_aficion) VALUES
  (
    (SELECT id_alumno FROM alumnos WHERE alumnos.nombre='Emma')
  ,
    (SELECT id_aficion FROM aficiones WHERE aficiones.aficion='Candy Crash')
  );

SELECT * FROM disfrutan;

INSERT INTO aficiones (aficiones.aficion, tipo) VALUES ('Cine', 'Arte');

INSERT INTO disfrutan (disfrutan.id_alumno,disfrutan.id_aficion) VALUES
  (
    (SELECT id_alumno FROM alumnos WHERE alumnos.nombre='Chema')
  ,
    (SELECT id_aficion FROM aficiones WHERE aficiones.aficion='Cine')
  );

INSERT INTO aficiones (aficiones.aficion, tipo) VALUES ('Pin Pong', 'Deporte');

INSERT INTO disfrutan (disfrutan.id_alumno,disfrutan.id_aficion) VALUES
  (
    (SELECT id_alumno FROM alumnos WHERE alumnos.nombre='Chema')
  ,
    (SELECT id_aficion FROM aficiones WHERE aficiones.aficion='Pin Pong')
  );

INSERT INTO aficiones (aficiones.aficion, tipo) VALUES ('Idiomas', 'Cultura');

INSERT INTO disfrutan (disfrutan.id_alumno,disfrutan.id_aficion) VALUES
  (
    (SELECT id_alumno FROM alumnos WHERE alumnos.nombre='Anuska')
  ,
    (SELECT id_aficion FROM aficiones WHERE aficiones.aficion='Idiomas')
  );
INSERT INTO aficiones (aficiones.aficion, tipo) VALUES ('Viajar', 'Cultura');

INSERT INTO disfrutan (disfrutan.id_alumno,disfrutan.id_aficion) VALUES
  (
    (SELECT id_alumno FROM alumnos WHERE alumnos.nombre='Agust�n')
  ,
    (SELECT id_aficion FROM aficiones WHERE aficiones.aficion='Viajar')
  );

INSERT INTO aficiones (aficiones.aficion, tipo) VALUES ('Dibujar', 'Cultura');

INSERT INTO disfrutan (disfrutan.id_alumno,disfrutan.id_aficion) VALUES
  (
    (SELECT id_alumno FROM alumnos WHERE alumnos.nombre='Luc�a')
  ,
    (SELECT id_aficion FROM aficiones WHERE aficiones.aficion='Dibujar')
  );
SELECT * FROM disfrutan JOIN alumnos USING (id_alumno);

SELECT c1.nombre, aficion FROM (
  SELECT * FROM disfrutan JOIN alumnos USING (id_alumno)
  )c1 JOIN aficiones USING(id_aficion);

SELECT nombre, COUNT(*) cuantas FROM (
  SELECT c1.nombre, aficion FROM (
  SELECT * FROM disfrutan JOIN alumnos USING (id_alumno)
  )c1 JOIN aficiones USING(id_aficion) )c2 GROUP BY nombre ORDER BY cuantas DESC;