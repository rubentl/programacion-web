USE aula;


-- //// CONSULTAS DE TOTALES
-- Poblaci�n del pa�s
SELECT FORMAT(SUM(poblacion),0) FROM provincias;

-- Superficie del pa�s
SELECT FORMAT(SUM(superficie),0) FROM provincias;

-- Densidad de poblaci�n del pa�s
SELECT SUM(poblacion) FROM provincias;
SELECT SUM(superficie) FROM provincias;
SELECT (
    SELECT SUM(poblacion) FROM provincias
  )/(
    SELECT SUM(superficie) FROM provincias
  );
SELECT SUM(poblacion)/SUM(superficie) FROM provincias;

-- Densidad de poblaci�n de las provincias de m�s a menos
SELECT provincia,poblacion/superficie densidad
  FROM provincias
  ORDER BY densidad DESC;

-- Poblaci�n media de las provincias
SELECT SUM(poblacion)/COUNT(*) FROM provincias;
SELECT FORMAT(AVG(poblacion),0) FROM provincias;

-- Poblaci�n media de provincias que empiezan por A
SELECT AVG(poblacion) 
  FROM provincias WHERE provincia LIKE 'a%';

-- Poblaci�n media de las provincias entre 2 y 3 millones de habitantes
SELECT * FROM provincias
  WHERE poblacion BETWEEN 2e6 AND 3e6;
SELECT * FROM provincias
  WHERE poblacion>=2e6 AND poblacion<=3e6;
SELECT AVG(poblacion) FROM provincias
  WHERE poblacion BETWEEN 2e6 AND 3e6;

-- Poblaci�n media de las provincias entre 1 y 2 millones de habitantes
SELECT AVG(poblacion) FROM provincias
  WHERE poblacion BETWEEN 1e6 AND 2e6;

-- Listado de provincias por superficie descendente
SELECT * FROM provincias
  ORDER BY poblacion DESC;

-- Ranking de densidad de poblaci�n de las provincias
SELECT provincia,ROUND(poblacion/superficie,0) densidad
  FROM provincias
  ORDER BY densidad DESC;

-- Contar provincias por millones de habitantes
SELECT 4.8;
SELECT ROUND(4.4);
SELECT FLOOR(-4.8);
SELECT CEIL(-4.8);
SELECT TRUNCATE(-4.8,0);
SELECT provincia,FLOOR(poblacion/1e6) FROM provincias;
SELECT millones,COUNT(*) FROM (
    SELECT provincia,FLOOR(poblacion/1e6) millones
      FROM provincias  
  ) c1 GROUP BY c1.millones;
-- Ranking de provincias m�s pobladas
SELECT * FROM provincias
  ORDER BY poblacion DESC;
 
 
-- ****************************************************************************************
-- //// CONSULTAS DE TOTALES Y SUBCONSULTAS
-- Provincia m�s poblada
-- c1
SELECT MAX(poblacion) FROM provincias;
SELECT provincia FROM provincias WHERE poblacion=(
    SELECT MAX(poblacion) FROM provincias  
  );

-- Provincia m�s extensa
SELECT provincia FROM provincias WHERE provincias.superficie = 
  (SELECT MAX(provincias.superficie) FROM provincias);

-- Porcentaje de la poblaci�n de cada provincia 
-- de mayor a menor
-- c1
SELECT SUM(poblacion) FROM provincias;
-- resultado
SELECT provincia,poblacion*100/(
    SELECT SUM(poblacion) FROM provincias  
  ) porcentaje FROM provincias
  ORDER BY porcentaje DESC;

-- Comprobar si el porcentaje suma 100
-- c3
SELECT porcentaje FROM (
    SELECT provincia,poblacion*100/(
        SELECT SUM(poblacion) FROM provincias  
      ) porcentaje FROM provincias  
  ) c2;
-- resultado
SELECT SUM(porcentaje) FROM (
    SELECT porcentaje FROM (
        SELECT provincia,poblacion*100/(
            SELECT SUM(poblacion) FROM provincias  
          ) porcentaje FROM provincias  
      ) c2  
  ) c3;

-- Provincia m�s poblada de las inferiores a 1 mill�n de habitantes
-- c1
SELECT * FROM provincias WHERE poblacion<1e6;
-- c2
SELECT MAX(c1.poblacion) FROM (
    SELECT * FROM provincias WHERE poblacion<1e6  
  ) c1;
-- c3
SELECT provincia FROM provincias WHERE poblacion=(
    SELECT MAX(c1.poblacion) FROM (
        SELECT * FROM provincias WHERE poblacion<1e6  
      ) c1  
  );

-- Provincia menos poblada de las superiores al mill�n de habitantes
SELECT provincia FROM provincias where provincias.poblacion = 
  (SELECT Min(poblacion) FROM provincias p 
    WHERE p.poblacion > 1e6 );

-- Provincia m�s poblada de las que tienen una poblaci�n entre 1 y 2 millones de habitantes
SELECT provincia FROM provincias where provincias.poblacion = 
  (SELECT MAX(poblacion) FROM provincias p 
    WHERE p.poblacion BETWEEN 1e6 AND 2e6);


-- ************************************************************************************
  -- //// CONSULTAS DE TOTALES Y AGRUPAMIENTO
-- Poblaci�n de las autonom�as de m�s a menos
SELECT p.autonomia, SUM(poblacion) pob FROM provincias p 
  GROUP BY p.autonomia
  ORDER BY pob DESC;

-- Ordena las autonom�as por superficie de m�s a menos
SELECT p.autonomia, SUM(p.superficie) sup FROM provincias p
  GROUP BY p.autonomia
  ORDER BY sup DESC;

-- Densidad de poblaci�n de cada autonom�a de m�s a menos
SELECT provincias.autonomia, (SUM(provincias.poblacion)/SUM(provincias.superficie)) densidad FROM provincias
  GROUP BY provincias.autonomia
  ORDER BY densidad DESC;
  

-- ***************************************************************************************
-- //// CONSULTAS DE TOTALES, AGRUPAMIENTOS Y SUBCONSULTAS
-- Poblaci�n de la autonom�a m�s poblada
SELECT MAX(pobla_auto) FROM (
  SELECT autonomia, SUM(provincias.poblacion) pobla_auto FROM provincias 
  GROUP BY provincias.autonomia
  ) c1;

-- Autonom�a m�s poblada
SELECT autonomia FROM (SELECT autonomia, SUM(provincias.poblacion) pobla_auto FROM provincias 
  GROUP BY provincias.autonomia)c1
  WHERE pobla_auto = (
  SELECT MAX(pobla_auto) FROM (
  SELECT autonomia, SUM(provincias.poblacion) pobla_auto FROM provincias 
  GROUP BY provincias.autonomia
  )c2);

-- Contar autonom�as por millones de habitantes
SELECT FLOOR(poblacion/1e6) millones,COUNT(*) autonomias FROM 
  (SELECT autonomia,SUM(poblacion) poblacion FROM provincias 
    group BY autonomia) c1 GROUP BY millones;

-- Poblaci�n media por autonom�as
  -- c1
SELECT SUM(poblacion) FROM provincias;
  -- c2 
SELECT COUNT(DISTINCT autonomia) FROM provincias;
 -- c1/c2
SELECT FORMAT((SELECT SUM(poblacion) FROM provincias)
  /
  (SELECT COUNT(DISTINCT autonomia) FROM provincias),0) resultado;

-- Poblaci�n media de las autonom�as de menos de un mill�n de habitantes
SELECT provincias.autonomia, AVG(poblacion) media, SUM(provincias.poblacion) n_pobla FROM provincias
  GROUP BY provincias.autonomia
  HAVING n_pobla < 1e6;

SELECT autonomia, media FROM (
  SELECT provincias.autonomia, AVG(poblacion) media, SUM(provincias.poblacion) n_pobla FROM provincias
  GROUP BY provincias.autonomia
  HAVING n_pobla < 1e6)c1;

-- Poblaci�n media de las autonom�as entre 2 y 3 millones de habitantes
SELECT autonomia, media FROM (
  SELECT provincias.autonomia, AVG(poblacion) media, SUM(provincias.poblacion) n_pobla FROM provincias
  GROUP BY provincias.autonomia
  HAVING n_pobla > 2e6 AND n_pobla < 3e6)c1;

-- Autom�a m�s extensa
  -- c1
SELECT provincias.autonomia, SUM(superficie)sup FROM provincias 
  GROUP BY provincias.autonomia;
  -- c2
SELECT MAX(sup) FROM (
  SELECT provincias.autonomia, SUM(superficie)sup FROM provincias 
  GROUP BY provincias.autonomia
  ) c2;
  -- Consulta total
SELECT autonomia FROM (
  SELECT provincias.autonomia, SUM(superficie)sup FROM provincias 
  GROUP BY provincias.autonomia
  )c1
  WHERE c1.sup = (
    SELECT MAX(sup) FROM (
     SELECT provincias.autonomia, SUM(superficie)sup FROM provincias 
     GROUP BY provincias.autonomia
  )c2);

-- Porcentaje de superficie de las autonom�as de menor a mayor
SELECT SUM(superficie)sup_tot FROM provincias;
SELECT autonomia, SUM(superficie)sup_pro FROM provincias
  GROUP BY provincias.autonomia;
SELECT autonomia, (c1.sup_pro / (SELECT SUM(superficie)sup_tot FROM provincias)*100) porcen FROM (SELECT autonomia, SUM(superficie)sup_pro FROM provincias
  GROUP BY provincias.autonomia) c1 ORDER BY porcen;
-- Suma de los porcentajes calculados anteriormente
  SELECT SUM(porcen) FROM (
    SELECT autonomia, (c1.sup_pro / (SELECT SUM(superficie)sup_tot FROM provincias)*100) porcen FROM (SELECT autonomia, SUM(superficie)sup_pro FROM provincias
    GROUP BY provincias.autonomia) c1
    ) c2;

-- Porcentaje de poblaci�n de las autonom�as de menor a mayor
SELECT SUM(poblacion)pob_tot FROM provincias;
SELECT autonomia, SUM(poblacion)pob_pro FROM provincias
  GROUP BY provincias.autonomia;
SELECT autonomia, (c1.pob_pro / (SELECT SUM(poblacion)pob_tot FROM provincias)*100) porcen FROM 
  (SELECT autonomia, SUM(poblacion)pob_pro FROM provincias
  GROUP BY provincias.autonomia) c1 ORDER BY porcen;



-- ************************************************************************************ --
-- Autonom�a menos poblada de las superiores al mill�n de habitantes
  -- c1
SELECT DISTINCT provincias.autonomia, SUM(poblacion) pob_aut
  FROM provincias GROUP BY provincias.autonomia;
  -- c2 
SELECT * FROM 
  (SELECT DISTINCT provincias.autonomia, SUM(poblacion) pob_aut
  FROM provincias GROUP BY provincias.autonomia) c1
  WHERE c1.pob_aut > 1e6;
  -- c3
SELECT MIN(pob_aut) FROM 
  (SELECT * FROM 
  (SELECT provincias.autonomia, SUM(poblacion) pob_aut
  FROM provincias GROUP BY provincias.autonomia) c1
  WHERE c1.pob_aut > 1e6) c2;
 -- Consulta final
SELECT autonomia FROM (SELECT * FROM 
  (SELECT DISTINCT provincias.autonomia, SUM(poblacion) pob_aut
  FROM provincias GROUP BY provincias.autonomia) c1
  WHERE c1.pob_aut > 1e6) c2
  WHERE c2.pob_aut = (SELECT MIN(pob_aut) FROM 
  (SELECT * FROM 
  (SELECT provincias.autonomia, SUM(poblacion) pob_aut
  FROM provincias GROUP BY provincias.autonomia) c1
  WHERE c1.pob_aut > 1e6) c2);


-- ***************************************************************************************
-- ////// VARIOS
-- �En qu� posici�n est� Cantabria en poblaci�n?
   -- c1
SELECT poblacion FROM provincias WHERE provincias.provincia LIKE "Cantabria";

SELECT COUNT(*)+1 FROM provincias 
  WHERE provincias.poblacion > (
    SELECT poblacion FROM provincias WHERE provincias.provincia LIKE "Cantabria"
  ); 

-- �Qu� porcentaje de la poblaci�n del pa�s representa Cantabria?
  -- c1
SELECT SUM(poblacion) FROM provincias;
  -- c2 
SELECT poblacion FROM provincias WHERE provincias.provincia LIKE "Cantabria";
  -- Consulta final
SELECT (
  SELECT poblacion FROM provincias WHERE provincias.provincia LIKE "Cantabria"
  )/(
  SELECT SUM(poblacion) FROM provincias
      ) * 100;

-- Ranking de densidad de poblaci�n de las autonomias
SELECT provincias.autonomia, (SUM(provincias.poblacion)/SUM(provincias.superficie)) densidad FROM provincias
  GROUP BY provincias.autonomia
  ORDER BY densidad DESC;
