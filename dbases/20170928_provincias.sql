USE aula;
/* CONSULTAS DE PROYECCI�N Y SELECCI�N */

-- 1--�Qu� provincias tienen m�s de un mill�n de habitantes?
SELECT provincia,poblacion FROM provincias WHERE poblacion > 1000000;
-- 2--�Qu� provincias empiezan por C?
SELECT provincia FROM provincias WHERE provincias.provincia LIKE 'c%';
-- 3--�Qu� autonom�as empiezan por C?
SELECT autonomia FROM provincias WHERE provincias.autonomia LIKE 'c%';
-- 4--�Qu� autonom�as tienen provincias que empiezan por C?
SELECT autonomia,provincia FROM provincias WHERE provincias.provincia LIKE 'c%';
-- 5--�Qu� autonom�as tienen provincias de m�s de un mill�n de habitantes?
SELECT DISTINCT autonomia FROM provincias WHERE provincias.poblacion > 1e6;
-- 6--�Cu�les son las provincias de Galicia y el Pa�s Vasco?
SELECT autonomia,provincia FROM provincias WHERE provincias.autonomia='Galicia' OR provincias.autonomia
  = 'Pa�s Vasco';
-- 7--�Qu� provincias de Galicia no empiezan por L?
SELECT provincia FROM provincias
  WHERE provincias.provincia NOT LIKE 'l%' AND provincias.autonomia = 'Galicia';
-- 8--�Qu� provincias de ambas castillas no empiezan por A?
SELECT provincia FROM provincias
  WHERE provincias.autonomia LIKE 'Castilla%' AND provincias.provincia not LIKE 'A%'; 
-- 9--�Qu� provincias que empiezan por A no pertencen a ambas Castillas?
SELECT provincia FROM provincias 
  WHERE provincias.provincia LIKE 'a%' AND provincias.autonomia NOT LIKE 'Castilla%';
-- 10--�Qu� provincias de m�s de dos millones de habitaciones pertencen a Catalu�a?
SELECT provincia FROM provincias 
  WHERE provincias.poblacion > 2e6 AND provincias.autonomia = 'Catalu�a';
-- �Qu� provincias de m�s de dos millones de habitaciones no pertencen a Catalu�a?
-- -- soluci�n por conjuntos
-- -- provincias de m�s de dos millones de habitantes
-- -- provincias que pertencen a catalu�a
-- -- resultado
SELECT * FROM (
    SELECT provincia FROM provincias WHERE poblacion>2e6
  ) c1 WHERE c1.provincia NOT IN (
    SELECT provincia FROM provincias WHERE autonomia='Catalu�a'
  );
SELECT provincia FROM provincias
  WHERE provincias.poblacion >2e6 AND provincias.autonomia NOT LIKE 'Catalu�a';
  
-- 11--�Qu� provincias de menos de medio mill�n de habitantes no pertencen a ningua de ambas Castillas?
SELECT provincia,poblacion FROM provincias
  WHERE provincias.poblacion < 500000 AND provincias.autonomia NOT LIKE 'Castilla%';

/* CONSULTAS DE TOTALES */

-- 12--�Cu�ntas autonom�as empiezan por C?
SELECT COUNT(*) FROM (
  SELECT DISTINCT autonomia FROM provincias WHERE provincias.autonomia LIKE 'c%'
  ) c1;
-- 13--�Cu�ntas provincias pertenecen a autonom�as que empiezan por C?
SELECT COUNT(*) FROM (
  SELECT provincia FROM provincias 
    WHERE provincias.autonomia LIKE 'c%'
  ) c1;
-- 14--�Cu�ntas provincias que empiezan por A no pertencen a ambas Castillas?
SELECT COUNT(*) FROM (
  SELECT provincia FROM provincias 
    WHERE provincias.autonomia not LIKE 'castilla%' 
    AND provincias.provincia LIKE 'a%'
  ) c1;

/* CONSULTAS DE TOTALES CON SUBCONSULTAS */

-- 15--�Cu�l es la provincia no castellana de menor densidad de poblaci�n
select provincia from provincias 
    where (poblacion/superficie) = (
        select min(densidad) from ( 
            select provincia, (poblacion/superficie) densidad FROM provincias 
                where autonomia not like 'castilla%') c1);
        
-- 16--�En qu� autonom�a est� la provincia m�s extensa?
SELECT autonomia from provincias 
    where superficie = ( 
        select max(superficie) from provincias);

/* CONSULTAS DE AGRUPAMIENTOS */

-- 17--�Qu� provincias de autonom�as de m�s de 2 millones de habitantes tiene m�s de un mill�n de habitantes?
-- -- provincias de autonom�as de m�s de 2 millones de habitantes
-- -- provincias de m�s de un mill�n de habitantes
  -- a <--
   SELECT autonomia, SUM(provincias.poblacion) habitantes FROM provincias GROUP BY autonomia;
  -- c2 <--
   SELECT * FROM (SELECT autonomia, SUM(provincias.poblacion) habitantes FROM provincias GROUP BY autonomia) a
    WHERE a.habitantes > 2e6;
  -- c2 variante <--
   SELECT autonomia, SUM(provincias.poblacion) habitantes FROM provincias 
    GROUP BY autonomia
    HAVING habitantes > 2e6;
  -- c2 variante <--
   SELECT autonomia FROM provincias 
    GROUP BY autonomia
    HAVING SUM(provincias.poblacion) > 2e6;
  -- c3 <--
   SELECT autonomia FROM (
    SELECT * FROM (SELECT autonomia, SUM(provincias.poblacion) habitantes FROM provincias 
                    GROUP BY autonomia) a
      WHERE a.habitantes > 2e6) c2;
  -- c1 <--
    SELECT provincia,autonomia FROM provincias 
      WHERE provincias.poblacion > 1e6;
  -- Consulta final <--
    SELECT c1.provincia FROM (
      SELECT provincia,autonomia FROM provincias 
      WHERE provincias.poblacion > 1e6
      ) c1 WHERE c1.autonomia IN (
        SELECT autonomia FROM provincias 
        GROUP BY autonomia
        HAVING SUM(provincias.poblacion) > 2e6
      );

-- 18-- �Qu� provincias de autonom�as de menos de 2 millones de habitantes tiene m�s de un mill�n de habitantes?
    SELECT c1.provincia FROM (
      SELECT provincia,autonomia FROM provincias 
      WHERE provincias.poblacion > 1e6
      ) c1 WHERE c1.autonomia IN (
        SELECT autonomia FROM provincias 
        GROUP BY autonomia
        HAVING SUM(provincias.poblacion) < 2e6
      );

-- �Qu� provincias de autonom�as de m�s de 2 millones de habitantes tiene menos de un mill�n de habitantes?
  SELECT autonomia FROM provincias 
        GROUP BY autonomia
        HAVING SUM(provincias.poblacion) > 2e6;

SELECT provincia FROM provincias WHERE provincias.poblacion < 1e6;

    SELECT c1.provincia FROM (
      SELECT provincia,autonomia FROM provincias 
      WHERE provincias.poblacion < 1e6
      ) c1 WHERE c1.autonomia IN (
        SELECT autonomia FROM provincias 
        GROUP BY autonomia
        HAVING SUM(provincias.poblacion) > 2e6
      );

-- �Cu�l es la provincia m�s poblada de la autonom�a m�s poblada?
SELECT autonomia, sum(provincias.poblacion ) FROM provincias GROUP BY provincias.autonomia ;

SELECT autonomia,MAX(c1.total) FROM (
  SELECT autonomia, sum(provincias.poblacion ) total FROM provincias GROUP BY provincias.autonomia
  ) c1;

SELECT autonomia FROM (
  SELECT autonomia,MAX(c1.total) FROM (
  SELECT autonomia, sum(provincias.poblacion ) total FROM provincias GROUP BY provincias.autonomia
  ) c1) c2;

SELECT MAX(poblacion) FROM provincias WHERE provincias.autonomia LIKE (SELECT autonomia FROM (
  SELECT autonomia,MAX(c1.total) FROM (
  SELECT autonomia, sum(provincias.poblacion ) total FROM provincias GROUP BY provincias.autonomia
  ) c1)c2);

SELECT provincia FROM provincias WHERE provincias.poblacion = (
  SELECT MAX(poblacion) FROM provincias WHERE provincias.autonomia LIKE (SELECT autonomia FROM (
  SELECT autonomia,MAX(c1.total) FROM (
  SELECT autonomia, sum(provincias.poblacion ) total FROM provincias GROUP BY provincias.autonomia
  ) c1)c2)
  );

-- �Cu�l es la provincia m�s poblada de la autonom�a m�s despoblada?


-- �Cu�l es la provincia menos poblada de la autonom�a m�s poblada?

-- �Cu�l es la provincia m�s poblada de la autonom�a m�s extensa?

-- �Cu�l es la provincia con menos densidad de poblaci�n de la autonom�a con m�s densidad de poblaci�n?
SELECT autonomia, (poblacion/superficie) densidad FROM provincias GROUP BY provincias.autonomia ;

SELECT MIN(c1.densidad) FROM (
  SELECT autonomia, (poblacion/superficie) densidad FROM provincias GROUP BY provincias.autonomia
  )c1;

SELECT autonomia FROM (SELECT autonomia, (poblacion/superficie) densidad FROM provincias GROUP BY provincias.autonomia) c1
  WHERE c1.densidad = (SELECT MIN(c2.densidad) FROM (
  SELECT autonomia, (poblacion/superficie) densidad FROM provincias GROUP BY provincias.autonomia
  )c2);

SELECT min(poblacion/superficie) densidad FROM provincias 
  WHERE autonomia = (
   SELECT autonomia FROM (SELECT autonomia, (poblacion/superficie) densidad FROM provincias GROUP BY provincias.autonomia) c1
  WHERE c1.densidad = (SELECT MIN(c2.densidad) FROM (
  SELECT autonomia, (poblacion/superficie) densidad FROM provincias GROUP BY provincias.autonomia
  )c2) 
  );

SELECT provincia FROM (SELECT provincias.provincia,(poblacion/superficie) densidad FROM provincias 
  WHERE autonomia = (
   SELECT autonomia FROM (SELECT autonomia, (poblacion/superficie) densidad FROM provincias GROUP BY provincias.autonomia) c1
  WHERE c1.densidad = (SELECT MIN(c2.densidad) FROM (
  SELECT autonomia, (poblacion/superficie) densidad FROM provincias GROUP BY provincias.autonomia
  )c2) 
  ) )c3 WHERE densidad = (SELECT min(poblacion/superficie) densidad FROM provincias 
  WHERE autonomia = (
   SELECT autonomia FROM (SELECT autonomia, (poblacion/superficie) densidad FROM provincias GROUP BY provincias.autonomia) c1
  WHERE c1.densidad = (SELECT MIN(c2.densidad) FROM (
  SELECT autonomia, (poblacion/superficie) densidad FROM provincias GROUP BY provincias.autonomia
  )c2) 
  ));


-- �Cu�l es la provincia con m�s densidad de poblaci�n de la autonom�a con menos densidad de poblaci�n?
