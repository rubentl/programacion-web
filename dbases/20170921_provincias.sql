-- //// CONSULTAS DE TOTALES
-- Poblaci�n del pa�s
SELECT sum(poblacion) FROM provincias;
-- Superficie del pa�s
SELECT SUM(superficie) FROM provincias;
-- Densidad de poblaci�n del pa�s
SELECT SUM(poblacion) / SUM(superficie) FROM provincias;
-- Densidad de poblaci�n de las provincias de m�s a menos
SELECT provincia, (poblacion / superficie) p  FROM provincias
  ORDER BY p DESC;
-- Poblaci�n media por provincias
SELECT AVG(poblacion) FROM provincias;
-- Poblaci�n media de provincias que empiezan por A
SELECT AVG(poblacion) FROM provincias 
  WHERE provincia LIKE 'A%';
-- Poblaci�n media de las provincias entre 2 y 3 millones de habitantes
SELECT AVG(poblacion) FROM provincias
  WHERE poblacion > 2000000 AND poblacion < 3000000;
-- Poblaci�n media de las provincias entre 1 y 2 millones de habitantes
SELECT AVG(poblacion) FROM provincias
  WHERE poblacion BETWEEN 1e6 AND 2e6;
-- Listado de provincias por superficie descendente
SELECT provincia FROM provincias
  ORDER BY superficie DESC;
-- Ranking de densidad de poblaci�n de las autonom�as
SELECT autonomia, round((poblacion / superficie)/100) rank  FROM provincias
  GROUP BY autonomia ORDER BY rank DESC;
-- Contar provincias por millones de habitantes
SELECT millones, COUNT(*) FROM (
  SELECT provincia, FLOOR(poblacion/1e6) millones FROM provincias 
  ) c1 GROUP BY c1.millones;
-- Ranking de provincias m�s pobladas
SELECT p.provincia, ROUND(poblacion/1e6) rank FROM provincias p 
  ORDER BY rank DESC;
  
-- //// CONSULTAS DE TOTALES Y SUBCONSULTAS
-- Provincia m�s poblada
SELECT provincia FROM provincias 
  WHERE poblacion = (SELECT MAX(poblacion) FROM provincias);
-- Provincia m�s extensa
SELECT p.provincia, superficie FROM provincias p 
  WHERE p.superficie = (SELECT MAX(p.superficie) FROM provincias p);
-- Porcentaje de la poblaci�n de cada provincia de mayor a menor
SELECT provincia, (provincias.poblacion/(SELECT SUM(poblacion) FROM provincias))*100 porcen  FROM provincias
  ORDER BY porcen Desc;
-- Comprobar si el porcentaje suma 100
SELECT SUM((provincias.poblacion * 100)/(SELECT SUM(poblacion) FROM provincias)) porcen  FROM provincias;
-- Provincia m�s poblada de las inferiores a 1 mill�n de habitantes
SELECT provincia FROM provincias
  WHERE provincias.poblacion = (SELECT MAX(c1.poblacion) FROM 
  (SELECT p.provincia, p.poblacion FROM provincias p 
    WHERE p.poblacion < 1e6) c1 );
-- Provincia menos poblada de las superiores al mill�n de habitantes
SELECT c1.provincia, MIN(c1.poblacion) FROM 
  (SELECT p.provincia, p.poblacion FROM provincias p 
    WHERE p.poblacion > 1e6) c1;
-- Provincia m�s poblada de las que tienen una poblaci�n entre 1 y 2 millones de habitantes
SELECT c1.provincia, MAX(c1.poblacion) FROM 
  (SELECT p.provincia, p.poblacion FROM provincias p 
    WHERE p.poblacion BETWEEN 1e6 AND 2e6) c1;  

  -- //// CONSULTAS DE TOTALES Y AGRUPAMIENTO
-- Poblaci�n de las autonom�as de m�s a menos
SELECT p.autonomia, SUM(poblacion) suma FROM provincias p 
  GROUP BY p.autonomia
  ORDER BY suma DESC;
-- Ordena las autonom�as por superficie de m�s a menos
SELECT p.autonomia, SUM(p.superficie) suma FROM provincias p
  GROUP BY p.autonomia
  ORDER BY suma DESC;
-- Densidad de poblaci�n de cada autonom�a de m�s a menos
SELECT provincias.autonomia, (provincias.poblacion/provincias.superficie) densidad FROM provincias
  GROUP BY provincias.autonomia
  ORDER BY densidad DESC;
  
-- //// CONSULTAS DE TOTALES, AGRUPAMIENTOS Y SUBCONSULTAS
-- Poblaci�n de la autonom�a m�s poblada
SELECT MAX(pobla_auto) FROM (
  SELECT autonomia, SUM(provincias.poblacion) pobla_auto FROM provincias 
  GROUP BY provincias.autonomia
  ) c1;
-- Autonom�a m�s poblada
SELECT autonomia FROM (SELECT autonomia, SUM(provincias.poblacion) pobla_auto FROM provincias 
  GROUP BY provincias.autonomia)c1
  WHERE pobla_auto = (
  SELECT MAX(pobla_auto) FROM (
  SELECT autonomia, SUM(provincias.poblacion) pobla_auto FROM provincias 
  GROUP BY provincias.autonomia
  )c2);
-- Contar autonom�as por millones de habitantes


-- Poblaci�n media por autonom�as
  SELECT provincias.autonomia, AVG(poblacion) FROM provincias GROUP BY autonomia;

-- Poblaci�n media de las autonom�as de menos de un mill�n de habitantes
SELECT provincias.autonomia, AVG(poblacion) media, SUM(provincias.poblacion) n_pobla FROM provincias
  GROUP BY provincias.autonomia
  HAVING n_pobla < 1e6;

SELECT autonomia, media FROM (
  SELECT provincias.autonomia, AVG(poblacion) media, SUM(provincias.poblacion) n_pobla FROM provincias
  GROUP BY provincias.autonomia
  HAVING n_pobla < 1e6)c1;

-- Poblaci�n media de las autonom�as entre 2 y 3 millones de habitantes
SELECT autonomia, media FROM (
  SELECT provincias.autonomia, AVG(poblacion) media, SUM(provincias.poblacion) n_pobla FROM provincias
  GROUP BY provincias.autonomia
  HAVING n_pobla > 2e6 AND n_pobla < 3e6)c1;

-- Autom�a m�s extensa
  -- c1
SELECT provincias.autonomia, SUM(superficie)sup FROM provincias 
  GROUP BY provincias.autonomia;

  -- c2
SELECT MAX(sup) FROM (
  SELECT provincias.autonomia, SUM(superficie)sup FROM provincias 
  GROUP BY provincias.autonomia
  ) c2;

  -- Consulta total
SELECT autonomia FROM (
  SELECT provincias.autonomia, SUM(superficie)sup FROM provincias 
  GROUP BY provincias.autonomia
  )c1
  WHERE c1.sup = (
    SELECT MAX(sup) FROM (
     SELECT provincias.autonomia, SUM(superficie)sup FROM provincias 
     GROUP BY provincias.autonomia
  )c2);


-- Porcentaje de superficie de las autonom�as de menor a mayor
SELECT SUM(superficie)sup_tot FROM provincias;
SELECT autonomia, SUM(superficie)sup_pro FROM provincias
  GROUP BY provincias.autonomia;
SELECT autonomia, (c1.sup_pro / (SELECT SUM(superficie)sup_tot FROM provincias)*100) porcen FROM (SELECT autonomia, SUM(superficie)sup_pro FROM provincias
  GROUP BY provincias.autonomia) c1 ORDER BY porcen;


-- Suma de los porcentajes calculados anteriormente
  SELECT SUM(porcen) FROM (
    SELECT autonomia, (c1.sup_pro / (SELECT SUM(superficie)sup_tot FROM provincias)*100) porcen FROM (SELECT autonomia, SUM(superficie)sup_pro FROM provincias
    GROUP BY provincias.autonomia) c1
    ) c2;
-- Porcentaje de poblaci�n de las autonom�as de menor a mayor

-- Autonom�a menos poblada de las superiores al mill�n de habitantes


-- ////// VARIOS
-- �En qu� posici�n est� Cantabria en poblaci�n?

-- �Qu� porcentaje de la poblaci�n del pa�s representa Cantabria?