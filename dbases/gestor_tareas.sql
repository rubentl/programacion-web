CREATE DATABASE gestor_tareas;
USE gestor_tareas;
CREATE TABLE roles(
  rol varchar(15),
  PRIMARY KEY(rol)
  );
INSERT INTO roles VALUES 
  ('admin'),('user'),('guest');
CREATE TABLE usuarios(
  id_usuario int AUTO_INCREMENT,
  login varchar(127),
  passwd varchar(255),
  email varchar(127),
  nombre varchar(127),
  rol varchar(15),
  PRIMARY KEY(id_usuario),
  FOREIGN KEY(rol) REFERENCES roles(rol)
  );
INSERT INTO usuarios 
  (login,passwd,email,nombre,rol)
  VALUES 
  ('david',MD5('hola'),
  'david@alpeformacion.es',
  'David','admin' );
CREATE TABLE tareas(
  id_tarea int AUTO_INCREMENT,
  id_creador int,
  tarea varchar(127),
  categoria varchar(127),
  detalles text,
  instante_alarma datetime,
  prioridad float,
  estima_duracion float,
  fecha_inicio datetime,
  fecha_limite datetime,
  PRIMARY KEY(id_tarea),
  FOREIGN KEY(id_creador) REFERENCES usuarios(id_usuario)
  );
INSERT INTO tareas
  (id_creador,tarea,estima_duracion)
  VALUES
  (1,'Terminar base datos',1);
CREATE TABLE reciben(
  id_usuario int,
  id_tarea int,
  PRIMARY KEY(id_usuario,id_tarea),
  FOREIGN KEY(id_usuario) REFERENCES usuarios(id_usuario),
  FOREIGN KEY(id_tarea) REFERENCES tareas(id_tarea)
 );
INSERT INTO reciben VALUES (1,1);