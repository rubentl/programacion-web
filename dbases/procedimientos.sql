SELECT * from disfrutan WHERE disfrutan.id_alumno IS NULL OR disfrutan.id_aficion IS null;

DROP PROCEDURE nulos;

CREATE PROCEDURE nulos()
  begin
    DECLARE var_id_alumno int;
    SET var_id_alumno = (
    SELECT id_alumno from disfrutan 
      WHERE disfrutan.id_alumno IS NULL OR disfrutan.id_aficion IS NULL
      LIMIT 1);
    SELECT * FROM alumnos WHERE alumnos.id_alumno = var_id_alumno;
  end;

CALL nulos();


CREATE PROCEDURE alumno(var_id_alumno int)
  BEGIN
   SELECT * FROM alumnos WHERE alumnos.id_alumno = var_id_alumno;
  END;

CALL alumno(10);


CREATE FUNCTION nombre(var_id_alumno int)
  RETURNS varchar(31)
BEGIN 
  return (SELECT nombre FROM alumnos WHERE alumnos.id_alumno = var_id_alumno);
END;

CREATE FUNCTION aficion(var_id_aficion int)
  RETURNS varchar(64)
BEGIN 
  return (SELECT aficion FROM aficiones WHERE id_aficion = var_id_aficion);
END;

SELECT nombre(10);

SELECT nombre(id_alumno) nombre, aficion(id_aficion) aficion FROM disfrutan;

CREATE PROCEDURE eliminar_nulos()
  BEGIN 
    DELETE FROM disfrutan 
      WHERE disfrutan.id_aficion IS NULL OR disfrutan.id_alumno IS NULL;
  END;

SELECT id_alumno, id_aficion FROM disfrutan WHERE id_aficion IS NULL;

CALL eliminar_nulos();

DROP PROCEDURE estadistica;

CREATE PROCEDURE estadistica()
  BEGIN
    Declare var_Alum INT ; 
    Declare  var_Afi INT;
 
    CREATE TABLE if NOT EXISTS estadisticas (
      alumnos int NOT NULL,
      aficiones int NOT NULL,
      alarma boolean
    );
    TRUNCATE TABLE estadisticas;

    set var_Alum = (SELECT COUNT(*) n_alum FROM alumnos);
    set var_Afi = (SELECT COUNT(*) n_afi FROM aficiones);
    INSERT INTO estadisticas VALUES (var_Alum, var_Afi, 0);

    if (Select Count(*) FROM alumnos)>
          (SELECT COUNT(distinct id_alumno) FROM disfrutan)
    THEN UPDATE estadisticas set alarma = 1;
    ELSE UPDATE estadisticas set alarma = 0;
    END IF;
  END;

  CALL estadistica();

SELECT * FROM estadisticas;



/* Crea la funci�n aficion_popular que nos devuelva la afici�n m�s popular entre los alumnos */
CREATE FUNCTION aficion_popular()
  RETURNS varchar(64)
BEGIN 
  return (
    SELECT aficion FROM aficiones WHERE aficiones.id_aficion = (
      SELECT id_aficion FROM (
        SELECT disfrutan.id_aficion, COUNT(*) popularidad FROM disfrutan GROUP BY id_aficion
        )c2 WHERE c2.popularidad = (
          SELECT MAX(popularidad) FROM (
             SELECT disfrutan.id_aficion, COUNT(*) popularidad FROM disfrutan GROUP BY id_aficion
             )c1
          )LIMIT 1
        )
    );
END; 

SELECT aficion_popular();

/* Crea un procedimiento almacenado aficion_popular  que, en la tabla de estad�stica, nos
  actualice un campo con la afici�n m�s compartida y 
  marque con un bool a los alumnos que compartan esa afici�n */

ALTER TABLE estadisticas ADD COLUMN aficion_popular varchar(64);
ALTER TABLE alumnos ADD COLUMN aficion_popular boolean;

DROP PROCEDURE aficion_popular;
CREATE PROCEDURE aficion_popular()
  BEGIN
    UPDATE estadisticas set aficion_popular = aficion_popular();
    UPDATE alumnos set aficion_popular = 1 WHERE alumnos.id_alumno IN (
      SELECT id_alumno FROM disfrutan WHERE disfrutan.id_aficion = (
        SELECT id_aficion FROM aficiones WHERE aficion = aficion_popular()
        ) 
      );
  END;

CALL aficion_popular();

/* Crea una funci�n que, a partir de una parte del nombre del alumno,
  nos devuelva con cu�ntos compa�eros podr�a organizar el mayor evento de sus aficiones y
  -1 si hay m�s de un alumno que responda a ese nombre
*/

  DROP FUNCTION crear_evento;
CREATE FUNCTION crear_evento(var_alumno varchar(31))
  RETURNS int
  BEGIN
    DECLARE var_n_alumno int;
    DECLARE var_return int;
    DECLARE var_id_alumno int;
    SET var_n_alumno = (SELECT COUNT(*) FROM alumnos 
                          WHERE alumnos.nombre LIKE CONCAT('%',var_alumno,'%'));
    IF var_n_alumno = 1 
      THEN 
        SELECT id_alumno INTO var_id_alumno FROM alumnos 
          WHERE alumnos.nombre LIKE CONCAT('%',var_alumno,'%');
        SELECT MAX(popular) - 1  INTO var_return FROM (
          SELECT id_aficion, COUNT(*) popular FROM disfrutan WHERE id_aficion IN (
              SELECT id_aficion FROM disfrutan WHERE disfrutan.id_alumno = var_id_alumno                                                         
              ) GROUP BY disfrutan.id_aficion
          ) c1;
      ELSE
         SET var_return = -1; 
    end IF ;
    RETURN var_return;
  END;

SELECT crear_evento('agus');


/* Crea un procedimiento almacenado que recorra la tabla aficiones y
  rellene un campo n_aficiones que contenga el n�mero de alumnos que la comparten  
  */

ALTER TABLE alumnos ADD COLUMN n_aficiones int;

CREATE PROCEDURE numero_aficiones()
  BEGIN 
    DECLARE cursor Cursor for 
  END;