USE aula;
-- Crear tabla de puesto y lugar de procedencia
CREATE TABLE poblaciones (
  puesto int,
  poblacion varchar (127)
  );

-- Introducimos datos
INSERT INTO poblaciones VALUES
  ('700','Li�rganes'),
  ('708','Renedo de Pi�lagos'),
  ('714','Ampuero'),
  ('707','Venecia'),
  ('709','Santander'),
  ('701','Santander'),
  ('702','Renedo de Pi�lagos'),
  ('703','Santander'),
  ('713','Santander'),
  ('711','Santander'),
  ('704','Castro Urdiales'),
  ('710','Castro Urdiales'),
  ('712','Santander'),
  ('715','L�mpias');

-- Consultas
SELECT * FROM poblaciones;
  -- El nombre del alumno cuya poblaci�n es Li�rganes
SELECT a.nombre FROM aula a, poblaciones p 
  WHERE p.poblacion='Li�rganes' AND a.puesto=p.puesto;
  -- Poblaci�n de santander
SELECT * FROM poblaciones p 
  WHERE p.poblacion='Santander';
  -- Proyecci�n del puesto de la seleci�n anterior
SELECT puesto FROM poblaciones p 
  WHERE p.poblacion='Santander';
  -- Proyecci�n del puesto para poblaciones distintas de Santander
SELECT puesto FROM poblaciones p 
  WHERE p.poblacion<>'Santander';
  -- De donde son los que no son de santander sin repetidos
SELECT distinct poblacion FROM poblaciones 
  WHERE poblacion <> 'Santander';
  -- Ordenar lo anterior
SELECT distinct poblacion FROM poblaciones 
  WHERE poblacion <> 'Santander' 
  ORDER BY poblacion;
  -- listado de todas las poblaciones por orden alfab�tico
SELECT DISTINCT poblacion FROM poblaciones 
  ORDER BY poblacion;
  -- poblacines que empiecen por L
SELECT poblacion FROM poblaciones 
  WHERE poblacion 
  LIKE 'L%'; 
  -- Puestos a partir del 709
SELECT puesto FROM  poblaciones 
  WHERE puesto > 709;
  -- poblaciones con %e_o%
SELECT distinct poblacion FROM poblaciones
  WHERE poblacion LIKE '%e_o%';
  -- Que la poblaci�n tenga m�s de una palabra
SELECT DISTINCT poblacion FROM poblaciones 
  WHERE poblacion LIKE '% %';
  -- Poblaciones con un de
SELECT DISTINCT poblacion FROM poblaciones 
  WHERE poblacion LIKE '% de %';
  -- Que la poblacion termine con espacio
SELECT * FROM poblaciones WHERE poblacion REGEXP ' $';
SELECT * FROM poblaciones WHERE poblacion LIKE '% ';