var nature = {
    imagen: [
        "http://lorempixel.com/900/600/nature/1",
        "http://lorempixel.com/900/600/nature/2",
        "http://lorempixel.com/900/600/nature/3",
        "http://lorempixel.com/900/600/nature/4",
        "http://lorempixel.com/900/600/nature/5",
        "http://lorempixel.com/900/600/nature/6",
        "http://lorempixel.com/900/600/nature/7",
        "http://lorempixel.com/900/600/nature/8",
    ],
    miniatura: [
        "http://lorempixel.com/200/100/nature/1",
        "http://lorempixel.com/200/100/nature/2",
        "http://lorempixel.com/200/100/nature/3",
        "http://lorempixel.com/200/100/nature/4",
        "http://lorempixel.com/200/100/nature/5",
        "http://lorempixel.com/200/100/nature/6",
        "http://lorempixel.com/200/100/nature/7",
        "http://lorempixel.com/200/100/nature/8",
    ]
};

var datos = 0;

function precarga(imagenes) {
    var imagen = new Array(imagenes['imagen'].length);
    var miniatura = new Array(imagenes['imagen'].length);
    for (var i = 0; i < imagen.length; i++) {
        imagen[i] = new Image();
        imagen[i].src = imagenes['imagen'][i];
        miniatura[i] = new Image();
        miniatura[i].src = imagenes['miniatura'][i];
    };
    return {
        imagenes: imagen,
        miniaturas: miniatura
    };
};

function resaltarFoto(miniatura) {
    document.querySelector("#fotos>div:nth-child(" + (miniatura + 1) + ")")
        .classList.add("actual");
};

function vista(imagen) {
    var vista = document.querySelector("#vista");
    var previo = vista.childNodes[0];
    if (typeof(previo) == 'object') {
        vista.replaceChild(imagen, previo);
    } else {
        vista.appendChild(imagen);
    }
};

function fotos(miniaturas, pagina) {
    var fotos = document.querySelectorAll("#fotos>div");
    var previo = null;
    pagina *= 4;
    for (var i = 0; i < 4; i++) {
        fotos[i].classList.remove("actual");
        previo = fotos[i].childNodes[0];
        if (typeof(previo) == 'object') {
            fotos[i].replaceChild(miniaturas[pagina], previo);
        } else {
            fotos[i].appendChild(miniaturas[pagina]);
        }
        pagina++;
    };
};

function eventos() {
    document.querySelector("#paginacion>ul>li:nth-child(1) a")
        .addEventListener("click", function() {
            datos.actuales.vista = 0;
            datos.actuales.pagina = 0;
            vista(datos.img['imagenes'][0]);
            fotos(datos.img['miniaturas'], 0);
            resaltarFoto(0);
        });
    document.querySelector("#paginacion>ul>li:nth-child(4) a")
        .addEventListener("click", function() {
            datos.actuales.vista = datos.actuales.total;
            datos.actuales.pagina = datos.actuales.paginas;
            vista(datos.img['imagenes'][datos.actuales.vista]);
            fotos(datos.img['miniaturas'], datos.actuales.paginas);
            resaltarFoto(3);
        });
    document.querySelector("#paginacion>ul>li:nth-child(2) a")
        .addEventListener("click", function() {
            if (datos.actuales.vista > 0) {
                datos.actuales.vista -= 1;
                datos.actuales.pagina = Math.trunc(datos.actuales.vista / 4);
                vista(datos.img['imagenes'][datos.actuales.vista]);
                fotos(datos.img['miniaturas'], datos.actuales.pagina);
                resaltarFoto(datos.actuales.vista % 4);
            }
        });
    document.querySelector("#paginacion>ul>li:nth-child(3) a")
        .addEventListener("click", function() {
            if (datos.actuales.vista < datos.actuales.total) {
                datos.actuales.vista += 1;
                datos.actuales.pagina = Math.trunc(datos.actuales.vista / 4);
                vista(datos.img['imagenes'][datos.actuales.vista]);
                fotos(datos.img['miniaturas'], datos.actuales.pagina);
                resaltarFoto(datos.actuales.vista % 4);
            }
        });
};

function categoria(imagenes){
    var imgs = precarga(imagenes);
    var indices = {
        vista: 0,
        pagina: 0,
        paginas: (imgs['imagenes'].length / 4) - 1,
        total: imgs['imagenes'].length - 1
    };
    vista(imgs['imagenes'][indices.vista]);
    fotos(imgs['miniaturas'], indices.pagina);
    resaltarFoto(0);
    return {img:imgs, actuales:indices};
};

window.addEventListener("load", function(){
    datos = categoria(nature);
    eventos();
});
