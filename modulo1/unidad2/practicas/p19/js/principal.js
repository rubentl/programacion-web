var dados = {
    dado: [{
        ancho: 100,
        alto: 100,
        valor: 1,
        src: "imgs/1.svg"
    }, {
        ancho: 100,
        alto: 100,
        valor: 2,
        src: "imgs/2.svg"
    }, {
        ancho: 100,
        alto: 100,
        valor: 3,
        src: "imgs/3.svg"
    }, {
        ancho: 100,
        alto: 100,
        valor: 4,
        src: "imgs/4.svg"
    }, {
        ancho: 100,
        alto: 100,
        valor: 5,
        src: "imgs/5.svg"
    }, {
        ancho: 100,
        alto: 100,
        valor: 6,
        src: "imgs/6.svg"
    }],
    azar: function() {
        var num = Math.floor(Math.random() * 6);
        return this.dado[num];
    }
};

function tirada(datos) {
    var result = {
        dado1: datos.azar(),
        dado2: datos.azar(),
    };
    result.suma = result.dado1.valor + result.dado2.valor;
    return result;
};

function dibujarTirada(tiro) {
    var lienzo = document.querySelector("#dados");
    lienzo.style.width = ((tiro.dado1.ancho * 2) + 10) + "px";
    var img1 = document.createElement("img");
    img1.src = tiro.dado1.src;
    img1.style.width = tiro.dado1.ancho + "px";
    var img2 = document.createElement("img");
    img2.src = tiro.dado2.src;
    img2.style.marginLeft = "10px";
    img2.style.width = tiro.dado2.ancho + "px";
    lienzo.appendChild(img1);
    lienzo.appendChild(img2);
    var resultado = document.createElement("div");
    resultado.innerHTML = tiro.suma;
    resultado.style.border = "1px solid black";
    resultado.style.textAlign = "center";
    resultado.style.padding = "10px 30px";
    lienzo.appendChild(resultado);
}

function diezTiradas(dados) {
    var lienzo = document.querySelector("#dados");
    lienzo.innerHTML = "";
    var tiradas = new Array(10).fill(0);
    tiradas.forEach(function(item, idx, arr) {
        var tmp = tirada(dados);
        arr[idx] = tmp.suma;
        dibujarTirada(tmp);
    });
    var mayor = Math.max.apply(null, tiradas);
    var resumen = document.createElement("p");
    resumen.style.padding = "30px 0";
    resumen.innerHTML = "La tirada mayor ha sido: " + mayor;
    lienzo.appendChild(resumen);
}
