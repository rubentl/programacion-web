window.onload = function() {
    /* Un comentario que no explica mucho:
     * Saludamos a la consola con el típico tópico
     * de Hola mundo.
     */
    console.log("Hola mundo");
    /* Y vuelvo a enviar otro mensaje
     * proclamando que soy el primer script
     * como si eso le importara a la consola
     */
    console.log("Soy el primer script");

    var mensaje = "Hola Mundo!\nQué fácil es incluir 'comillas simples' y \"comillas dobles\"";
    alert(mensaje);
};
