function parte1() {
    var punto = document.createElement("div");
    punto.classList.add("parte1");
    var coor = document.querySelector("#coor");
    window.addEventListener("mousemove", function(evt) {
        var clone = punto.cloneNode();
        clone.style.top = evt.pageY + "px";
        clone.style.left = evt.pageX + "px";
        document.body.appendChild(clone);
        coor.innerHTML = evt.pageX + "," + evt.pageY;
    });
}

function parte2() {
    var punto = document.createElement("div");
    punto.classList.add("parte2");
    var coor = document.querySelector("#coor");
    window.addEventListener("click", function(evt) {
        var clone = punto.cloneNode();
        clone.style.top = evt.pageY + "px";
        clone.style.left = evt.pageX + "px";
        document.body.appendChild(clone);
        coor.innerHTML = evt.pageX + "," + evt.pageY;
    });
}

function parte3() {
    var coor = document.querySelector("#coor");
    var canvas = document.createElement("canvas");
    document.querySelector("body").appendChild(canvas);
    canvas.clientWidth = "600px";
    canvas.clientHeight = "400px";
    canvas.style.margin = "100px 100px";
    canvas.style.border = "1px solid black";
    var ctx = canvas.getContext("2d");
    window.addEventListener("mousemove",function(evt){
        ctx.fillStyle = "black";
        ctx.fillRect(evt.clientX-100, evt.clientY-100, 3 ,3);
        coor.innerHTML = (evt.clientX-100) + "," + (evt.clientY-100);
    });
}

function parte4(){
    var color = "black";
    var tamano = 3;
    document.querySelector("#tamano").addEventListener("change", function(evt){
        tamano = parseInt(evt.target.value);
    });
    document.querySelector("#rojo").addEventListener("click", function(evt){
        color = "red";
    });
    document.querySelector("#verde").addEventListener("click", function(evt){
        color = "green";
    });
    document.querySelector("#azul").addEventListener("click", function(evt){
        color = "blue";
    });
     var canvas = document.createElement("canvas");
    document.querySelector("body").appendChild(canvas);
    canvas.clientWidth = "600px";
    canvas.clientHeight = "400px";
    canvas.style.margin = "100px 100px";
    canvas.style.border = "1px solid black";
    var ctx = canvas.getContext("2d");
    window.addEventListener("mousemove",function(evt){
        ctx.fillStyle = color || "black";
        ctx.fillRect(evt.clientX-canvas.offsetLeft, evt.clientY-canvas.offsetTop, tamano, tamano);
    });

}
