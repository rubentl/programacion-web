window.addEventListener("load", function() {
    var aciertos = 0;
    var juego = 0;
    var errores = new Array(5);
    var tiempo = 60 * 3;
    var total = tiempo; // 3 minutos en segundos
    var erroresDatos = [{
        imagenes: ["fotos/1bien.png", "fotos/1mal.png"],
        coordenadas: [{
            top: 70,
            left: 120,
            width: 230,
            height: 100
        }, {
            top: 200,
            left: 320,
            width: 100,
            height: 100
        }, {
            top: 280,
            left: 200,
            width: 90,
            height: 80
        }, {
            top: 340,
            left: 140,
            width: 70,
            height: 50
        }, {
            top: 370,
            left: 55,
            width: 90,
            height: 100
        }]
    }, {
        imagenes: ["fotos/2bien.png", "fotos/2mal.png"],
        coordenadas: [{
            top: 55,
            left: 195,
            width: 50,
            height: 50
        }, {
            top: 100,
            left: 140,
            width: 100,
            height: 50
        }, {
            top: 95,
            left: 340,
            width: 90,
            height: 80
        }, {
            top: 280,
            left: 150,
            width: 70,
            height: 75
        }, {
            top: 350,
            left: 80,
            width: 70,
            height: 80
        }]
    }, {
        imagenes: ["fotos/3bien.png", "fotos/3mal.png"],
        coordenadas: [{
            top: 90,
            left: 400,
            width: 50,
            height: 50
        }, {
            top: 220,
            left: 355,
            width: 50,
            height: 50
        }, {
            top: 150,
            left: 440,
            width: 50,
            height: 50
        }, {
            top: 220,
            left: 270,
            width: 50,
            height: 70
        }, {
            top: 150,
            left: 310,
            width: 50,
            height: 50
        }]
    }];

    function activarCuentaAtras() {
        // contador de tiempo para la barra
        var id = setInterval(function() {
            if (tiempo <= 0) {
                clearInterval(id);
                mensaje("Se acabó el tiempo.");
            } else {
                tiempo -= 1;
                actualizaBarra(total, tiempo);
            };
        }, 1000);
    };

    function acierto(evt) {
        // para los click de los aciertos.
        evt.target.classList.add("acierto");
        // no quiero que pueda dar por bueno pinchar las sucesivas veces.
        evt.target.removeEventListener("click", acierto);
        // con esto corrijo lo que resta el evento del posible fallo
        tiempo += 5;
        aciertos += 1;
        if (aciertos == 5) {
            alert("¡¡Enhorabuena!!\nAcertaste todo.\n");
            aciertos = 0;
            juego += 1;
            if (juego < 3) {
                jugar(juego);
            } else {
                mensaje("Se acabó el juego.<br>BYE");
            }
        }
    };

    function error(evt) {
        tiempo -= 5;
        actualizaBarra(total, tiempo);
    };

    function asignarEventos(errores) {
        // para capturar un click equivocado
        document.querySelector("#mal").addEventListener("click", error);
        // para capturar los aciertos
        for (var i = 0; i < errores.length; i++) {
            errores[i] = document.querySelector("#error" + i);
            // los borro para que no acumulen
            errores[i].removeEventListener("click", acierto);
            errores[i].addEventListener("click", acierto);
        }
    };

    function prepararImagenes(imgs) {
        document.querySelector("#bien").style.backgroundImage =
            "url(" + imgs[0] + ")";
        document.querySelector("#mal").style.backgroundImage =
            "url(" + imgs[1] + ")";
    };

    function prepararErrores(coor) {
        // coor son las ubicaciones de las cajas de los errores
        // lleno el array con todas las cajas y su ubicación.
        for (var i = 0; i < coor.length; i++) {
            errores[i].classList.remove("acierto");
            errores[i].style.top = coor[i]['top'] + "px";
            errores[i].style.left = coor[i]['left'] + "px";
            errores[i].style.width = coor[i]['width'] + "px";
            errores[i].style.height = coor[i]['height'] + "px";
        }
    };

    function actualizaBarra(total, tiempo) {
        // La barra tiene una longitud de 1000px
        var laBarra = document.querySelector("#barra");
        // pixeles por segundo
        var factor = 1000 / total;
        var longitud = 1000 - ((total - tiempo) * factor);
        laBarra.style.width = longitud.toString() + "px";
    };

    function mensaje(texto) {
        document.querySelector(".wrapper").innerHTML = "<h1>" + texto + "</h1>";
    };

    function jugar(juego) {
        asignarEventos(errores);
        prepararImagenes(erroresDatos[juego]['imagenes']);
        prepararErrores(erroresDatos[juego]['coordenadas']);
    };

    activarCuentaAtras();
    jugar(0);
});
