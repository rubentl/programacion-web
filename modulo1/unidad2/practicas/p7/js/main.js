window.addEventListener("load", function() {

    function reloj(segundo) {
        var minutos = Math.floor(segundo / 60);
        var segundos = segundo % 60;

        //Anteponer un 0 a los minutos si son menos de 10
        minutos = minutos < 10 ? '0' + minutos : minutos;

        //Anteponer un 0 a los segundos si son menos de 10
        segundos = segundos < 10 ? '0' + segundos : segundos;

        var result = minutos + ":" + segundos;
        document.querySelector("#tiempo").innerHTML = result;
    };

    function mensaje(texto) {
        document.querySelector("#mensaje>h5").innerHTML = texto  +
                        " (" + palabras.aciertos + " de "+ 
                        (palabras.aciertos + palabras.fallos) + ")";
    };

    function Contador(segundos) {
        this.segundos = segundos;
        this.time = this.segundos;
        this.id = 0;
        this.iniciar = function() {
            reloj(this.segundos);
            this.time = this.segundos;
            this.id = setInterval(function(objeto) {
                if (objeto.time == 0) {
                    clearInterval(objeto.id);
                    palabras.fallos++;
                    mensaje("Te has quedado sin tiempo. ");
                } else {
                    objeto.time -= 1;
                    reloj(objeto.time);
                }
            }, 1000, this);
        };
        this.parar = function() {
            clearInterval(this.id);
        };
    };

    function iniciarCanvas() {
        return {
            borrar: function() {
                this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
                return this;
            },
            nuevo: function() {
                this.canvas = document.querySelector("#dibujo");
                this.ctx = this.canvas.getContext("2d");
                this.borrar();
                this.ctx.fillStyle = "#000000";
                this.ctx.fillRect(40, 20, 5, 100);
                this.ctx.fillRect(40, 20, 100, 2);
                this.ctx.fillRect(30, 120, 60, 2);
                this.ctx.lineWidth = 2;
                this.ctx.beginPath();
                this.ctx.moveTo(85, 121);
                this.ctx.lineTo(42, 80);
                this.ctx.stroke();
                this.index = 0;
                return this;
            },
            index: 0,
            siguiente: function() {
                // dibujo la siguiente pieza del ahorcado de un máximo de 10
                // controlado con la variable index.
                switch (this.index) {
                    case 0:
                        this.ctx.fillRect(140, 20, 5, 30);
                        this.index++;
                        break;
                    case 1:
                        this.ctx.beginPath();
                        this.ctx.arc(142, 58, 8, 0, 2 * Math.PI);
                        this.ctx.lineWidth = 2;
                        this.ctx.strokeStyle = '#000000';
                        this.ctx.stroke();
                        this.index++;
                        break;
                    case 2:
                        this.ctx.fillRect(141, 65, 3, 20);
                        this.index++;
                        break;
                    case 3:
                        this.ctx.beginPath();
                        this.ctx.moveTo(142, 85);
                        this.ctx.lineTo(130, 110);
                        this.ctx.stroke();
                        this.index++;
                        break;
                    case 4:
                        this.ctx.beginPath();
                        this.ctx.moveTo(143, 85);
                        this.ctx.lineTo(155, 110);
                        this.ctx.stroke();
                        this.index++;
                        break;
                    case 5:
                        this.ctx.fillRect(130, 110, 10, 1);
                        this.index++;
                        break;
                    case 6:
                        this.ctx.fillRect(155, 110, 10, 1);
                        this.index++;
                        break;
                    case 7:
                        this.ctx.fillRect(130, 66, 25, 1);
                        this.index++;
                        break;
                    case 8:
                        this.ctx.beginPath();
                        this.ctx.moveTo(130, 66);
                        this.ctx.lineTo(120, 86);
                        this.ctx.stroke();
                        this.index++;
                        break;
                    case 9:
                        this.ctx.beginPath();
                        this.ctx.moveTo(155, 66);
                        this.ctx.lineTo(165, 86);
                        this.ctx.stroke();
                        this.index = 0;
                        // devueve false en el caso de que se hay terminado
                        // de dibujar el ahorcado. Has perdido.
                        return false;
                }
                // devuelvo true si aún quedan partes por dibujar
                // si quedan intentos.
                return true;
            }
        }
    }

    function prepararEventos() {
        document.querySelector("#acciones li:nth-child(1) a")
            .addEventListener("click", function() {
                contador.parar();
                mensaje(
                    palabras.resolver(
                        prompt("¿Cuál crees que es la palabra?")
                    )
                );
                palabras.limpiar();
            });
        document.querySelector("#acciones li:nth-child(2) a")
            .addEventListener("click", function() {
                contador.parar();
                canvas.nuevo();
                palabras.nueva();
                contador.iniciar();
                mensaje("Pulsa una letra que crees contenida en la palabra")
                document.querySelector("#frase").focus();
            });
        document.querySelector("#acciones li:nth-child(3) a")
            .addEventListener("click", function() {
                alert("Pues al pulsar una tecla y no estar contenida en la palabra se irá construyendo el ahorcado.\nTienes 10 intentos\nSi no aciertas la palabra, pierdes.");
            });
        document.querySelector("body")
            .addEventListener("keydown", function(evt) {
                // compruebo que las teclas son [a-z][0-9]
                if ((evt.keyCode >= 65 && evt.keyCode <= 90) ||
                    (evt.keyCode >= 48 && evt.keyCode <= 57)) {
                    if (palabras.contiene(evt.key) == false) {
                        if (canvas.siguiente() == false) {
                            palabras.fallos++;
                            mensaje("Has cometido demasiados fallos. Has perdido");
                            palabras.limpiar();
                            contador.parar();
                        };
                    };
                };
                palabras.dibujar();
            });
    }

    function Palabra(palabra) {
        this.palabra = palabra;
        this.frase = document.querySelector("#frase");
        // creo un array para contener la tabla de letras acertadas
        this.limpiar();
    };
    Palabra.prototype.limpiar = function() {
        this.aciertos = new Array(this.palabra.length);
        this.aciertos.fill(0);
    };
    Palabra.prototype.resolver = function(esPalabra) {
        return esPalabra == this.palabra ?
            "¡Enhorabuena! Has acertado" :
            "¡Mal! No es correcta";
    };
    Palabra.prototype.contiene = function(letra) {
        // comparo la letra con cada letra de la palabra misteriosa
        // si coincide coloco 1 en el array aciertos si no 0
        this.aciertos = [].map.call(this.palabra, function(valor, idx) {
            return valor == letra ? 1 :
                this.aciertos[idx] == 1 ? 1 :
                0;
        }, this);
        return this.palabra.indexOf(letra) != -1;
    };
    Palabra.prototype.dibujar = function() {
        var acierto;
        // construyo el array para visualizar las letras acertadas y
        // las no acertadas serán una barra baja
        var aPintar = this.aciertos.map(function(valor, idx) {
            return valor == 1 ?
                this.palabra[idx] :
                "_";
        }, this);
        // Se acierta la palabra si no quedan "_" en 
        // el array de comprobacion aPintar
        if (aPintar.every(function(item) {
                return item != "_";
            })) {
            // aquí ya sé que resolver va a devolver true
            // lo que hago es recoger la frase que devuelve
            palabras.aciertos++;
            mensaje(this.resolver(aPintar.join("")));
            contador.parar();
            this.limpiar();
            acierto = true;
        }else{
            acierto = false;
        }
        this.frase.innerHTML = aPintar.join(" ");
        return acierto;
    };


    var canvas = iniciarCanvas().nuevo();
    var contador = new Contador(60 * 2);
    var palabras = {
        idx: 0,
        aciertos: 0,
        fallos : 0,
        cual: [
            "santander",
            "soria",
            "madrid",
            "pontevedra",
            "huelva",
            "sevilla",
            "malaga",
            "oviedo",
            "lerida",
            "barcelona"
        ].map(function(valor) {
            return new Palabra(valor);
        }),
        nueva: function() {
            this.idx = Math.floor(Math.random() * 10);
            this.dibujar();
        },
        limpiar: function() {
            this.cual[this.idx].limpiar();
        },
        dibujar: function() {
            // si dibujar devuelve true es que la palabra ha sido 
            // acertada y lo sumo a los aciertos
            this.aciertor += this.cual[this.idx].dibujar() ? 1 : 0;
        },
        contiene: function(letra) {
            return this.cual[this.idx].contiene(letra);
        },
        resolver: function(esPalabra) {
            return this.cual[this.idx].resolver(esPalabra);
        }
    };

    prepararEventos();

});
