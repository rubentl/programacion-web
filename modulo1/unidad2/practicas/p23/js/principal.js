window.addEventListener("load", function() {

    function Cifrado(numero, texto) {
        var num = numero || 4;
        var frase = texto || "";
        var noValidos = [",","ñ","á","é","ì","ó","ú",".","¿"];


        function grupos(str) {
            var tmp = (str.length / num) | 0;
            if ((str.length % num) != 0) tmp += 1;
            return tmp;
        }

        function rotacion(arr) {
            arr.unshift(arr.pop());
            return arr;
        }

        function rotacionArray(arr) {
            return rotacion(arr).join(" ");
        }

        function rotacionString(str) {
            var newArr = new Array(grupos(str)).fill(0);
            var strArr = str.split("");
            return newArr.map(function(item, idx, arr) {
                return rotacion(strArr.splice(0, num)).join("");
            }).join("");
        }
        this.isOk = function (frase){
            return noValidos.find(function(item){
                return frase.includes(item);}) == undefined;
        }
        this.cifrar = function(laFrase) {
            var txt = laFrase || frase;
            var result = "No es una frase válida. Consulta la documentación";
            if (this.isOk(txt)) {
                var strArray = txt.split(" ");
                var newArr = new Array(grupos(strArray)).fill(0);
                result = newArr.map(function(item, idx, arr) {
                    var palabras = rotacionArray(strArray.splice(0, num));
                    return palabras.split(" ").map(function(it, id, ar) {
                        return rotacionString(it);
                    }).join(" ");
                }).join(" ");
            }
            return result;
        }
    }
    document.entradas.addEventListener("submit", function(evt) {
        evt.preventDefault();
        var frase = document.entradas.frase.value;
        var num = document.entradas.numero.value;
        var cifrar = new Cifrado(num, frase);
        var result = cifrar.cifrar();
        document.querySelector("#resultado").innerHTML = result;
    });
});
