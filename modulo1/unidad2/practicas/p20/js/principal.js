var dados = {
    dado: [{
        ancho: 100,
        alto: 100,
        valor: 1,
        src: "imgs/1.svg"
    }, {
        ancho: 100,
        alto: 100,
        valor: 2,
        src: "imgs/2.svg"
    }, {
        ancho: 100,
        alto: 100,
        valor: 3,
        src: "imgs/3.svg"
    }, {
        ancho: 100,
        alto: 100,
        valor: 4,
        src: "imgs/4.svg"
    }, {
        ancho: 100,
        alto: 100,
        valor: 5,
        src: "imgs/5.svg"
    }, {
        ancho: 100,
        alto: 100,
        valor: 6,
        src: "imgs/6.svg"
    }],
    azar: function() {
        var num = (Math.random() * 6) | 0;
        return this.dado[num];
    }
};

function crearJugada() {
    var jugada = document.querySelector("#jugada");
    var dado = document.createElement("div");
    dado.setAttribute("id", "dado");
    jugada.appendChild(dado);
    var fichas = document.createElement("div");
    fichas.classList.add("fichas");
    jugada.appendChild(fichas);
    var ficha = new Array(6).fill(0);
    ficha.forEach(function(item, idx, arr) {
        arr[idx] = document.createElement("div");
        arr[idx].innerHTML = idx + 1;
        fichas.appendChild(arr[idx]);
    })
    return {
        dado: dado,
        fichas: ficha
    };
}

function dibujarTirada(tiro) {
    var jugada = crearJugada();
    var img1 = document.createElement("img");
    img1.src = tiro.src;
    img1.style.width = tiro.ancho + "px";
    img1.style.height = tiro.alto + "px";
    jugada.dado.appendChild(img1);
    var ficha = document.createElement("div");
    ficha.classList.add("ficha");
    jugada.fichas[tiro.valor - 1].appendChild(ficha);
}

function limpiar() {
    var id = document.querySelector("#jugada");
    id.innerHTML = "";
}

function tirar() {
    dibujarTirada(dados.azar());
}

/* 
 * Este será el tablero de juego. 
 * Al que hay que darle el lugar donde dibujarse,
 * y el número de filas y columnas. 
 * Más tarde a su creación hay que introducir los jugadores 
 * en él con el método: nuevoJugador. 
 */
function Tablero(id, filas, columnas) {
    this.tablero = new Array(filas).fill(0);
    this.id = document.querySelector(id);
    this.filas = filas;
    this.columnas = columnas;
    this.casillas = filas * columnas;
    // 1/5 de las casillas serán casillas de la muerte.
    this.muerte = new Array((this.casillas / 5) | 0).fill(0);
    this.jugadores = [];
    var _this = this;
    this.constuirTablero = function() {
        this.tablero.forEach(function(item, idx, arr) {
            arr[idx] = document.createElement("div");
            for (var i = 0; i < columnas; i++) {
                var elemen = document.createElement("div");
                elemen.innerHTML = (i + 1) + (columnas * idx);
                arr[idx].appendChild(elemen);
            };
            _this.id.appendChild(arr[idx]);
        });
    }
    this.construirMuerte = function() {
        // poner en el array el número de casillas que serán de la muerte
        this.muerte.forEach(function(item, idx, arr) {
            // hay que comprobar que no sean repetidas
            do {
                var num = (Math.random() * _this.casillas) | 0;
            } while (arr.indexOf(num) != -1);
            arr[idx] = num;
        });
    }
        // Lo usaré de constructor. 
    this.constructor = function() {
        this.constuirTablero();
        this.construirMuerte();
    };
    this.dibujarMuerte = function(){
        this.muerte.forEach(function(item,idx,arr){
            var fila = (item / _this.columnas) | 0;
            var column = item % _this.columnas;
            var padre = _this.tablero[fila].childNodes[column];
            var img = document.createElement("img");
            img.src = "imgs/calavera.jpg";
            img.style.width = ((padre.clientWidth / 3) | 0) + "px";
            img.style.height = ((padre.clientHeight /3) | 0) + "px";
            img.style.bottom = "0";
            img.style.right = "0";
            padre.appendChild(img);
        });
    };
    this.dibujarFichas = function() {
        // por supuesto, sólo hay fichas que dibujar si hay jugadores en 
        // el tablero.
        if (this.jugadores.length > 0) {
            this.limpiarFichas();
            this.dibujarMuerte();
            this.jugadores.forEach(function(item, idx, arr) {
                // si la casilla del jugador llega a la meta gana
                if (item.casilla >= _this.casillas) {
                    item.victoria();
                    return;
                };
                if (item.casilla >= 0) {
                    if (_this.muerte.indexOf(item.casilla) == -1){
                        var ficha = document.createElement("div");
                        ficha.classList.add("ficha");
                        ficha.style.background = item.color;
                        ficha.style.border = "1px solid black";
                        // calcular la fila y la columna que corresponde a la 
                        // casilla del jugador
                        var fila = (item.casilla / _this.columnas) | 0;
                        var column = item.casilla % _this.columnas;
                        var padre = _this.tablero[fila].childNodes[column];
                        // no quiero que se solapen oculten las fichas una debajo de
                        // otra.
                        ficha.style.top = 25 + (padre.children.length * 8) + "px";
                        ficha.style.left = 25 + (padre.children.length * 8) + "px";
                        // la ficha es la mitad de tamaño que su padre (la casilla)
                        ficha.style.width = ((padre.clientWidth / 2) | 0) + "px";
                        ficha.style.height = ((padre.clientHeight / 2) | 0) + "px";
                        padre.appendChild(ficha);
                    }else {
                        var mensaje = document.querySelector("#mensaje");
                        mensaje.style.color = "black";
                        mensaje.style.fontSize = "20px";
                        mensaje.innerHTML = "¡¡¡ Casilla de la muerte. Vuelves al inicio. !!!";
                        item.nuevaPartida();
                    };
                };
            });
        };
    }
    this.dibujarDado = function(elDado) {
        var id = document.querySelector("#dado");
        id.innerHTML = "";
        var img1 = document.createElement("img");
        img1.src = elDado.src;
        img1.style.width = elDado.ancho + "px";
        img1.style.height = elDado.alto + "px";
        id.appendChild(img1);
    }
    this.dibujarTirada = function(tirada) {
        this.limpiarMensaje();
        this.dibujarDado(tirada);
        this.dibujarFichas();
    }
    this.limpiarFichas = function() {
        _this.id.innerHTML = "";
        _this.constuirTablero();
    }
    this.limpiarDado = function() {
        document.querySelector("#dado").innerHTML = "";
    }
    this.limpiarMensaje = function() {
        document.querySelector("#mensaje").innerHTML = "";
    }
    this.limpiar = function() {
            _this.limpiarMensaje();
            _this.limpiarDado();
            _this.limpiarFichas();
            _this.construirMuerte();
            _this.jugadores.forEach(function(item, idx, arr) {
                item.nuevaPartida();
            });
        }
        // Añade un jugador al tablero y le decimos al jugador que 
        // está en el tablero.
    this.nuevoJugador = function(jugador) {
        this.jugadores[this.jugadores.length] = jugador;
        jugador.setTablero(this);
    }
    this.constructor();
}

/*
 * El jugador al que se le da un nombre y un color. 
 * El color se usará en la ficha y en el botón.
 * El jugador se encarga de llevar la cuenta de su casilla,
 * de crear su boton y de añadir el evento al botón, además de 
 * indicarle al tablero que la tirada se ha realizado.
 */
function Jugador(nombre, color) {
    this.color = color;
    this.nombre = nombre;
    this.casilla = -1;
    var tablero = 0;
    var _this = this;
    var boton = 0;
    this.setTablero = function(tab) {
        tablero = tab;
    }
    this.victoria = function() {
        tablero.limpiar();
        var mensaje = document.querySelector("#mensaje");
        mensaje.style.color = this.color;
        mensaje.innerHTML = "¡¡¡ " + this.nombre + " ha ganado !!!";
    }
    this.tirar = function() {
        var tirada = dados.azar();
        _this.casilla = _this.casilla + tirada.valor;
        tablero.dibujarTirada(tirada);
    };
    this.nuevaPartida = function() {
        this.casilla = -1;
    }
    this.constructor = function() {
        var botones = document.querySelector(".botones");
        boton = document.createElement("button");
        boton.style.color = this.color;
        boton.innerHTML = this.nombre;
        botones.appendChild(boton);
        boton.addEventListener("click", this.tirar);
    }
    this.constructor();
}
