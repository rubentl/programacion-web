var PopUp = function(arg) {
    var titulo = "";
    var origen = "";
    var divSalida = "";
    var texto="";
    var tipo=1;

    function mostrar() {
        poner();
        setTimeout(() => {
            divSalida.children[2].addEventListener("click",ocultar);
            divSalida.style.zIndex=100;
            divSalida.children[0].innerHTML = titulo;
            divSalida.children[1].innerHTML = texto;
            switch(tipo){
                case 1:
                    activarEfecto1();
                    break;
                case 2:
                    activarEfecto2();
                    break;
                case 3:
                    activarEfecto3();
                    break;
                case 4:
                    activarEfecto4();
                    break;
            }
        }, 100);
    }

    function ocultar(){
        switch(tipo){
            case 1:
                desactivarEfecto1();
                break;
            case 2:
                desactivarEfecto2();
                break;
            case 3:
                desactivarEfecto3();
                break;
            case 4:
                desactivarEfecto4();
                break;
        }
        divSalida.style.zIndex=1;
        divSalida.children[0].innerHTML = "";
        divSalida.children[1].innerHTML = "";
        setTimeout(() => {
          quitar()
        }, 1000);
    }

    function activarEfecto1(){
        divSalida.style.opacity = 0.8;
        divSalida.style.transform = "perspective(100px) rotateX(20deg)";
    }

    function desactivarEfecto1(){
        divSalida.style.transition="all 1s";
        divSalida.style.opacity=0;
        divSalida.style.transform = "perspective(0px) rotateX(0deg)";       
        divSalida.addEventListener("transitionend",function temporal(){
            /* volver a activar las animaciones entrada */
            divSalida.style.transition="all 2s";
            divSalida.removeEventListener("transitionend",temporal);
        });
    }


    function activarEfecto2(){
        divSalida.style.opacity = 0.8;
        divSalida.style.transform = "rotate(360deg)";   
    }

    function desactivarEfecto2(){
        divSalida.style.transition="all 1s";
        divSalida.style.opacity=0;
        divSalida.style.transform="rotate(0deg)";
        divSalida.addEventListener("transitionend",function temporal(){
            divSalida.style.transition="all 2s";
            divSalida.removeEventListener("transitionend",temporal); 
        });
    }

    function activarEfecto3(){
        divSalida.style.transition="all 1s";
        divSalida.style.transform = "scale(0,0)";       
        divSalida.addEventListener("transitionend",function temporal(){
            divSalida.style.transition="all 2s";
            divSalida.style.opacity = 0.8;
            divSalida.style.transform = "scale(1,1)";   
            divSalida.removeEventListener("transitionend",temporal);    
        });
    }

    function desactivarEfecto3(){
        divSalida.style.transition="all 1s";
        divSalida.style.opacity=0;
        divSalida.addEventListener("transitionend",function temporal(){
            divSalida.style.transition="all 2s";
            divSalida.removeEventListener("transitionend",temporal);
        });
    }

    function activarEfecto4(){
        divSalida.style.transition="all 1s";
        divSalida.style.transform = "translate(-300px,-300px)";     
        divSalida.addEventListener("transitionend",function temporal(){
            divSalida.style.transition="all 3s ease-out";
            divSalida.style.opacity = 0.8;
            divSalida.style.transform = "translate(100px,100px)";   
            divSalida.removeEventListener("transitionend",temporal);    
        });
    }

    function desactivarEfecto4(){
        divSalida.style.transition="all 1s";
        divSalida.style.opacity=0;
        divSalida.addEventListener("transitionend",function temporal(){
            divSalida.style.transition="all 3s ease-out";
            divSalida.style.transform = "translate(0px,0px)";       
            divSalida.removeEventListener("transitionend",temporal);
        });
    }
    
    function poner(){
        document.querySelector("body").appendChild(divSalida);
    }

    function quitar(){
        document.querySelector("body").removeChild(divSalida);
    }

    this.activar = function() {
        document.querySelector(origen).addEventListener("click",mostrar);
    }

    this.desactivar = function() {
        document.querySelector(origen).removeEventListener("click", mostrar);
    }

    
    function crear(){
        /* creo toda la estructura del popUp */
        /* se queda almacenada en divSalida */
        var boton=document.createElement("button");
        boton.type="button";
        boton.innerHTML="Cerrar";
        divSalida = document.createElement("div");
        divSalida.appendChild(document.createElement("header"));
        divSalida.appendChild(document.createElement("div"));
        divSalida.className="salida";
        divSalida.appendChild(boton);
    }

    this.Popup = function(arg) {
        tipo = arg.tipo || 1;
        origen = arg.origen || "origen";
        titulo = arg.titulo || "titulo";
        texto = arg.texto || "texto";
        crear();
        this.activar();
    }

    this.Popup(arg);
}