window.addEventListener("load", function() {
    var datos = {
        labels: ["Daniel", "Roberto", "Ana", "Jose", "Silvia",
                 "Jesús", "Lola", "Rosa", "Carmen", "Sara"
        ],
        datasets: [{
            label: "",
            data: [40, 10, 20, 15, 17, 25, 14, 10, 7, 19],
            backgroundColor: [
                'rgba(155, 99, 132, 0.4)',
                'rgba(54, 162, 235, 0.4)',
                'rgba(200, 206, 86, 0.4)',
                'rgba(255, 106, 86, 0.4)',
                'rgba(75, 192, 192, 0.4)',
                'rgba(153, 255, 255, 0.4)',
                'rgba(255, 159, 64, 0.4)',
                'rgba(175, 192, 192, 0.4)',
                'rgba(153, 102, 255, 0.4)',
                'rgba(255, 159, 64, 0.4)'
            ],
            borderWidth: 1,
        }]
    };

    var ctx = {
        columnas: document.querySelector("#columnas").getContext("2d"),
        barras: document.querySelector("#barras").getContext("2d"),
        lineas: document.querySelector("#lineas").getContext("2d"),
        sectores: document.querySelector("#sectores").getContext("2d"),
    };

    var charts = {
        columnas: new Chart(ctx.columnas, {
            type: "bar",
            data: datos
        }),
        barras: new Chart(ctx.barras, {
            type: 'horizontalBar',
            data: datos
        }),
        lineas: new Chart(ctx.lineas, {
            type: 'line',
            data: datos
        }),
        sectores: new Chart(ctx.sectores, {
            type: 'pie',
            data: datos
        })
    };

    /* no sé porque no me respeta las medidas del css
     * así que tengo que ponerlas así */
    charts.columnas.canvas.style.width = "900px";
    charts.barras.canvas.style.width = "900px";
    charts.lineas.canvas.style.width = "900px";
    charts.sectores.canvas.style.width = "900px";
    charts.sectores.canvas.style.height = "400px";

});
