window.onload = function() {
    var numeros = new Array(23, 45, 3, -2, 1, 3, 89, -10, 56, 6);
    var frase = "Esto es una frase para comprobar los ejercicios de javascript.";

    function pares(numeros) {
        var result = new Array();
        for (var i in numeros) {
            if (numeros[i] % 2 == 0) {
                result.push(numeros[i]);
            }
        }
        return result;
    }

    function maximo(numeros) {
        return Math.max.apply(null, numeros);
    }

    function minimo(numeros) {
        return Math.min.apply(null, numeros);
    }

    function invertido(numeros) {
        return numeros.reverse();
    }

    function entre(numeros, dentro) {
        return numeros.indexOf(dentro);
    }

    function suma(numeros) {
        var positivos = new Array();
        var negativos = new Array();

        function sumar(total, number) {
            return total + number;
        }
        for (var i in numeros) {
            if (numeros[i] < 0) {
                negativos.push(numeros[i]);
            } else {
                positivos.push(numeros[i]);
            }
        }
        return [positivos.reduce(sumar), negativos.reduce(sumar)];
    }

    function porcentajeVocal(frase) {
        var vocales = new Array(0, 0, 0, 0, 0);
        for (var i = 0; i < frase.length; i++) {
            switch (frase[i].toLowerCase()) {
                case "a":
                    vocales[0] += 1;
                    break;
                case "e":
                    vocales[1] += 1;
                    break;
                case "i":
                    vocales[2] += 1;
                    break;
                case "o":
                    vocales[3] += 1;
                    break;
                case "u":
                    vocales[4] += 1;
            }
        }
        vocales[0] = "a= " + vocales[0] + "/" + frase.length + ".";
        vocales[1] = "e= " + vocales[1] + "/" + frase.length + ".";
        vocales[2] = "i= " + vocales[2] + "/" + frase.length + ".";
        vocales[3] = "o= " + vocales[3] + "/" + frase.length + ".";
        vocales[4] = "u= " + vocales[4] + "/" + frase.length + ".";
        return vocales;
    }

    function vocal(letra) {
        return (["a", "e", "i", "o", "u"].indexOf(letra.toLowerCase()) != -1);
    }

    function vocalesConsonantes(frase) {
        var consonantes = 0;
        var vocales = 0;
        for (var i in frase) {
            if (vocal(frase[i])) {
                vocales += 1;
            } else {
                consonantes += 1;
            }
        }
        return [vocales, consonantes];
    }

    function replaceVocal(frase) {
        var vector = frase.split("");
        for (var i in vector) {
            if (vocal(vector[i])) {
                vector[i] = ".";
            }
        }
        return vector.join("");
    }
    document.querySelector("#punto1").innerHTML = pares(numeros).join(', ');
    document.querySelector("#punto1_2").innerHTML = maximo(numeros);
    document.querySelector("#punto1_3").innerHTML = minimo(numeros);
    document.querySelector("#punto2").innerHTML = invertido(numeros);
    var dentro = (entre(numeros, 5) == -1) ? "no" : "sí";
    document.querySelector("#punto3").innerHTML = "El número 5 " + dentro + " está en el array: " + numeros.join();
    dentro = (entre(numeros, 56) == -1) ? "no" : "sí";
    document.querySelector("#punto3_2").innerHTML = "El número 56 " + dentro + " está en el array: " + numeros.join();
    document.querySelector("#punto4").innerHTML = suma(numeros).join(", ");
    document.querySelector("#punto5").innerHTML = frase + "<br>" + porcentajeVocal(frase).join(", ");
    var vocales = vocalesConsonantes(frase);
    document.querySelector("#punto6").innerHTML = frase + "<br>Vocales = " + vocales[0] + "<br>Consonantes = " + vocales[1];
    document.querySelector("#punto7").innerHTML = frase + "<br>" + replaceVocal(frase);
}
