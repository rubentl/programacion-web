var PopUp = function(obj){
    var titulo = obj.titulo || "Upsss!!";
    var texto = obj.texto || "¿Para qué quieres un popUp sin texto?";
    var selector = obj.selector || "body";
    var salida = document.createElement("div");
    var tipos = [
        {nombre: "1", funcion: primera},
        {nombre: "2", funcion: segunda},
        {nombre: "3", funcion: tercera},
        {nombre: "4", funcion: cuarta}
    ];

    var generarPopUp = function(){
        var h1 = document.createElement("h1");
        var p = document.createElement("p");
        var boton = document.createElement("button");
        boton.innerHTML = "Cerrar";
        boton.type = "button";
        h1.innerHTML = titulo;
        p.innerHTML = texto;
        salida.appendChild(h1);
        salida.appendChild(p);
        salida.appendChild(boton);
        salida.classList.add("salida");
        document.body.appendChild(salida);
    }
    var activar = function(tip){
        document.querySelector(selector).addEventListener("click",function(){
            tipos.find(function(item){return item.nombre === tip;}).funcion();
        });
    }
    function primera(){
        salida.style.opacity = 0.8;
        salida.style.transform = "perspective(100px) rotateX(20deg)";
        salida.children[2].addEventListener("click", function(){
            salida.style.transition = "all 3s";
            salida.style.opacity = 0;
            salida.transform = "perspective(0px) rotateX(0deg)";
            salida.addEventListener("transitionend", function(){
                salida.style.transition = "all 2s";
            });
        });
    }
    function segunda(){
        salida.style.opacity = 0.8;
        salida.style.transform = "rotate(360deg)";
        salida.children[2].addEventListener("click",function temp(){
            salida.style.transition="all 1ms";
            salida.style.opacity=0;
            salida.style.transform="rotate(0deg)";
            salida.addEventListener("transitionend",function(){
                salida.style.transition="all 2s";
            });
        });
    }
    function tercera(){
        salida.style.transition="all 1ms";
        salida.style.transform = "scale(0,0)";
        salida.addEventListener("transitionend",function temporal(){
            salida.style.transition="all 2s";
            salida.style.opacity = 0.8;
            salida.style.transform = "scale(1,1)";
            salida.removeEventListener("transitionend",temporal);
        });
        salida.children[2].addEventListener("click",function(){
            salida.style.transition="all 1ms";
            salida.style.opacity=0;
            salida.addEventListener("transitionend",function temporal(){
                salida.style.transition="all 2s";
                salida.removeEventListener("transitionend",temporal);
            });
        });
    }
    function cuarta(){
        salida.style.transition="all 1ms";
        salida.style.transform = "translate(-300px,-300px)";
        salida.addEventListener("transitionend",function temporal(){
            salida.style.transition="all 3s ease-out";
            salida.style.opacity = 0.8;
            salida.style.transform = "translate(100px,100px)";
            salida.removeEventListener("transitionend",temporal);
        });
        salida.children[2].addEventListener("click",function(){
            salida.style.transition="all 1ms";
            salida.style.opacity=0;
            salida.addEventListener("transitionend",function temporal(){
                salida.style.transition="all 3s ease-out";
                salida.style.transform = "translate(0px,0px)";
                salida.removeEventListener("transitionend",temporal);
            });
        });
    }
    generarPopUp();
    activar(obj.tipo);
}


