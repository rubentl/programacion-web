window.addEventListener("load", function() {

    // El funcionamiento se deja en manos del css y sus animaciones. 
    // Habría que ajustar las manecillas cada cierto tiempo porque 
    // el css no es preciso y el reloj se desajusta.
    function enHora() {
        // Obtengo la hora para establecer el reloj 
        var date = new Date;
        var segundos = date.getSeconds();
        var minutos = date.getMinutes();
        var horas = date.getHours();

        // Cada manecilla con su ángulo
        var manecillas = [{
            manecilla: 'horas',
            angulo: (horas * 30) + (minutos / 2)
        }, {
            manecilla: 'minutos',
            angulo: (minutos * 6)
        }, {
            manecilla: 'segundos',
            angulo: (segundos * 6)
        }];

        // recorremos cada manecilla para establecer su ángulo
        // conforme la hora actual.
        manecillas.forEach(function(item) {
            var element = document.querySelector('.reloj .' + item.manecilla);
            element.style.transform = 'rotateZ(' + item.angulo + 'deg)';
        });
    };

    function relojDigital() {
        var date = new Date();

        [['horas', date.getHours()],
         ['minutos', date.getMinutes()],
         ['segundos', date.getSeconds()]]
            .forEach(function(elemento){
                var valor = elemento[1] < 10 ? '0' + elemento[1] : elemento[1];
                document.querySelector('.digital .' + elemento[0]).innerHTML = valor;
            });
    };

    enHora();
    // actualizar cada minuto
    // setInterval(enHora, 60000);
    relojDigital();
    setInterval(relojDigital, 1000);

});
