window.addEventListener("load", function(){
	 var menuPpal=document.querySelectorAll("#menu a");
	 for(var c=0; c<menuPpal.length; c++){
	 	menuPpal[c].addEventListener("click", cambiar);
	 }

    menuPpal[0].setAttribute("class", "current");

    crearMetodos();

	ponerEncabezado("inicio");
	ponerNoticias("inicio");

});

function cambiar(){
	/* recoger texto del enlace pulsado y ponerle guión bajo si es mayor de 1 palabra */
	var txt=event.target.innerHTML.toLowerCase();
	txt=txt.split(" ");
	if(txt.length>1){
		txt[0]=txt[0] + "_";
	}
	txt=txt.join("");

    //Poner class current a pestaña del menú activa
	document.querySelectorAll("#menu a").forEach(function(item){
		item.removeAttribute("class", "current");
		});
	event.target.parentNode.setAttribute("class", "current")

	ponerEncabezado(txt);
	ponerNoticias(txt);

}

function crearMetodos(){

    datos.encabezado = function(pagina){
        return this.getPagina(pagina).filter(function(item){
            return (item.tipos == "Encabezado");
        })[0];
    }
    datos.getPagina = function(pagina){

        return datos.filter(function(item){
            return (item.pagina == pagina);
        })
    }

    datos.getMenus = function(){
        var menus = datos.map(function(item){
           return item.pagina;
        });
        var unico = menus.filter(function(item, pos, arr) {
            return (arr.indexOf(item) == pos);
            });
        return unico.map(function(item){
            var tmp = item.split('_');
            return (tmp.length > 1) ? tmp[1] : tmp[0];
        });

    }

      datos.noticias = function(pagina) {
        var noticias = this.getPagina(pagina).filter(function(item) {
            return (item.tipos == "Noticias");
        });

        noticias.forEach(function(item, idx, arr) {
            item.grande = function() {
                return (item.texto.length > 40);
            }
            item.miniTexto = function() {
                return item.texto.slice(0,39) + " ...";
            }
        });
        return noticias;
    }
}

function lightBox(obj){
    var img = document.createElement("img");
    img.src = obj.foto_grande;
    var txt = document.createElement("p");
    txt.innerHTML = obj.texto;
    var titulo = document.createElement("h3");
    titulo.innerHTML = obj.titulo;
    var cerrar = document.createElement("button");
    cerrar.innerHTML = "Cerrar";
    var div = document.createElement("div");
    div.appendChild(titulo);
    div.appendChild(img);
    div.appendChild(txt);
    div.appendChild(cerrar);
    var contenedor = document.createElement("div");
    contenedor.appendChild(div);
    cerrar.addEventListener("click", function(event){
        event.preventDefault();
        document.querySelector("body").removeChild(contenedor);
    })
    contenedor.style.background = "rgba(0,0,0,0.8)";
    contenedor.style.position = "fixed";
    contenedor.style.top = "0";
    contenedor.style.left = "0";
    contenedor.style.width = "100%";
    contenedor.style.height = "100%";
    contenedor.style.zIndex = "1000";
    contenedor.style.display = "flex";
    contenedor.style.justifyContent = "center";
    contenedor.style.alignItems = "center";
    div.style.margin = "0 auto";
    div.style.width = "30%";
    img.style.width = "100%";
    txt.style.textAlign = "justify";
    txt.style.fontSize = "20px";
    titulo.style.color = "white";
    titulo.style.textAlign = "center";
    titulo.style.fontSize = "30px";
    cerrar.style.fontSize = "15px"
    cerrar.style.padding = "10px";
    cerrar.style.cursor = "pointer";
    return contenedor;
}



function ponerEncabezado(arg){
	var encabezado=datos.encabezado(arg);
	document.querySelector("#pitch h1").innerHTML=encabezado.titulo;
	document.querySelector("#pitch p").innerHTML=encabezado.texto;

}

function ponerNoticias(arg){
	var noticias=datos.noticias(arg);
	var enlace=document.querySelectorAll(".more a");
    enlace.forEach(function(item){item.parentNode.innerHTML=""});

	noticias.forEach(function(item,idx,arr){
		document.querySelectorAll(".column h3")[idx].innerHTML=item.titulo;
		document.querySelectorAll(".column img")[idx].src=item.foto;
		 if (item.grande()) {
        	document.querySelectorAll(".column p:first-of-type")[idx].innerHTML=item.miniTexto();
             var enlace = document.createElement("a");
             enlace.innerHTML = "leer más ...";
             enlace.href = "#";
             document.querySelectorAll(".more")[idx].appendChild(enlace);
            var leerMas = function(evt){
                document.querySelector("body").appendChild(lightBox(item));
                evt.preventDefault();
            }
        	enlace.addEventListener("click",leerMas);
   		 }else{
   		 	document.querySelectorAll(".column p:first-of-type")[idx].innerHTML=item.texto;
   		 }

	});

}

