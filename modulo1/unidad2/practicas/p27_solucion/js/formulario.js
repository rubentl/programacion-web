window.addEventListener("load", function() {
    var doc = document;
    var form = {
        texto: doc.querySelector("#texto"),
        foto: doc.querySelector("#foto"),
        foto_grande: doc.querySelector("#foto_grande"),
        titulo: doc.querySelector("#titulo"),
        tipos: doc.querySelector("#tipos"),
        enlace: doc.querySelector("#enlace"),
        pagina: doc.querySelector("#pagina"),
        ocultar: function(campo) {
            this[campo].style.display = "none";
            this[campo].removeAttribute("required");
            doc.querySelector('label[for="' + campo + '"]').style.display = "none";
            return this;
        },
        mostrar: function(campo) {
            this[campo].style.display = "inline-block";
            if (campo !== "texto")
                this[campo].setAttribute("required", "true");
            doc.querySelector('label[for="' + campo + '"]').style.display = "inline-block";
            return this;
        },
        mostrarMenu: function() {
            this.iniciar()
                .mostrar("texto").mostrar("enlace");
            tinymce.init({
                selector: '#texto'
            });
            return this;
        },
        mostrarEncabezado: function() {
            this.iniciar()
                .mostrar("titulo").mostrar("texto");
            tinymce.init({
                selector: '#texto'
            });
            return this;
        },
        mostrarNoticias: function() {
            this.iniciar()
                .mostrar("titulo").mostrar("texto")
                .mostrar("foto").mostrar("foto_grande");
            tinymce.init({
                selector: '#texto'
            });
            return this;
        },
        mostrarPie: function() {
            this.iniciar()
                .mostrar("titulo").mostrar("enlace")
                .ocultar("texto");
            return this;
        },
        iniciar: function() {
            this.ocultar("texto").ocultar("foto")
                .ocultar("foto_grande").ocultar("titulo")
                .ocultar("enlace");
            this.tipos.setAttribute("required", "true");
            this.pagina.setAttribute("required", "true");
            tinymce.remove('#texto');
            return this;
        },
    }

    form.iniciar().mostrarMenu();
    form.tipos.addEventListener("change", function(evt) {
        var idx = evt.target.selectedIndex;
        var opt = evt.target.options.item(idx);
        switch (opt.value) {
            case "m":
                form.mostrarMenu();
                break;
            case "e":
                form.mostrarEncabezado();
                break;
            case "n":
                form.mostrarNoticias();
                break;
            case "p":
                form.mostrarPie();
                break;
        }
    });

    document.querySelector("#entradas").addEventListener("submit", function(evt) {
        evt.preventDefault();
        var txt = tinymce.activeEditor.getContent({
            format: 'text'
        });
        if (form.foto.value = "") form.foto.value = "defecto.jpg";
        if (form.foto_grande.value = "") form.foto_grande.value = "defecto.jpg";
        if ((txt != "") && (txt != "\n") && (txt != "\r")) {
            doc.querySelector("#entradas").submit();
        } else {
            var texto = doc.querySelector('label[for="texto"]');
            form.texto.classList.add("error");
            texto.classList.add("error")
            texto.innerHTML = "Este campo no puede estar vacio";
        }
    })

});
