function reloj(segundo) {
    var minutos = Math.floor(segundo / 60);
    var segundos = segundo % 60;

    //Anteponer un 0 a los minutos si son menos de 10 
    minutos = minutos < 10 ? '0' + minutos : minutos;

    //Anteponer un 0 a los segundos si son menos de 10 
    segundos = segundos < 10 ? '0' + segundos : segundos;

    var result = minutos + ":" + segundos;
    document.querySelector("#tiempo").innerHTML = result;
};

function sinTiempo(){
    document.querySelector("#encuesta").classList.add("oculto");
    alert("Se te acabó el tiempo.\nAún así corregiré lo que has respondido.");
    corregir();
};

function corregir(){
    document.querySelector("#encuesta").classList.add("oculto");
    document.querySelector("#tiempo").classList.add("oculto");
    document.querySelector("#aviso").classList.add("oculto");
   
    var mensaje = document.querySelector("#mensaje");
    var solucion = new Array(3,1,3,1,3,2,1,3,4,2);
    var correctas = new Array();
    var incorrectas = new Array();
    var contestadas = 0;
    var puntuacion = 0;

    // recorro las soluciones y las compararé con las respuestas
    for (var i=0;i<solucion.length;i++){
        var radios = document.getElementsByName(i+1);
        for (var j=0;j<radios.length;j++){
            // Sólo me interesa las opciones seleccionadas
            if (radios[j].checked){
                var valor = radios[j].value;
                // ¿Es una respuesta correcta?
                if (valor == solucion[i]){
                    // Pongo la pregunta contestada correctamente
                    correctas.push(i+1);
                    break;
                }else{
                    // Pongo la pregunta que se ha fallado
                    incorrectas.push(i+1);
                    break;
                }
            }
        }
    }

    contestadas = correctas.length + incorrectas.length;
    // La puntación son las correctas menos un cuarto de las incorrectas :-)
    puntuacion = correctas.length - (incorrectas.length / 4);
    // No quiero puntuaciones negativas, como mucho un cero patatero :-)
    if (puntuacion < 0) puntuacion = 0;

    // Visualización de los resultados en formato tabla.
    // Uso un array por comodidad del push y luego el join.
    var resultados = new Array();
    resultados.push("<h2>RESULTADOS</h2>");
    resultados.push("<table><tbody>");
    resultados.push("<tr><td>Respuestas contestadas:</td><td>"+contestadas+"</td></tr>");
    resultados.push("<tr><td>Número de respuestas correctas:</td><td>"+correctas.length+"</td></tr>");
    resultados.push("<tr><td>Número de respuestas incorrectas:</td><td>"+incorrectas.length+"</td></tr>");
    resultados.push("<tr><td>Puntuación total del test:</td><td>"+puntuacion+"</td></tr>");
    resultados.push("<tr><td>Preguntas acertadas:</td><td>"+correctas.join(", ")+"</td></tr>");
    resultados.push("<tr><td>Preguntas erróneas:</td><td>"+incorrectas.join(", ")+"</td></tr>");
    resultados.push("</tbody></table>");
    mensaje.innerHTML = resultados.join("");
    mensaje.classList.add("visible");
};

function inicio() {
    // Oculto el div que usaré para presentar los resultados
    document.querySelector("#mensaje").classList.add("oculto");

    // Este será el contador hacia atrás de 5 minutos
    var time = 60 * 5; 

    // Guardo el id de la funcion para parar el contador
    var id = setInterval(function() {
        if (time == 0) {
            clearInterval(id);
            sinTiempo();
        } else {
            time -= 1;
            reloj(time);
        }
    }, 1000);

    // Evento para corregir el cuestionario
    document.querySelector("#corregir").addEventListener("click", corregir);
};

window.addEventListener("load", inicio);
