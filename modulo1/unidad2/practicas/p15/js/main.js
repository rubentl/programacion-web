window.addEventListener("load", function() {
    var imgs = {
        src: [
            ["imgs/frutas/1.svg","cereza"],
            ["imgs/frutas/2.svg","piña"],
            ["imgs/frutas/3.svg","limon"],
            ["imgs/frutas/4.svg","fresa"],
            ["imgs/frutas/5.svg","platano"],
            ["imgs/frutas/6.svg","naranja"],
            ["imgs/frutas/7.svg","sandia"],
            ["imgs/frutas/8.svg","uva"]
        ],
        cara: {
            normal: "imgs/face-plain.svg",
            triste: "imgs/face-sad.svg",
            alegre: "imgs/face-smile.svg",
        },
        random: function() {
            var idx = Math.floor((Math.random() * this.frutas.length));
            return this.frutas[idx];
        },
        frutas: [],
        precarga: function() {
            _this = this;
            this.frutas = new Array(this.src.length).fill(0);
            this.frutas.forEach(function(item, idx, arr) {
                arr[idx] = new Image();
                arr[idx].src = _this.src[idx][0];
                arr[idx].tipo = _this.src[idx][1];
            });
        }
    };

    var Juego = function() {
        var moneda = 0;
        var apuesta = 0;
        var _this = this;
        this.cara = function(laCara) {
            document.querySelector("#carita>img").src = laCara;
        };
        this.setMoneda = function() {
            moneda++;
            document.querySelector("#moneda").innerHTML = moneda.toString();
            _this.cara(imgs.cara.normal);
            return this;
        };
        this.getMoneda = function() {
            return moneda;
        };
        this.setApuesta = function() {
            if (moneda > 0){
                apuesta++;
                moneda--;
                document.querySelector("#apuesta").innerHTML = apuesta.toString();
                document.querySelector("#moneda").innerHTML = moneda.toString();
                _this.cara(imgs.cara.normal);
            };
            return this;
        };
        this.getApuesta = function() {
            return apuesta;
        };
        this.resetApuesta = function(){
            apuesta = 0;
            document.querySelector("#apuesta").innerHTML = apuesta.toString();
            return this;
        };
        this.nuevaJugada = function(){
            var jugada = new Array(imgs.random(),imgs.random(),imgs.random());
            document.querySelector("#img1>img").src = jugada[0].src;
            document.querySelector("#img2>img").src = jugada[1].src;
            document.querySelector("#img3>img").src = jugada[2].src;
            return [jugada[0].tipo,jugada[1].tipo,jugada[2].tipo];
        };
        this.premio = function(obj){
            function cuantasCerezas(arr){
                return arr.reduce(function(total,item){
                    return (item == "cereza")
                        ? total += 1
                        : total ;
                    },0);
                };
            function tresFrutas(arr){
                var result = 0;
                if ((arr[0] == arr[1]) &&
                    (arr[1] == arr[2])){
                    result = 5;
                };
                return result;
            };
            function dosFrutas(arr){
                var result = 0;
                if ((arr[0] == arr[1]) ||
                    (arr[0] == arr[2]) ||
                    (arr[1] == arr[2])){
                    result = 2;
                };
                return result;
            };
            var ganado = 0;
            switch(cuantasCerezas(obj.jugada)){
                case 3:
                    ganado = 10;
                    break;
                case 2:
                    ganado = 4;
                    break;
                case 1:
                    ganado = 1;
                    ganado += dosFrutas(obj.jugada);
                    break;
                case 0:
                    if (tresFrutas(obj.jugada) == 5){
                        ganado = 5;
                    }else if (dosFrutas(obj.jugada) == 2) {
                        ganado = 2;
                    };
            };
            console.log(ganado);
            return ganado * obj.apuesta;
        };
        this.jugar = function(){
            document.querySelector("#resultado").innerHTML = "";
            if (_this.getApuesta() > 0){
                var resultado = _this.nuevaJugada();
                var ganado = _this.premio({jugada: resultado, apuesta: _this.getApuesta()});
                _this.resetApuesta();
                if (ganado == 0){
                    _this.cara(imgs.cara.triste);
                } else{
                    document.querySelector("#resultado").innerHTML = ganado.toString();
                    for (var i = 0; i <= ganado; i++){
                        _this.setMoneda();
                    }
                    _this.cara(imgs.cara.alegre);
                };
            };
            return _this;
        };
        document.querySelector("#btMoneda").addEventListener("click", this.setMoneda);
        document.querySelector("#btApuesta").addEventListener("click", this.setApuesta);
        document.querySelector("#btJugar").addEventListener("click", this.jugar);
    };

    imgs.precarga();
    var juego = new Juego();
});
