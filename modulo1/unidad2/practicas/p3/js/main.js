window.onload = function() {
    var wallys = document.querySelectorAll(".wallys");
    var contador = 0;

    function exito(evt) {
        evt.target.style.border = "2px solid white";
        if (contador == 0) {
            alert("¡Muy bien!");
            contador += 1;
        } else {
            alert("Ya está, pasa al siguiente.");
        }
    }
    for (var i = 0; i < wallys.length; i++) {
        wallys[i].addEventListener("click", exito);
    }
};
