$(document).ready(function() {


    $('a.smooth').on('click', function(e) {
        e.preventDefault();
        let anchor = $(this).attr('href');
        $('html, body').stop().animate({
            scrollTop: $(anchor).offset().top);
        }, 1500, 'easeOutBack');
    });


});
