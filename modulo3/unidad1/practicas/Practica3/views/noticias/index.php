<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Listado de noticias';

echo '<table class="table table-striped table-hover">';
echo '<thead><th>ID</th><th>Titulo</th><th>Texto</th></thead>';
echo '<tbody>';

echo '<h1>' .  Html::encode($this->title) .  '</h1>';

echo '<p>' 
    . Html::a('Nueva Noticia', ['create'], ['class' => 'btn btn-success'])
    . '</p>';

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'pager' => ['hideOnSinglePage'=>true],
    'summary' => '',
    'itemView' => '_index'
]);
echo '</tbody></table>';
