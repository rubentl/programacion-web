<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Noticias */

$this->title = $model->id;
echo $this->render('index', [
    'dataProvider' => $dataProviderIndex,
]);
?>
<div class="noticias-view col-sm-10 col-sm-offset-1">

    <h3><?= Html::encode('Categoría: ' . $model->Categoria) ?></h3>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'Titulo',
            'Texto',
        ],
        'summary' =>''
    ]); ?>

</div>
