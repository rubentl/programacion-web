<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Marcadores';

echo '<table class="table table-striped table-hover">';
echo '<thead><th>ID</th><th>Enlace</th><th>Descripción</th></thead>';
echo '<tbody>';

    echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => $vista,
            'pager' => ['hideOnSinglePage'=>true],
            'summary' => ''
        ]); 

    echo '</tbody></table>';
