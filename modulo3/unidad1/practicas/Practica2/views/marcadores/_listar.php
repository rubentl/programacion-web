<?php

use yii\bootstrap\Modal;

    $arr_tmp = explode('.', $model->enlace, -1);
    $enlace = $arr_tmp[count($arr_tmp) -1];

echo '<tr><td>' . $model->id . '</td>';
echo '<td><a href="' . $model->enlace . '" target="_blank">' . $enlace . '</a></td>';
echo '<td>';

Modal::begin([
    'header' => '<h2>' . $model->descripcion_corta . '</h2>',
    'toggleButton' => ['label'=>$model->descripcion_corta, 'class' => 'btn-link '],
    'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>'
            . '<a class="btn btn-primary" href="' . $model->enlace . '" target="_blank">Visitar</a>'
]); 
    
echo $model->descripcion_larga;

Modal::end();
    
echo '</td></tr>';

