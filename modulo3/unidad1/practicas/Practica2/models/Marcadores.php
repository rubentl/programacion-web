<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marcadores".
 *
 * @property string $id
 * @property string $publico
 * @property string $enlace
 * @property string $descripcion_larga
 * @property string $descripcion_corta
 */
class Marcadores extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        // return 'marcadores';
        return 'Marcadores';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['enlace', 'descripcion_larga', 'descripcion_corta'], 'required'],
            [['descripcion_larga'], 'string'],
            [['publico'], 'string', 'max' => 2],
            [['enlace', 'descripcion_corta'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'publico' => 'Publico',
            'enlace' => 'Enlace',
            'descripcion_larga' => 'Descripcion Larga',
            'descripcion_corta' => 'Descripcion Corta',
        ];
    }

    /**
     * @inheritdoc
     * @return MarcadoresQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MarcadoresQuery(get_called_class());
    }
}
