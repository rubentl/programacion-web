<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entradas".
 *
 * @property int $id
 * @property string $Titulo
 * @property string $Texto
 * @property string $Foto
 * @property bool $portada
 */
class Entradas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entradas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['portada'], 'boolean'],
            [['Titulo', 'Texto'], 'required', 'message' => 'El {attribute} es obligatorio'],
            [['Titulo','Texto', 'Foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Titulo' => 'Titulo de la noticia',
            'Texto' => 'Texto',
            'Foto' => 'Foto',
            'portada' => 'Portada',
        ];
    }

    /**
     * @inheritdoc
     * @return EntradasQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EntradasQuery(get_called_class());
    }
}
