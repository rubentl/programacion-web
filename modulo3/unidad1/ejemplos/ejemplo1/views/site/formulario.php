<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin()
?>
<?= $form->field($model, 'name') ?>
<?= $form->field($model, 'age') ?>
<?= $form->field($model, 'email') ?>
<?= $form->field($model, 'subject') ?>
<?= $form->field($model, 'body') ?>

<div class="form-group">
  <div class="col-lg-offset-1 col-lg-11">
    <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
  </div>
</div>
<?php ActiveForm::end() ?>

