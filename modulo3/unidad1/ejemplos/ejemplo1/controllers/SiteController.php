<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\ContactForm;


class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index',[
            'mensaje' => 'Estoy probando los parametros'
        ]);
    }

    public function actionContacto(){
      $modelo = new ContactoForm();
      if($modelo->load(Yii::$app->request->get())){
        return $this->render("mostrar",[
           "datos"=>$modelo
        ]);
      }else{
        return $this->render("formulario",[
            'model' => $modelo
        ]);
      }
    }
}
