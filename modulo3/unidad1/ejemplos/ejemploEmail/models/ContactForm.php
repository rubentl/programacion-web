<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $email;
    public $subject;
    public $body;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['email', 'subject', 'body'], 'required', 'message'=>'Colega, {attribute} no puede estar vacío. ¡Concéntrate!'],
            // email has to be a valid email address
            ['email', 'email', 'message'=>'El correo debe estar bien formado'],
            ['email', 'string', 'max'=>30, 'message'=>'el correo debe ser menor de 31 caracteres'],
            [['subject', 'body'], 'string']
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Correo electrónico',
            'subject' => 'Asunto',
            'body' => 'Mensaje'
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo('clasealpeformacion@gmail.com')
                ->setFrom('clasealpeformacion@gmail.com')
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();

            return true;
        }
        return false;
    }
}
