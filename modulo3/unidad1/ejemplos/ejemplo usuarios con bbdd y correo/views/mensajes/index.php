<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mensajes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mensajes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Mensajes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?php 
$columns = [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'usuario',
            'mensajes:ntext',
        ];

if (!Yii::$app->user->isGuest){
    
    $columns[] = ['class' => 'yii\grid\ActionColumn'];
}

echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' =>  $columns,    
    ]); ?>
</div>
