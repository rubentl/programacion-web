<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mensajes".
 *
 * @property int $id
 * @property string $usuario
 * @property string $mensajes
 */
class Mensajes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mensajes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mensajes'], 'required', 'message'=>"Por dios, rellena el {attribute}"],
            [['mensajes'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario' => 'Usuario',
            'mensajes' => 'Mensajes',
        ];
    }

    /**
     * @inheritdoc
     * @return MensajesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MensajesQuery(get_called_class());
    }

    public function save($runValidation = true, $attributeNames = NULL){
        $this->usuario = Yii::$app->user->identity->usuario;
        return parent::save($runValidation = true, $attributeNames = NULL);
    }
}
