<?php

use yii\helpers\Html;

?>

    <form action="<?= yii\helpers\Url::to(['site/formulario']) ?>" method="post">
        <div class="form-group">
        <label for="iusuario">Nombre</label>
        <input type="text" class="form-control" id="iusuario" name="usuario">
        </div>
        <button class="btn btn-info">Enviar</button>
    </form>
