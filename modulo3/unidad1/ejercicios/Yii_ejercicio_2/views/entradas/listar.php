
<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Listado de Entradas';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Hoja de Ejercicios</h1>

        <p class="lead">El listado de entradas se debería ver aquí.</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center">
                <?php
                foreach($model as $obj){
                    echo "<h3>$obj->Titulo</h3>";
                    echo "<p>$obj->Texto</p>";
                }
                ?>
            </div>
        </div>

    </div>
</div>
