<?php
spl_autoload_register(
    function ($clase){
        require_once 'Clases\\' . $clase . '.php';
    }
);
?>
<!DOCTYPE html>

<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title>Ejercicio 5</title>
  </head>
  <body>
<?php
$padre = new Persona("ramón", "abramo", 35);
$hijo = $padre;
$hija = clone $padre;
$hijo->setEdad(100);
$hija->setEdad(50);
var_dump($hijo);
var_dump($padre);
var_dump($hija);
?>
  </body>
</html>
