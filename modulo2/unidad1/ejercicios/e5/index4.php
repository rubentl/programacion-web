<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 5</title>
</head>
<body>
<?php
class Cadena{
    private $valor;
    private $longitud;
    private $vocales;

    function __construct($valor){
        $this->setValor($valor);
    }

    public function getValor($minusculas = FALSE){
        return ($minusculas) 
            ? strtolower($this->valor)
            : $this->valor;
    }

    public function getLongitud(){
        $this->calcularLongitud();
        return $this->longitud;
    }

    public function getVocales(){
        $this->numeroVocales();
        return $this->vocales;
    }

    public function setValor($valor){
        $this->valor = $valor;
        return $this;
    }

    public function setLongitud($longitud){
        $this->longitud = $longitud;
        return $this;
    }

    public function setVocales($vocales){
        $this->vocales = $vocales;
        return $this;
    }

    private function calcularLongitud(){
        $this->setLongitud(strlen($this->valor));
        return $this;
    }

    private function numeroVocales(){
        $vocales = array('a', 'e', 'i', 'o', 'u');
        $longitud = 0;
        foreach ($vocales as $valor) {
            $longitud += substr_count($this->getValor(1), $valor);
        }
        $this->setVocales($longitud);
    }

    public function repeticionVocal($vocal){
        $longitud = 0;
        $longitud += substr_count($this->getValor(1), $vocal);
        return $longitud;
    }
}

$texto = new Cadena("Don Quijote de la Mancha");
echo 'Las vocales de "'. $texto->getValor() .'" son ' . $texto->getVocales() . '.<br>';
echo "Y su longitud es " . $texto->getLongitud() . ".<br>";
?>
</body>
</html>
