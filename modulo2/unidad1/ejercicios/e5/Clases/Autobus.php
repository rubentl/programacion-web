<?php

/**
 * Description of Autobus
 *
 * @author ruben
 */
class Autobus extends Vehiculo {
    private $pasajeros;

    public function subir_pasajeros($cantidad){
        $this->pasajeros = $cantidad;
        echo 'Se han subido '. $cantidad . ' pasajeros<br>';
        return $this;
    }

    public function verificar_encendido(){
        if ($this->encendido == true){
            echo 'Autobús encendido';
        }else{
            echo 'Autobús apagado';
        }
        echo '<br>';
        return $this;
    }
}
