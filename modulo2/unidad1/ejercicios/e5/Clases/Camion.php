<?php

/**
 * Description of Camion
 *
 * @author ruben
 */
class Camion extends Vehiculo {
    private $carga;

    public function cargar($cantidad){
        $this->carga = $cantidad;
        echo 'Se ha cargado la cantidad ' . $cantidad . '<br>';
        return $this;
    }
    public function verificar_encendido(){
        if ($this->encendido == true){
            echo 'Camión encendido';
        }else{
            echo 'Camión apagado';
        }
        echo '<br>';
        return $this;
    }
}
