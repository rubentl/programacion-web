<?php
spl_autoload_register(
    function ($clase){
        require_once 'Clases\\' . $clase . '.php';
    }
);
?>
<!DOCTYPE html>

<html lang="es">
  <head>
    <meta charset="UTF-8">
    <title>Ejercicio 5</title>
  </head>
  <body>
<?php
$camion = new Camion();
$camion->matricula = 'MDU - 293';
$camion->encender()
    ->cargar(10)
    ->verificar_encendido()
    ->apagar();

$autobus = new Autobus();
$autobus->matricula = 'KDF - 923';
$autobus->encender()
    ->subir_pasajeros(5)
    ->verificar_encendido()
    ->apagar();
?>
  </body>
</html>
