<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Ejercicio 5</title>
</head>
<body>
<?php
class Persona{
    private $nombre;
    
    public function dormir(){
        echo 'Estoy durmiendo. ¡No molestar!<br>';
        return $this;
    }
    public function hablar(){
        echo 'Soy ' . $this->nombre . '. El durmiente feliz<br>';
        return $this;
    }
    public function contar(){
        echo 'Sé contar hasta 10: ';
        for($i=1;$i<=10;$i++){
            echo $i . ' ';
        }
        echo '<br>';
        return $this;
    }
    public function adquirirNombre($nombre){
        $this->nombre = $nombre;
        return $this;
    }
    public function decirNombre(){
        echo $this->nombre . '<br>';
        return $this;
    }
}
$yo = new Persona();
$yo->adquirirNombre('Rubén')
    ->hablar()
    ->contar()
    ->decirNombre()
    ->dormir();
?>
</body>
</html>

