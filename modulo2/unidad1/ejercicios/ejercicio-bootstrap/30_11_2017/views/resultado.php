<?php

function resultado($resultados) {
  if (isset($resultados['anadir'])) {
    correcto($resultados);
  } else {
    error($resultados);
  }
}

function correcto($resultados) {
  $val = [];
  unset($resultados["anadir"]);
  foreach ($resultados as $indice => $valor) {
    array_push($val, "<li>$indice: $valor</li>");
  }
  $resultados = "Todo ha ido bien.<br>Gracias por participar."
          . '<div class="container">'
          . '<ul>'
          . implode("", $val)
          . "</ul></div>";
  ventana($resultados);
}

function error($errores) {
  ventana(implode("<br>", $errores));
}

function ventana($contenido) {
  mensaje($contenido, "Resultados");
}
