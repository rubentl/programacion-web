<?php

function crearFormulario($datos) {
  echo '<form class="form-horizontal">';
  foreach ($datos as $key => $value) {
    control($key, $value);
  }
  echo "</form>";
}

function control($k, $v) {
  $nombre = $k;
  $tipo = "text";
  $placeholder = " ";
  $id = " ";
  $texto = " ";
  extract($v);
  if ($tipo != "submit") {
    echo '<div class="form-group">';
    echo "<label for=\"$id\" class=\"col-sm-2 control-label\">$texto</label>";
    echo '<div class="col-sm-10">';
    echo "<input type=\"$tipo\" class=\"form-control\" id=\"$id\" name=\"$nombre\" placeholder=\"$placeholder\">";
    echo "</div></div>";
  } else {
    echo '<div class="form-group">';
    echo '<div class="col-sm-offset-2 col-sm-10">';
    echo "<button name=\"$nombre\" type=\"$tipo\" class=\"btn btn-default\">$texto</button>";
    echo "</div></div>";
  }
}

function mensaje($mensaje, $titulo) {
  echo '<div class="modal fade" tabindex="-1" role="dialog" id="validacion">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">' . $titulo . '</h4>
      </div>
      <div class="modal-body">
        <p>' . $mensaje . '</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>';
}

function listado($registros, $clave) {

  echo '<div class="container"><ul class="list-group">';

  foreach ($registros as $key => $value) {
    echo '<li class="list-group-item">' . "$key: $value"
    . '<button type="submit" class="close" name="' . $clave . '" aria-label="Close">'
    . '<span aria-hidden="true">&times;</span></button>'
    . '</li>';
  }
  echo '</ul></div>';
}
