<?php
session_start();
if (!isset($_SESSION["registros"])) {
  $_SESSION["reistros"] = [];
}
include "componentes/componentes.php"
?>
<!doctype html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">

    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
  </head>
  <body>
    <?php

    function accionIndex() {
      include "views/formulario.php";
      include "views/registros.php";
    }

    function accionRegistrar() {
      $nombre = $_REQUEST["nombre"];
      $fecha = $_REQUEST["fecha"];
      array_push($_SESSION["registros"], [
          "Nombre" => $nombre,
          "Fecha de Nacimiento" => $fecha
      ]);
    }

    function accionComprobar() {
      include "views/resultado.php";

      $errores = [];
      $hoy = date("Y");
      $anno_fecha = explode("-", $_REQUEST["fecha"])[0];

      if (strlen($_REQUEST["nombre"]) == 0 || strlen($_REQUEST["nombre"]) > 5) {
        $errores[] = "El nombre esta mal";
      }

      if ($anno_fecha == $hoy || $anno_fecha <= $hoy - 10) {
        $errores[] = "La fecha es incorrecta";
      }

      if (count($errores) == 0) {
        accionRegistrar();
        $resultados = $_REQUEST;
      } else {
        $resultados = $errores;
      }
      resultado($resultados);
    }

    if (isset($_REQUEST["anadir"])) {
      accionIndex();
      accionComprobar();
    } else {
      accionIndex();
    }

    if (isset($_SESSION["registros"])) {
      registros($_SESSION["registros"]);
    }
    ?>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.min.js"></script>

    <script src="js/main.js"></script>
  </body>
</html>
