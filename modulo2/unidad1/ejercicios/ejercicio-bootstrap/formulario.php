<?php

function formulario() {
  return crear_formulario([
      inicio_form([
          "archivo" => "index_1.php",
          "metodo" => "get",
          "nombre" => "formulario",
          "clase" => "form-horizontal"
      ]), crear_input([
          "tipo" => "text",
          "nombre" => "nombre",
          "id" => "nombre",
          "texto" => "Nombre"
      ]), crear_input([
          "tipo" => "date",
          "nombre" => "fecha",
          "id" => "fecha",
          "texto" => "Fecha de Nacimiento"
      ]), crear_boton([
          "nombre" => "anadir",
          "texto" => "Añadir",
          "clase" => "btn btn-default"
      ]), final_form()]
  );
}
