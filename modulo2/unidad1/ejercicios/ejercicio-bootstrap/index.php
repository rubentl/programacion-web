<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <style>
      body {
        padding-top: 50px;
        padding-bottom: 20px;
      }
    </style>
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
  </head>
  <body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div class="container">
      <div class="page-header text-center">
        <h1>Ejemplo de Formulario y php en la misma página</h1>
      </div>
    </div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <?php if (key_exists("enviar", $_GET)) : ?>
      <div class="jumbotron">
        <div class="container">
          <?php
          echo "<dl class=\"dl-horizontal\">";
          foreach ($_GET as $key => $value) {
            if ($key != "enviar") {
              echo "<dt>$key</dt><dd>$value</dd>";
            }
          }
          echo "</dl>";
          ?>
        </div>
      </div>
    <?php else : ?>
      <div class="container">
        <div class="row">
          <form name="formulario" action="index.php" method="get" class="form-horizontal">

              <div class="form-group">
                <label for="nombre" class="control-label col-xs-12 col-sm-2" >Nombre</label> 
                <div class=" col-xs-12 col-sm-10">
                  <input type="text" name="nombre" id="nombre" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="edad" class="control-label col-xs-12  col-sm-2">Edad</label> 
                <div class=" col-xs-12 col-sm-10">
                  <input type="number" name="edad" id="edad" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-4 col-sm-offset-8 col-xs-6 col-xs-offset-3">
                  <button name="enviar" class="btn btn-default">Enviar</button>
                </div>
              </div>

          </form>
        </div>
      <?php endif ?>

      <hr>

      <footer>
        <div class="container">
          <p>&copy; Company 2015</p>
        </div>
      </footer>
    </div>      
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.min.js"></script>

    <script src="js/main.js"></script>

  </body>
</html>
