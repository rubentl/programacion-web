<?php

function final_form() {
  return "</form>";
}

function inicio_form($datos) {
  return '<form action="' . $datos["archivo"] . '" '
          . 'method="' . $datos["metodo"] . '" '
          . 'name="' . $datos["nombre"] . '" '
          . 'class="' . $datos["clase"] . '">';
}

function crear_input($datos) {
  return '<div class="form-group"><div class="col-sm-6 col-sm-offset-2">'
          . '<input type="' . $datos["tipo"] . '" '
          . 'class="form-control" '
          . 'id="' . $datos["id"] . '" '
          . 'name="' . $datos["nombre"] . '"></div><div class="col-sm-4">'
          . '<label for="' . $datos["id"] . '" '
          . 'class="control-label">' . $datos["texto"] . "</label>"
          . '</div></div>';
}

function crear_boton($datos) {
  return '<div class="col-sm-4 col-sm-offset-8"><button type="submit" '
          . 'class="' . $datos["clase"] . '" '
          . 'name="' . $datos["nombre"] . '">'
          . $datos["texto"]
          . "</button></div>";
}

function crear_formulario($datos) {
  return '<div class="container">'.  implode("", $datos) . '</div>';
}

function mensaje($mensaje, $titulo) {
  return '<div class="modal fade" tabindex="-1" role="dialog" id="validacion">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
        <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">' . $titulo . '</h4>
      </div>
      <div class="modal-body">
        <p>' . $mensaje . '</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>' . '<script>window.addEventListener("load", function(){$("#validacion").modal("show")});</script>';
}