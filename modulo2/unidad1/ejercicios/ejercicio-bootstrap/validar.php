<?php

function valores() {
  $val = [];
  foreach ($_REQUEST as $indice => $valor) {
    array_push($val, "<li class=\"list-group-item\">$indice: $valor</li>");
  }
  return '<div class="container">'
          . '<ul class="list-group">'
          . implode("", $val)
          . "</ul></div>";
}

function validar() {
  $lon = strlen($_REQUEST["nombre"]);
  $nombre = ($lon < 5 && $lon > 0);
  $fecha = explode("-", $_REQUEST["fecha"]);
  $ano = intval($fecha[0]);
  $anoActual = intval(date('Y')); 
  $mensaje = "Muchas gracias por tu colaboración";
  $titulo = "Información";
  if ($ano >= $anoActual || $ano < ($anoActual - 10)) {
    $mensaje .= "<br>El año no es correcto.";
  }
  if (!$nombre) {
    $mensaje .= "<br>El nombre no es correcto.";
  }
  unset($_REQUEST["anadir"]);
  return valores() . mensaje($mensaje, $titulo);
}
