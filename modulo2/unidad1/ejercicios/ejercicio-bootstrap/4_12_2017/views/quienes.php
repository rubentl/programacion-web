<?php

cabecera("Nuestros Directivos", "para lo bueno y lo malo");

echo '<div class="container"><div class="row">';
thumbnail("Presidente", 
        "Alberto del Amor Hermoso", 
        ["src" => "img/presidente.png", "alt" => "Nuestro presidente"]);
thumbnail("Director Ejecutivo", 
        "Fernando Calatrava Sálvate", 
        ["src" => "img/director.jpg", "alt" => "Director Ejecutivo"]);   
thumbnail("Directora de Marketing", 
        "Susana Partida Final", 
        ["src" => "img/marketing.jpg", "alt" => "Directora de Marketing"]);
echo '</div></div>';
$_SESSION["migas"] = migas([
    "Inicio" => "index.php?page=home",
    "Empresa/Quienes Somos" => "index.php?page=quienes"]);