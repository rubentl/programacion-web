<?php 
  session_start();
  include_once "componentes/componentes.php"; 
?>
<!doctype html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <style>
      body {
        padding-top: 50px;
        padding-bottom: 20px;
      }
      .navbar-brand>img#logo{
        width: 25px;
      }
    </style>
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php?page=home"><img src="img/home.png" alt="home" id="logo"></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li><a href="index.php?page=home">Inicio <span class="sr-only"></span></a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Empresa<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="index.php?page=quienes">Quienes Somos</a></li>
                <li><a href="index.php?page=donde">Dónde Estamos</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <hr>

    <?php
    
    function indexAction() {
      include "views/inicio.php";
    }

    function quienesAction() {
      include "views/quienes.php";
    }

    function dondeAction() {
      include "views/donde.php";
    }

    if (isset($_REQUEST["page"])) {
      switch ($_REQUEST["page"]) {
        case "quienes":
          quienesAction();
          break;
        case "donde":
          dondeAction();
          break;
        case "home":
        default:
          indexAction();
          break;
      }
    } else {
      indexAction();
    }
    ?>

    <hr>

    <footer class="container">

      <p><?= $_SESSION["migas"];?></p>
      <p class="text-right">Academia Alpe &copy; 2017</p>

    </footer>

    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.min.js"></script>

    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>
