<?php

function jumbo($titulo, $mensaje){
    echo  '<div class="jumbotron">'
         . '<div class="container">'
         . "<h1 class=\"text-center\">$titulo</h1>"
         . "<p class=\"text-center\">$mensaje</p>"
         . '</div></div>';
}

function thumbnail($titulo, $texto, $imagen){
echo '<div class="col-sm-6 col-md-4">'
     . '<div class="thumbnail">'
     . '<img src='. $imagen['src'] .' alt='. $imagen["alt"] .'>'
     . '<div class="caption">'
     . '<h3>'. $titulo .'</h3>'
     . '<p>'. $texto .'</p></div></div></div>';
}

function maps($query){
  echo	'<iframe width="100%" height="450" '
        . 'frameborder="0" style="border:0" '
        . 'src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAtMQoSDph1nm7dnbU-vEzDo04vx5QgOqk&q='
          . $query .'" allowfullscreen></iframe>';
}

function cabecera($titulo, $subtitulo){
  echo '<div class="page-header container">'
        .'<h1>'.$titulo.'<small> '.$subtitulo.'</small></h1></div>';
}

function migas($arr, $activo = false){
  $result = '<ol class="breadcrumb">';
  foreach ($arr as $key => $value) {
    $result .= '<li><a href="' .$value.'">'.$key.'</a></li>';
  }
  return $result;
}