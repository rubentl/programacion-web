<?php
spl_autoload_register(function($clase) {
  require_once 'Clases/' . $clase . '.php';
});
      $app = new Aplicacion($_REQUEST);
      $app->estilos();

?>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Práctica 6</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/estilos.css">
  </head>
  <body>
    <div class="container">
      <h2 class="scale-transition center-align card-panel teal white-text">
        Sistema de información geográfica
      </h2>
      <div class="row">
        <form class="col s12" method="get" action="index.php">
          <div class="row">
            <div class="col s6 offset-s3">
              <input id="capa_hospitales" name="hospitales" type="checkbox"
                <?php if ($app->isActivo('hospitales')){echo 'checked';} else {echo '';} ?>/>
              <label for="capa_hospitales">Hospitales y residencias</label>
            </div>
          </div>
          <div class="row">
            <div class="col s6 offset-s3">
            <input id="capa_restaurantes" name="restaurantes" type="checkbox"
                <?php if ($app->isActivo('restaurantes')){echo 'checked';} else {echo '';} ?>/>
              <label for="capa_restaurantes">Restaurantes y bares</label>
            </div>
          </div>
          <div class="row">
            <div class="col s6 offset-s3">
              <input id="capa_autobuses" name="autobuses" type="checkbox"
                <?php if ($app->isActivo('autobuses')){echo 'checked';} else {echo '';} ?>/>
              <label for="capa_autobuses">Autobuses</label>
            </div>
          <div class="row col s3">
            <button class="btn waves-effect waves-light" type="submit" name="refrescar">Refrescar
              <i class="material-icons right">send</i>
            </button>
          </div>
          </div>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col s6 offset-s3">
        <div class="card-panel teal lighten-1 mapa">
          <img src="imgs/Santander.jpg" class="responsive-img"/>
          <?php $app->render(); ?>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
  </body>
</html>
