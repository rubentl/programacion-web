<?php

use Codeception\Util\Locator;

class FirstCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function frontpageWorks(AcceptanceTester $I)
    {
        
        $I->wantTo('Ver la página de inicio');
        $I->amOnPage('/');
        $I->see('Sistema de información geográfica', 'h2');  

        $I->checkOption('#capa_hospitales');
        $I->seeCheckboxIsChecked('#capa_hospitales');
        $I->click('refrescar');
        $I->see('+','p');

        $I->checkOption('#capa_autobuses');
        $I->seeCheckboxIsChecked('#capa_autobuses');
        $I->click('refrescar');
        $I->see('P','p');

        $I->checkOption('#capa_restaurantes');
        $I->seeCheckboxIsChecked('#capa_restaurantes');
        $I->click('refrescar');
        $I->see('Y','p');
    }
}
