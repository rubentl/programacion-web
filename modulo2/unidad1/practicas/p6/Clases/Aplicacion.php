<?php


class Aplicacion {
    protected $capas;

    function __construct($request){
        unset($request['refrescar']);
        $this->capas = [];
        $this->renderizados = [];
        foreach ($request as $index => $value) {
            $this->insertar($index);
        }
        $this->colisiones();
    }

    public function insertar($capa){
        $capa[0] = mb_strtoupper($capa[0]);
        $this->capas[] = new $capa(true);
        return $this;
    }

    public function render(){
        foreach ($this->capas as $capa) {
            $capa->render();
        }
        return $this;
    }

    public function colisiones(){
        $todos = [];
        foreach ($this->capas as $capa) {
            foreach ($capa->coordenadas as $valor) {
                array_push($todos, $valor);
            }
        }
        $this->desactivarConflictos($todos);
    }

    public function desactivarConflictos($todos){
        $unicos = array_unique($todos, SORT_REGULAR); 
        foreach ($todos as $coor) {
            $coor->desactivar();
        }
        foreach ($unicos as $coor) {
            $coor->activar();
        }
    }

    public function estilos(){
        $estilos ='@charset "UTF-8";.container{margin-top: 30px;}.mapa{position: relative;}';
        $file = fopen('css/estilos.css', 'w');
        fwrite($file, $estilos);
        foreach ($this->capas as $capa) {
            fwrite($file, $capa->estilos() . PHP_EOL);
        }
        fclose($file);
        return $this;
    }

    public function isActivo($id){
        $activo =  false;
        foreach ($this->capas as $capa) {
            if ($capa->getId() == $id){
                $activo = $capa->isActivo();
            };
        }
        return $activo;
    }

}
