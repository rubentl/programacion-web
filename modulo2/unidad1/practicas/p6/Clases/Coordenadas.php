<?php


/**
 * Description of Coordenadas
 *
 * @author ruben
 */
class Coordenadas {
    public $x;
    public $y;
    public $activo;

    function __construct($array){
        $this->x = $array['x'];
        $this->y = $array['y'];
        if ($array['activo']){
            $this->activar();
        }else{
            $this->desactivar();
        }
    }

    public function css(){
        $result = [];
        if ($this->isActivo()){
            $result = [
                'top' => $this->y . 'px;',
                'left' => $this->x . 'px;'
            ];
        }
        return $result;
    }

    public function isActivo(){
        return $this->activo;
    }

    public function activar(){
        $this->activo = true;
    }

    public function desactivar(){
        $this->activo = false;
    }

}
