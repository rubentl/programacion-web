<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Hospitales
 *
 * @author ruben
 */
class Hospitales extends Servicio{
    
    function __construct($isActivo){
        $coordenadas = [
            new Coordenadas(['x'=>100,'y'=>100,'activo'=>true]),
            new Coordenadas(['x'=>300,'y'=>200,'activo'=>true]),
            new Coordenadas(['x'=>200,'y'=>50,'activo'=>true])
        ];
        parent::__construct([
            'coordenadas' => $coordenadas,
            'activo' => $isActivo,
            'tag' => '+',
            'id' => 'hospitales',
            'estilos' => [
                'color'=>'#00f'
            ]
        ]);
    }
}
