<?php


/**
 * Description of Servicio
 *
 * @author ruben
 */
class Servicio {
    protected $activo;
    public $coordenadas;
    protected $tag;
    protected $id;
    protected $estilos =  [
                'line-height'=>'1.5em',
                'font-weight'=>'bold',
                'font-size'=>'25px'
            ];

    function __construct($argumentos){
        $this->desactivar();
        if ($argumentos['activo']){
            $this->activar();
        };
        $this->coordenadas = $argumentos['coordenadas'];
        $this->tag = $argumentos['tag'];
        $this->id = $argumentos['id'];
        $estilos_tmp = array_replace($this->estilos, $argumentos['estilos']);
        $this->estilos = $estilos_tmp;
    }

    public function isActivo(){
        return $this->activo;
    }

    public function activar(){
        $this->activo = true;
        return $this;
    }

    public function desactivar(){
        $this->activo = false;
        return $this;
    }

    public function getId(){
        return $this->id;
    }
    
    public function render(){
        if ($this->isActivo()){
            foreach ($this->coordenadas as $idx => $coor) {
                if ($coor->isActivo()){
                    echo '<div id="' . $this->id . '-' . $idx . '">';
                    echo "<p>$this->tag</p>";
                    echo '</div>';   
                }
            }
        }
        return $this;
    }

    public function estilos(){
        $result = '';
        if ($this->isActivo()){
            foreach ($this->coordenadas as $idx => $coor) {
                $result .= "#" . $this->id . "-";
                $result .= $idx . '{';
                $result .= 'position:absolute;';
                foreach ($coor->css() as $key => $value) {
                    $result .= "$key: $value;";
                }
                foreach ($this->estilos as $key => $value) {
                    $result .= "$key: $value;";
                }
                $result .= '}';
            }     
        }
        return $result;
    }

}
