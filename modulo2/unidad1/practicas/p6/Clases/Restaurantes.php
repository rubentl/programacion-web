<?php

/**
 * Description of Restaurantes
 *
 * @author ruben
 */
class Restaurantes extends Servicio{
    /**
     * 
     * @param boolean $isActivo
     */
    function __construct($isActivo){
        $coordenadas = [
            new Coordenadas(['x'=>500,'y'=>10,'activo'=>true]),
            new Coordenadas(['x'=>50,'y'=>300,'activo'=>true])
        ];
        parent::__construct([
            'coordenadas' => $coordenadas,
            'activo' => $isActivo,
            'tag' => 'Y',
            'id' => 'restaurantes',
            'estilos' => [
                'color'=>'#f00'
            ]
        ]);
    }
}
