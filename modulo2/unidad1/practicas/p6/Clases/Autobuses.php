<?php

/**
 * Description of Autobuses
 *
 * @author ruben
 */
class Autobuses extends Servicio{
    function __construct($isActivo){
        $coordenadas = [
            new Coordenadas(['x'=>400,'y'=>200,'activo'=>true]),
            new Coordenadas(['x'=>100,'y'=>100,'activo'=>true])
        ];
        parent::__construct([
            'coordenadas' => $coordenadas,
            'activo' => $isActivo,
            'tag' => 'P',
            'id' => 'autobuses',
            'estilos' => [
                'color'=>'#000'
            ]
        ]);
    }
}
