<?php

/**
 * Description of Aplicacion
 *
 * @author ruben
 */
class Aplicacion {

  protected $controlador;
  protected $accion;
  public $valores;
  protected $url;

  function __construct() {
    $this->url = $_SERVER['SCRIPT_NAME'];
    $this->setControladorAccion();
    $this->setValores();
    $this->llamarControlador();
  }

  private function setControladorAccion() {
    $ruta = explode('/', $_SERVER['PATH_INFO']);
    $this->controlador = $ruta[1];
    $tmp_accion = $ruta[2];
    $tmp_accion[0] = strtoupper($tmp_accion[0]);
    $tmp_accion = 'action' . $tmp_accion;
    $this->setAccion($tmp_accion);
  }

  public function getRuta($controlador, $accion){
    return $this->url . '/' . $controlador . '/' . $accion;
  }
  
  private function setValores() {
    $this->valores = isset($_GET) ? $_GET : [];
  }

  public function getControlador() {
    return $this->controlador;
  }

  function getAccion() {
    return $this->accion;
  }

  function setAccion($accion) {
    $this->accion = $accion;
  }

  public function llamarControlador() {
    require_once 'controlador/' . $this->getControlador() . 'Controller.php';
    $tmp_accion = $this->getAccion();
    $tmp_accion($this);
  }

}
