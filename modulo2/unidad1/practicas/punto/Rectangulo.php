<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rectangulo
 *
 * @author ramon
 */
class Rectangulo {
    public $vertices=[];
    public $base;
    public $altura;
    
    function __construct() {
        
        for($c=0;$c<4;$c++){
            $this->vertices[$c]=new Punto(0,0);
        }
        
        $argumentos= func_get_args();
        $nArgumentos= func_num_args();
        if(method_exists($this, $nombre="__construct$nArgumentos")){
            call_user_func_array(array($this,$nombre), $argumentos);
        }
    }
    
    private function calcularBase(){
        $this->base=$this->vertices[1]->x - $this->vertices[0]->x;
        return $this;
    }
    
    private function calcularAltura(){
        $this->altura=$this->vertices[2]->y - $this->vertices[1]->y;
        return $this;
    }
    
    public function __construct4($a1,$a2,$a3,$a4){
        $argumentos= func_get_args();
        foreach($argumentos as $i=>$arg){
            $this->vertices[$i]=$arg;
        }
        $this->calcularBase()
                ->calcularAltura();
    }
    
    public function __construct2($base,$altura){
        $this->base=$base;
        $this->altura=$altura;
        $this->vertices[1]->x=$base;
        $this->vertices[2]->x=$base;
        $this->vertices[2]->y=$altura;
        $this->vertices[3]->y=$altura;
    }
    
    public function __toString() {
        $salida="";
        foreach($this->vertices as $puntos){
            $salida.="<br>($puntos->x,$puntos->y)";
        }
        return $salida;
    }
    
    public function render(){
        echo '<script type="text/javascript">';
        echo "crearRectangulo(" . $this->vertices[0]->x . "," . $this->vertices[0]->y . "," . $this->base . "," . $this->altura . ");";
        echo '</script>';
    }
    
    public function render1(){
        $salida=file_get_contents("svg.inc");
        return str_replace(["{ancho}","{alto}","{cx}","{cy}","{altoLienzo}","{anchoLienzo}"],[$this->base,$this->altura,$this->vertices[0]->x ,$this->vertices[0]->y,500,500], $salida);
    }


}
