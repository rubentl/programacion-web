<?php

function actionIndex($app) {
  include 'vistas/formulario.html';
}

function actionDatos($app) {
  if (isset($app->valores)) {
    if (esBaseAltura($app->valores)) {
      $rectangulo = new Rectangulo($app->valores['base'], $app->valores['altura']);
    } else if (esCuatroPuntos($app->valores)) {
      $puntos = getPuntos($app->valores);
      $rectangulo = new Rectangulo($puntos['A'], $puntos['B'], $puntos['C'], $puntos['C']);
    } else {
      actionIndex($app);
    }
  }
  if ($rectangulo instanceof Rectangulo) {
    include 'vistas/dibujar.php';
  }
}

function esBaseAltura($valores) {
  $resultado = false;
  if (!empty($valores['base']) && !empty($valores['altura'])) {
    $resultado = true;
  }
  return $resultado;
}

function esCuatroPuntos($valores) {
  $resultado = false;
  if (!empty($valores['puntoA'][0]) && !empty($valores['puntoA'][1]) && !empty($valores['puntoB'][0]) && !empty($valores['puntoB'][1]) && !empty($valores['puntoC'][0]) && !empty($valores['puntoC'][1]) && !empty($valores['puntoD'][0]) && !empty($valores['puntoD'][1])) {
    $resultado = true;
  }
  return $resultado;
}

function getPuntos($valores) {
  return [
      'A' => new Punto($valores['puntoA'][0], $valores['puntoA'][1]),
      'B' => new Punto($valores['puntoB'][0], $valores['puntoB'][1]),
      'C' => new Punto($valores['puntoC'][0], $valores['puntoC'][1]),
      'D' => new Punto($valores['puntoD'][0], $valores['puntoD'][1]),
  ];
}
