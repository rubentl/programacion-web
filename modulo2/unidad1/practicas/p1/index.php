<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
  <head>
    <meta charset="UTF-8">
    <title>Práctica 1</title>
  </head>
  <body>
    En este ejercicio vamos a escribir un texto en la página web a través de PHP
    <hr>
    <?php
    echo "Este texto está escrito desde el script de PHP";
    /* Esto es otro comentario 
     * pero con la diferencia de que es de varias líneas. */
    echo "Este texto también se escribe desde el script de php.";
    ?>
    <hr>
    Esto está escrito en HTML normal.
    <hr>
    <?php
    ############################################################################
    ########## Este comentario es de una sola línea  ###########################
    ############################################################################
    
    # En una página podemos colocar tantos scripts de php como se desee.
    
    print("Esta es otra forma de escribir cosas en la web.");
    ?>
  </body>
</html>
