<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
  <head>
    <meta charset="UTF-8">
    <title>Práctica 1 Ejercicio 2</title>
  </head>
  <body>
    <table width="100%" border="1">
      <tr>
        <td>
          <?php
          // Cuando utilicéis la instrucción echo puedes utilizar las comillas dobles o simples.
          echo 'Este texto quiero que lo escribas utilizando la función echo de php';
          ?>
        </td>
        <td>Aquí debe colocar un texto directamente en HTML</td>
      </tr>
      <tr>
        <td>
          <?php
          // Cuando utilicéis la instrucción print puedes utilizar las comillas dobles o simples.
          print 'Este texto quiero que lo escribas utilizando la función print de php';
          ?>
        </td>
        <td><?= "Academia Alpe"?></td>
      </tr>
    </table>
    <?php
    // put your code here
    ?>
  </body>
</html>
