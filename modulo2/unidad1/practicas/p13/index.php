<?php
spl_autoload_register(function($clase) {
  require_once 'Clases/' . $clase . '.php';
});
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
  <head>
    <meta charset="UTF-8">
    <title>Práctica 13 ejercicio 1</title>
  </head>
  <body>
    <?php
    //$rect = new Rectangulo(50, 100);
    $rect = new Rectangulo([
        'inf_dcha' => ['x' => 100, 'y' => 100],
        'inf_izda' => ['x' => 20, 'y' => 100],
        'sup_dcha' => ['x' => 100, 'y' => 20],
        'sup_izda' => ['x' => 20, 'y' => 20]
    ]);
    echo $rect->dibujar();
    ?>
  </body>
</html>
