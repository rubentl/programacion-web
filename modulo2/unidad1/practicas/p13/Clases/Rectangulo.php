<?php

/**
 * Description of Rectangulo
 * 
 * $vertices = [
 *  'inf_dcha' => ['x' => 0, 'y' => 0],
 *  'inf_izda' => ['x' => 0, 'y' => 0],
 *  'sup_dcha' => ['x' => 0, 'y' => 0],
 *  'sup_izda' => ['x' => 0, 'y' => 0]
 * ]
 * 
 * $desplazamiento = ['x' => 0, 'y' => 0]
 *
 * @author ruben
 */
class Rectangulo {
  
  private $base;
  private $altura;
  private $vertices;
  private $superficie;

  public function __construct() {
    $argumentos = func_get_args();
    $numeroArgumentos = func_num_args();
    $this->base = 0;
    $this->altura = 0;
    $this->vertices = [];
    if (method_exists($this, $f = 'constructor' . $numeroArgumentos)) {
      call_user_func_array(array($this, $f), $argumentos);
    }
  }

  private function constructor2($base, $altura) {
    $this->base = $base;
    $this->altura = $altura;
    $this->vertices = [
      'inf_dcha' => ['x' => $base, 'y' => $altura],
      'inf_izda' => ['x' => 0, 'y' => $altura],
      'sup_izda' => ['x' => 0, 'y' => 0],
      'sup_dcha' => ['x' => $base, 'y' => 0]
    ];
    $this->calcular_superficie();
  }

  private function constructor1($vertices) {
    $this->vertices = $vertices;
    $this->base = $vertices['inf_dcha']['x'] - $vertices['inf_izda']['x'];
    $this->altura = $vertices['inf_izda']['y'] - $vertices['sup_izda']['y'];
    $this->calcular_superficie();
  }
  
  private function calcular_superficie(){
    $this->superficie = $this->base * $this->altura;
  }
  
  public function mover($desplazamiento){
    $this->vertices['inf_izda']['x'] += $desplazamiento['x'];
    $this->vertices['inf_izda']['y'] += $desplazamiento['y'];
    return $this;
  }
  
  public function dibujar(){
    $result = '<svg width=1000 height=600>';
    $result .= '<rect x=' . $this->vertices['inf_izda']['x'];
    $result .= ' y=' . $this->vertices['sup_izda']['y'];
    $result .= ' width=' . $this->base . ' height=' . $this->altura;
    $result .= ' stroke="black" stroke-width=1 /></svg>';
    return $result;
  }

}
