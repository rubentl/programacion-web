<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Punto
 *
 * @author ruben
 */
class Punto {
  private $x;
  private $y;
  
  function __construct($x, $y) {
    $this->setX($x);
    $this->setY($y);
  }
  
  public function getX() {
    return $this->x;
  }

  public function getY() {
    return $this->y;
  }

  public function setX($x) {
    $this->x = (float) $x;
    return $this;
  }

  public function setY($y) {
    $this->y = (float) $y;
    return $this;
  }


}
