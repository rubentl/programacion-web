<?php

/**
 * Description of Nif
 *
 * @author ruben
 */
class Nif {

  private $numero;
  private $letra;

  function __construct() {
    $argumentos = func_get_args();
    $numeroArgumentos = func_num_args();
    $this->numero = 0;
    $this->letra = ' ';
    if (method_exists($this, $f = 'constructor' . $numeroArgumentos)) {
      call_user_func_array(array($this, $f), $argumentos);
    }
  }

  private function constructor1($dni) {
    $this->setNumero($dni);
  }

  public function getNumero() {
    $result = sprintf("%'.08d-%s", $this->numero, $this->letra);
    return $result;
  }

  public function setNumero($numero) {
    $this->numero = $numero;
    $this->letra = $this->calcular();
    return $this;
  }

  private function calcular() {
    $tabla = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 
              'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'];
    $resto = $this->numero % 23;
    return $tabla[$resto];
  }

}
