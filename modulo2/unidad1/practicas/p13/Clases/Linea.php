<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Linea
 *
 * @author ruben
 */
class Linea {

  private $_puntoA;
  private $_puntoB;

  function __construct() {
    $argumentos = func_get_args();
    $numeroArgumentos = func_num_args();
    if (method_exists($this, $f = 'constructor' . $numeroArgumentos)) {
      call_user_func_array(array($this, $f), $argumentos);
    } else {
      $this->_puntoA = new Punto(0, 0);
      $this->_puntoB = new Punto(0, 0);
    }
  }

  private function constructor2($puntoA, $puntoB) {
    $this->_puntoA = $puntoA;
    $this->_puntoB = $puntoB;
  }

  public function mueveDerecha($desplazamiento) {
    $this->_puntoA->x += $desplazamiento;
    $this->_puntoB->x += $desplazamiento;
    return $this;
  }

  public function mueveIzquierda($desplazamiento) {
    $this->_puntoA->x -= $desplazamiento;
    $this->_puntoB->x -= $desplazamiento;
    return $this;
  }

  public function mueveArriba($desplazamiento) {
    $this->_puntoA->y -= $desplazamiento;
    $this->_puntoB->y -= $desplazamiento;
    return $this;
  }

  public function mueveAbajo($desplazamiento) {
    $this->_puntoA->y += $desplazamiento;
    $this->_puntoB->y += $desplazamiento;
    return $this;
  }

  public function render() {
    $result = '[';
    $result .= '(' . $this->_puntoA->getX() . ',' . $this->_puntoA->getY() . '),';
    $result .= '(' . $this->_puntoB->getX() . ',' . $this->_puntoB->getY() . ')';
    $result .= ']';
    echo $result;
  }

}
