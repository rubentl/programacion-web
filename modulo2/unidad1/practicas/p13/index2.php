<?php
spl_autoload_register(function($clase) {
  require_once 'Clases/' . $clase . '.php';
});
?>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>Práctica 13 ejercicio 2</title>
  </head>
  <body>
    <?php
    //$linea = new Linea();
    $linea = new Linea(new Punto(1.2,2.3), new Punto(2.0,3.5));
    $linea->render();
    ?>
  </body>
</html>
