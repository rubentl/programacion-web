<?php

use \clases\Listados;

echo '<div class=row>';
echo '<a class="btn btn-primary col-sm-2 col-md-offset-10" href="';
echo 'nuevo" role="button">Nuevo</a>';
echo '</div>';

echo (new Listados(
    [
        'clases' => ['table-striped', 'table-hover'],
        'campos' => [
            'id'=>[
                'cabecera' => 'Id',
                'tipo' => Listados::ENLACE,
                'texto' => 'id',
                'enlace' => 'modificar'
            ],
            'enlace'=>[
                'cabecera'=>'Enlace',
                'tipo'=> Listados::TEXTO
            ],
            'descripcion_corta'=>[
                'cabecera' => 'Descripción',
                'tipo' => Listados::TEXTO,
            ], 
        ],
        'datos' => $this->datos,
    ]
))->render();


echo '<div class=row>';
echo '<a class="btn btn-primary col-sm-2 col-md-offset-10" href="';
echo 'nuevo" role="button">Nuevo</a>';
echo '</div>';

?>
