<?php

use \clases\Listados;

echo (new Listados(
    [
        'clases' => ['table-striped', 'table-hover'],
        'campos' => [
            'id'=>['cabecera' => 'Id'],
            'enlace'=>[
                'cabecera'=>'Enlace',
                'tipo'=> Listados::ENLACE
        ],
            'descripcion_corta'=>[
                'cabecera' => 'Descripción',
                'tipo' => Listados::Modal,
                'contenido_modal' => 'descripcion_larga',
                'enlace_boton_modal' => 'enlace'
            ], 
        ],
        'datos' => $this->datos,
    ]
))->render();

?>
