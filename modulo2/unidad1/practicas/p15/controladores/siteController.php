<?php
namespace controladores;

use \modelos\Marcadores;

class siteController extends Controller{

    public function portadaAccion($datos){

        $this->privadosAccion($datos);
    }

    public function publicosAccion($datos){
        $objetos = Marcadores::findAll('publico = "no"');

        $this->render([
            "vista"=>"site/publicos",
            "datos"=>$objetos,
            "activo"=>"Públicos"
        ]);
    }

    public function privadosAccion($datos){
        $objetos = Marcadores::findAll();
        
        $this->render([
            "vista"=>"site/portada",
            "datos"=>$objetos,
            "activo"=>"Privados"
        ]);
    }

    public function adminAccion($datos){
        $objetos = Marcadores::findAll();

        $this->render([
            'vista'=>"site/admin",
            'datos'=>$objetos,
            'activo'=>'Admin'
        ]);
    }

    public function modificarAccion($datos){
        $modelo = Marcadores::findOne('id='.$datos['id']);

        $this->render([
            'vista' => "site/formulario",
            'modelo' => $modelo,
            'activo' => 'Admin'
        ]);
    }

    public function guardarAccion($datos){
        $modelo = new Marcadores();
        if (isset($datos['id'])){
            $modelo->load($datos)->save();
        }else{
            $modelo->load($datos)->insert();
        }
        $this->adminAccion($datos);
    }

    public function nuevoAccion($datos){
        $modelo = new Marcadores();
        $this->render([
            'vista' => "site/formulario",
            'modelo' => $modelo,
            'activo' => 'Admin'
        ]);
    }

    public function borrarAccion($datos){
        $modelo = Marcadores::findOne('id='.$datos['id']);
        $modelo->eliminar();
        $this->adminAccion($datos);
    }

}

