<?php
namespace modelos;
class Marcadores extends Modelo{

    public function campos(){
        return "enlace,descripcion_corta,descripcion_larga,publico";
    }

    public function primary(){
        return "id";
    }

    public function tipos(){
        return [
            'id' => 'hidden',
            'enlace'=>'input',
            'descripcion_corta'=>'input',
            'descripcion_larga'=>'textarea',
            'publico'=>['select'=>['si','no']]
        ];
    }

    public function attributeNombre(){
        return [
            'enlace' => 'Enlace',
            'descripcion_larga' => 'Descripción Larga',
            'descripcion_corta' => 'Descripción Corta',
            'publico' => 'Público'
        ];
    }

    public function load($datos){
        $primaria = $this->primary(); 
        $this->$primaria = $datos[$primaria];
        foreach (explode(',', $this->campos()) as $campo) {
            $this->$campo = $datos[$campo];
        }
        return $this;
    }

}
