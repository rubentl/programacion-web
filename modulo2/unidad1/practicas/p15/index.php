<?php

session_start();
spl_autoload_register(function ($nombre_clase) {
    $nombre_clase = str_replace("\\", DIRECTORY_SEPARATOR, $nombre_clase);
    include(__DIR__ . '/' . $nombre_clase . '.php');
});
?>

<?php
    new clases\Aplicacion('config/principal.php');
?>

