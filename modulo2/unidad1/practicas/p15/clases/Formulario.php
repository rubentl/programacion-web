<?php


namespace clases;

/**
 * Class Formulario
 * @author Ruben
 */
class Formulario
{
    protected $modelo;
    protected $cache;

    function __construct($modelo){
        $this->modelo = $modelo;
        $this->cache = $this->formulario();
    }

    public function render(){
        return $this->cache;
    }

    public function formulario(){
        if (isset($this->modelo->id)){
            $salida = $this->modificar();
        }else{
            foreach (explode(',', $this->modelo->campos()) as $campo) {
                $campos[$campo] = '';
            }
            $this->modelo->load($campos); 
            $salida = $this->nuevo();
        }
        return $salida;
    }

    protected function nuevo(){
        $salida = $this->beginForm(['method'=>'get','action'=>'guardar']);
        $salida .= $this->camposForm();
        $salida .= $this->beginBotones();
        $salida .= $this->botonFormGuardar();
        $salida .= $this->endBotones();
        $salida .= $this->endForm();
        return $salida;
    }

    protected function modificar(){
        $salida = $this->beginForm(['method'=>'get','action'=>'modificar']);
        $salida .= $this->camposForm();
        $salida .= $this->beginBotones();
        $salida .= $this->botonFormEliminar();
        $salida .= $this->botonFormGuardar();
        $salida .= $this->endBotones();
        $salida .= $this->endForm();
        return $salida;
    }

    protected function endForm(){
        return '</form>';
    }

    protected function beginForm($opciones){
        return '<form class="form-horizontal" method="' . $opciones['method'] . '"'
            . 'action="' . $opciones['action'] . '">';
    }

    protected function camposForm(){
        $tipos = $this->modelo->tipos();
        $etiquetas = $this->modelo->attributeNombre();
        $salida = '';
        foreach (explode(',', $this->modelo->campos()) as $campo) {

            if ($tipos[$campo] != 'hidden'){
                $salida .= '<div class="form-group">';
                $salida .= '<label for="id' . $campo . '" class="col-sm-2 col-sm-offset-2 control-label">' 
                        . $etiquetas[$campo] . '</label>';
                $salida .= '<div class="col-sm-6">';
                if (is_array($tipos[$campo]) && array_key_exists('select', $tipos[$campo])){
                    $salida .= $this->selectCampo($campo, $this->modelo->$campo);
                }elseif ($tipos[$campo] == 'textarea'){
                    $salida .= $this->textareaCampo($campo, $this->modelo->$campo);
                }else{
                    $salida .= $this->inputCampo($campo,$this->modelo->$campo);
                }
                $salida .= '</div></div>';
            }else{
                $salida .= $this->hiddenCampo($campo, $this->modelo->$campo);
            }
        }
        return $salida;
    }

    protected function selectCampo($campo, $valor){
        $salida =  '<select class="form-control" id="id' . $campo . '" name="' . $campo . '">';
        foreach ($this->modelo->tipos()[$campo]['select'] as $opcion) {
            $salida .= "<option ";
            if ($opcion == $valor){
                $salida .= 'selected';
            }
            $salida .= ">$opcion</option>";
        }
        $salida .= '</select>';
        return $salida;
    }

    protected function textareaCampo($campo, $valor){
        return '<textarea class="form-control" rows=5 id="id' . $campo .'"' 
                            . 'name="' . $campo .  '">'.  $valor .'</textarea>';
    }

    protected function inputCampo($campo, $valor){
        return '<input type="input" class="form-control" id="id' . $campo .'" value="'
                            . $valor . '" name="' . $campo .  '">';
    }

    protected function hiddenCampo($campo, $valor){
        return '<input type="hidden" name="' . $campo . '" value="' . $valor . '">';
    }

    protected function beginBotones(){
        return '<div class="form-group"><div class="pull-right">';
    }

    protected function endBotones(){
        return '</div></div>';
    }

    protected function botonFormEliminar(){
        $primary = $this->modelo->primary();
        return '<a href="site/borrar?id="' . $this->modelo->$primary . ' class="btn btn-primary">Eliminar</a>';
    }

    protected function botonFormGuardar(){
        return '<button type="submit" class="btn btn-primary margen-20">Guardar</a>';
    }

}
