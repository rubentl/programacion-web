<?php

namespace clases;

class Modalnuevo extends Modal{

    protected $titulo;
    protected $enlaceBoton;

    public function __construct($arg=[]){
        parent::__construct($arg);
        $this->titulo = $arg['titulo'];
        if (isset($arg['enlace_boton_modal'])){
            $this->enlaceBoton = $arg['enlace_boton_modal'];
            $this->nuevosBotones();
        }
    }

    protected function nuevosBotones(){
        $botones = '<div class="modal-footer">'
            . '<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>'
            . '<a class="btn btn-primary" href="' . $this->enlaceBoton . '" target="_blank">Visitar</a>'
            . '</div>';
        $nueva_salida = substr($this->salida, 0, strlen($this->salida) - 18) . $botones . substr($this->salida, strlen($this->salida) - 18);
        $this->salida = $nueva_salida;
    }

    public function render($boton=TRUE) {
        if($boton){
            $this->salida .= '<button type="button" class="btn '
                . $this->getClases() 
                .'" data-toggle="modal" data-target="#' 
                . $this->idVentana . '">' 
                . $this->titulo . '</button>';
        }else{
            $this->escribirJs('$("#'.$this->idVentana.'").modal("show");');
        }
        return $this->salida;
    }

}
