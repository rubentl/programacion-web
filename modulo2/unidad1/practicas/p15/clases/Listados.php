<?php


namespace clases;

/**
 * Description of Listados
 *
 * @author ruben
 */
class Listados {
    const TEXTO = 'Texto';
    const ENLACE = 'Enlace';
    const Modal = 'Modal';

    protected $clases = ['table'];
    protected $campos;
    protected $objetos;
                function __construct($arg){
        $this->clases = array_unique(array_merge($this->clases, $arg['clases']));
        $this->campos = $arg['campos'] or [''];
        $this->objetos = $arg['datos'] or [''];
    }

    public function render(){
        $salida = $this->begin();
        $salida .= $this->cabecera();
        $salida .= $this->cuerpo();
        $salida .= $this->end();
        return $salida;
    }

    protected function begin(){
        $result = '<table class="';
        $result .= implode(" ", $this->clases);
        $result .=  '">';
        return $result;
    }

    protected function cabecera(){
        $result = '<thead>';
        foreach ($this->campos as $campo => $nombre){
            $result .= '<th>' . $nombre['cabecera']. '</th>';
        }        
        $result .= '</thead>';
        return $result;
    }

    protected function cuerpo(){
        $result = '<tbody>';
        foreach ($this->objetos as $objeto) {
            $result .= '<tr>';
            foreach ($this->campos as $campo => $valor) {
                $result .= '<td>';
                $comando_tmp = 'insertar';
                if (isset($valor['tipo'])){
                    $comando_tmp = $comando_tmp . $valor['tipo'];
                }else{
                    $comando_tmp = $comando_tmp . Listados::TEXTO; 
                }
                $result .= $this->$comando_tmp($objeto, $campo);
                $result .= '</td>';
            }
            $result .= '</tr>';
        }
        $result .= '</tbody>';
        return $result;
    }

    protected function insertarTexto($objeto, $campo){
        return $objeto->$campo;
    }

    protected function insertarEnlace($objeto, $campo){
        if (isset($this->campos[$campo]['enlace'])){
            $enlace = $this->campos[$campo]['enlace'];
        }else{
            $enlace = $objeto->$campo;
        }
        if (isset($this->campos[$campo]['texto'])){
            $campo_mostrar = $this->campos[$campo]['texto'];
            $texto_enlace = $objeto->$campo_mostrar;
            $enlace .= '?' . $campo_mostrar . '=' . $texto_enlace;
        }else{
            $arr_tmp = explode('.', $enlace, -1);
            $texto_enlace = $arr_tmp[count($arr_tmp) -1];
        }
        
        return '<a href="' . $enlace . '" target="_blank">' . $texto_enlace . '</a>';
    }
    
    protected function insertarModal($objeto, $campo){
        $contenido_modal = $this->campos[$campo]['contenido_modal'];
        $enlace_boton = $this->campos[$campo]['enlace_boton_modal'];
        return (new Modalnuevo([
            'id' => 'id' . str_replace(' ', '', $objeto->$campo),
            'titulo' => $objeto->$campo,
            'datos' => [$objeto->$contenido_modal],
            'clases' => 'btn-link',
            'enlace_boton_modal' => $objeto->$enlace_boton,
        ]))->render(true);
    }

    protected function end(){
        return '</table>';
    }
}
