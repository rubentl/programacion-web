<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Práctica 5</title>
        <style>
            ul{
                list-style: none;
            }
            li{
                line-height:2em;
            }
            img{
                width: 20px;
                vertical-align: text-top;
                margin-right: 5px;
            }
        </style>
    </head>

    <body>
        <?php

        class Elemento {

            protected $nombre;
            protected $url;
            protected $tipo;
            protected $fecha;
            protected $tamano;
            protected $icono;

            function __construct($nombre, $icono) {
                $this->url = $nombre;
                $tmp_nombre = explode('/', $nombre);
                $this->nombre = end($tmp_nombre);
                $this->fecha = date("d F Y H:i:s.", filemtime($nombre));
                $this->tamano = filesize($nombre);
                $this->icono = $icono;
                $this->tipo = '';
            }

        }

        class Archivo extends Elemento {

            function __construct($nombre, $icono) {
                parent::__construct($nombre, $icono['archivo']);
                $this->tipo = 'archivo';
            }

            public function mostrar() {
                $formato = "%s  %'!6d bytes  <img src=\"%s\"> %s";
                $result = sprintf($formato, $this->fecha, $this->tamano, $this->icono, $this->nombre);
                return str_replace('!', '&nbsp;', $result);
            }

        }

        class Directorio extends Elemento {

            protected $hijos;
            protected $padre;

            function __construct($nombre, $icono) {
                parent::__construct($nombre, $icono['directorio']);
                $this->tipo = 'directorio';
                $tmp_url = explode('/', $this->url);
                array_pop($tmp_url);
                $tmp_padre = implode('/', $tmp_url);
                $this->padre = ($tmp_padre !== '') ? $tmp_padre : '.';
                $elementos = scandir($nombre);
                foreach ($elementos as $elemento) {
                    $this->nuevo($elemento, $icono);
                }
            }

            private function nuevo($elemento, $icono) {
                if ($elemento !== '..' && $elemento !== '.') {
                    $url = $this->url . '/' . $elemento;
                    $this->hijos[] = is_dir($url) ? new Directorio($url, $icono) : new Archivo($url, $icono);
                }
            }

            private function hay_hijos() {
                return (count($this->hijos) !== 0);
            }

            private function mostrarDirectorio() {
                $formato = "%s  %'!6d bytes  <img src=\"%s\"> <a href=\"index.php?page=%s\">%s</a>";
                $result = sprintf($formato, $this->fecha, $this->tamano, $this->icono, $this->url, $this->nombre);
                return str_replace('!', '&nbsp;', $result);
            }

            public function mostrar() {
                $result = '<div><ul>';
                $result .= '<li><a href="index.php?page=' . $this->padre . '">';
                $result .= 'Subir' . '</a></li>';
                if ($this->hay_hijos()) {
                    foreach ($this->hijos as $hijo) {
                        if ($hijo->tipo === 'archivo') {
                            $result .= '<li>' . $hijo->mostrar() . '</li>';
                        } else {
                            $result .= '<li>' . $hijo->mostrarDirectorio() . '</li>';
                        }
                    }
                }
                $result .= '</ul></div>';
                return $result;
            }

        }

        class Aplicacion {

            protected $raiz;

            function __construct($raiz, $iconos) {
                $this->raiz = new Directorio($raiz, $iconos);
            }

            public function mostrar() {
                echo $this->raiz->mostrar();
            }

        }

        $iconos = [
            'archivo' => 'imgs/file.png',
            'directorio' => 'imgs/folder.png'
        ];

        if (!isset($_REQUEST['page'])) {
            displayDirectorio('.', $iconos);
        } else {
            $actual = $_REQUEST['page'];
            displayDirectorio($actual, $iconos);
        }

        function displayDirectorio($directorio, $iconos) {
            $app = new Aplicacion($directorio, $iconos);
            $app->mostrar();
        }
        ?>
    </body>
</html>
