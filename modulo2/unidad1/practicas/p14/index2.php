<?php
spl_autoload_register(function($clase) {
  require_once 'Clases/' . $clase . '.php';
});
?>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>Práctica 14 ejercicio 2</title>
  </head>
  <body>

    <?php
    if (isset($_REQUEST['submit'])) {
      $personaFormulario = new Persona2($_REQUEST['nombre'], $_REQUEST['edad'], $_REQUEST['sexo'], $_REQUEST['peso'], $_REQUEST['altura']);
      $personaDefecto = new Persona2($_REQUEST['nombre'], $_REQUEST['edad'], $_REQUEST['sexo']);
      $personaAleatoria = new Persona2();
      $personaAleatoria->setNombre('Rubén');
      $personaAleatoria->setEdad(38);
      $personaAleatoria->setSexo('H');
      $personaAleatoria->setPeso(113);
      $personaAleatoria->setAltura(1.75);
      echo $personaFormulario;
      echo $personaDefecto;
      echo $personaAleatoria;
    } else {
      ?>
      <form action="index2.php" method="get">
        Nombre: <input type="text" name="nombre" /><br>
        Edad: <input type="number" name="edad" /><br>
        Sexo: <input type="text" name="sexo" /><br>
        Peso: <input type="number" name="peso" /><br>
        Altura: <input type="text" name="altura" /><br>
        <input type="submit" value="¡Envíame!" name="submit" />
      </form>

  <?php
}
?>
  </body>
</html>
