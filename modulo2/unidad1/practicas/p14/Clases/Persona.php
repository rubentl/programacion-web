<?php

/**
 * Description of Persona
 *
 * @author ruben
 */
class Persona {

  protected $nombre;
  protected $edad;

  function __construct($nombre, $edad) {
    $this->setNombre($nombre);
    $this->setEdad($edad);
  }

  public function getNombre() {
    return $this->nombre;
  }

  public function getEdad() {
    return $this->edad;
  }

  public function setNombre($nombre) {
    $this->nombre = $nombre;
    return $this;
  }

  public function setEdad($edad) {
    $this->edad = $edad;
    return $this;
  }
  
  public function mostrar() {
      return $this->getNombre() 
          .' tiene una edad de '
          . $this->getEdad();
  }

}
