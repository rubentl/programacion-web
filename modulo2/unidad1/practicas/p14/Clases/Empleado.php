<?php

class Empleado extends Persona{

    protected $sueldo;

    function __construct($nombre, $edad, $sueldo){
        parent::__construct($nombre, $edad);
        $this->setSueldo($sueldo);
    }

    public function setSueldo($sueldo){
        $this->sueldo = $sueldo;
        return $this;
    }

    public function getSueldo(){
        return $this->sueldo;
    }

    public function mostrar(){
        return parent::mostrar() 
            . ' y un sueldo de ' 
            . $this->getSueldo();
    }
}
