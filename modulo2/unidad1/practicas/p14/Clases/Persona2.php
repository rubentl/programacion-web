<?php

/**
 * Description of Persona
 *
 * @author ruben
 */
class Persona2 {

  protected $nombre;
  protected $edad;
  protected $dni;
  protected $sexo;
  protected $peso;
  protected $altura;

  function __construct() {
    $argumentos = func_get_args();
    $numeroArgumentos = func_num_args();
    $this->setNombre('');
    $this->setEdad(0);
    $this->setSexo('H');
    $this->setPeso(0);
    $this->setAltura(0);
    $this->generaDNI();
    if (method_exists($this, $f = 'constructor' . $numeroArgumentos)) {
      call_user_func_array(array($this, $f), $argumentos);
    }
  }

  private function constructor3($nombre, $edad, $sexo) {
    $this->setNombre($nombre)
            ->setEdad($edad)
            ->setSexo($sexo);
  }

  private function constructor5($nombre, $edad, $sexo, $peso, $altura) {
    $this->constructor3($nombre, $edad, $sexo);
    $this->setAltura($altura)->setPeso($peso);
  }

  public function getNombre() {
    return $this->nombre;
  }

  public function getEdad() {
    return $this->edad;
  }

  public function setNombre($nombre) {
    $this->nombre = $nombre;
    return $this;
  }

  public function setEdad($edad) {
    $this->edad = $edad;
    return $this;
  }

  public function getSexo() {
    return $this->sexo;
  }

  public function getPeso() {
    return $this->peso;
  }

  public function getAltura() {
    return $this->altura;
  }

  public function setSexo($sexo) {
    $this->sexo = $this->comprobarSexo($sexo);
    return $this;
  }

  public function setPeso($peso) {
    $this->peso = $peso;
    return $this;
  }

  public function setAltura($altura) {
    $this->altura = $altura;
    return $this;
  }

  private function comprobarSexo($sexo) {
    return ($sexo == 'M') ? 'M' : 'H';
  }

  protected function generaDNI() {
    $tmp_dni = rand(0, 99999999);
    $this->dni = sprintf("%08d-%1s", $tmp_dni, $this->calcularLetra($tmp_dni));
    return $this->dni;
  }

  private function calcularLetra($dni) {
    $tabla = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B',
        'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'];
    $resto = $dni % 23;
    return $tabla[$resto];
  }

  public function calcularIMC() {
    if ($this->getAltura() > 0 && $this->getPeso() > 0) {
      $imc = (int) ($this->getPeso() / ($this->getAltura() * $this->getAltura()));
    } else {
      $imc = 0;
    }
    $valor = 0;
    if ($imc < 20) {
      $valor = -1;
    } else if ($imc > 25) {
      $valor = 1;
    }
    return $valor;
  }

  public function categoriaPeso() {
    $imc = $this->calcularIMC();
    $valor = 'peso ideal';
    if ($imc == -1) {
      $valor = 'peso inferior';
    } else if ($imc == 1) {
      $valor = 'sobrepeso';
    }
    return $valor;
  }

  public function esMayorDeEdad() {
    return ($this->edad > 17);
  }

  public function categoriaEdad() {
    return ($this->esMayorDeEdad()) ? 'mayor' : 'menor';
  }

  public function categoriaSexo() {
    return ($this->getSexo() == 'M') ? 'mujer' : 'hombre';
  }

  public function __toString() {
    return $this->getNombre() . ' con DNI ' . $this->dni
            . ' tiene una edad de ' . $this->getEdad()
            . ' y por tanto es ' . $this->categoriaSexo()
            . ' ' . $this->categoriaEdad()
            . ' de edad.'
            . '<br>Su peso de ' . $this->getPeso()
            . ' y su altura de ' . $this->getAltura()
            . ' indican que está en ' . $this->categoriaPeso()
            . '<br>';
  }

}
