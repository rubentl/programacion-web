<?php

/**
 * Description of Lavadora
 *
 * @author ruben
 */
class Lavadora extends Electrodomestico {

  const CARGA = 5;

  protected $carga;

  public function __construct() {
    parent::__construct();
    $this->carga = self::CARGA;
    $argumentos = func_get_args();
    $numeroArgumentos = func_num_args();
    if (method_exists($this, $f = 'constructor' . $numeroArgumentos)) {
      call_user_func_array(array($this, $f), $argumentos);
    }
  }

  private function constructor5($precio, $peso, $color, $consumo, $carga) {
    $this->constructor4($precio, $peso, $color, $consumo);
    $this->carga = $carga;
  }

  public function getCarga() {
    return $this->carga;
  }

  public function precioFinal() {
    $precio = parent::precioFinal();
    if ($this->getCarga() > 30) {
      $precio += 50;
    }
    return $precio;
  }

}
