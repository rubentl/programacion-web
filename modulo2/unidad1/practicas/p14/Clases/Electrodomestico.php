<?php

/**
 * Description of Electrodomestico
 *
 * @author ruben
 */
class Electrodomestico {

  const COLORES = ['blanco', 'negro', 'rojo', 'azul', 'gris'];
  const CONSUMOS = ['A', 'B', 'C', 'D', 'E', 'F'];
  const CONSUMO_PRECIOS = ['A'=>100, 'B'=>80, 'C'=>60, 'D'=>50, 'E'=>30, 'F'=>10];
  const PESO_PRECIOS = [10, 50, 80, 100];
  const CONSUMO = 'F';
  const PRECIOBASE = 100;
  const PESO = 60;
  const COLOR = 'blanco';

  protected $precio_base;
  protected $consumo;
  protected $peso;
  protected $color;

  public function __construct() {
    $argumentos = func_get_args();
    $numeroArgumentos = func_num_args();
    $this->precio_base = self::PRECIOBASE;
    $this->consumo = self::CONSUMO;
    $this->color = self::COLOR;
    $this->peso = self::PESO;
    if (method_exists($this, $f = 'constructor' . $numeroArgumentos)) {
      call_user_func_array(array($this, $f), $argumentos);
    }
  }

  protected function constructor2($precio, $peso) {
    $this->peso = $peso;
    $this->precio_base = $precio;
  }

  protected function constructor4($precio, $peso, $color, $consumo) {
    $this->constructor2($precio, $peso);
    $this->color = $this->comprobarColor(strtolower($color));
    $this->consumo = $this->comprobarConsumoEnergetico(strtoupper($consumo));
  }

  private function comprobarColor($color) {
    if (in_array($color, self::COLORES)) {
      return $color;
    } else {
      return self::COLOR;
    }
  }

  private function comprobarConsumoEnergetico($consumo) {
    if (in_array($consumo, self::CONSUMOS)) {
      return $consumo;
    } else {
      return self::CONSUMO;
    }
  }

  public function precioFinal() {
    $precio = $this->getPrecio_base();
    $precio += self::CONSUMO_PRECIOS[$this->getConsumo()];
    $rango = 3;
    $peso = $this->getPeso();
    if ($peso < 20){
        $rango = 0;
    }else if ($peso < 50){
        $rango = 1;
    }else if ($peso < 80){
        $rango = 2;
    }
    $precio += self::PESO_PRECIOS[$rango];
    return $precio;
  }

  public function getPrecio_base() {
    return $this->precio_base;
  }

  public function getConsumo() {
    return $this->consumo;
  }

  public function getPeso() {
    return $this->peso;
  }

  public function getColor() {
    return $this->color;
  }

}
