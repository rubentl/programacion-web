<?php

/**
 * Description of Television
 *
 * @author ruben
 */
class Television extends Electrodomestico{
  const RESOLUCION = 20;
  protected $resolucion;
  protected $sintonizador_tdt;
    public function __construct() {
    parent::__construct();
    $this->resolucion = self::RESOLUCION;
    $argumentos = func_get_args();
    $numeroArgumentos = func_num_args();
    if (method_exists($this, $f = 'constructor' . $numeroArgumentos)) {
      call_user_func_array(array($this, $f), $argumentos);
    }
  }

  private function constructor6($precio, $peso, $color, $consumo, $resolucion, $sintonizador) {
    $this->constructor4($precio, $peso, $color, $consumo);
    $this->resolucion = $resolucion;
    $this->sintonizador_tdt = $sintonizador;
  }
  
  public function getResolucion() {
    return $this->resolucion;
  }

  public function getSintonizador_tdt() {
    return $this->sintonizador_tdt;
  }

  public function precioFinal(){
    $precio = parent::precioFinal();
    if ($this->getResolucion() > 40) {
      $precio += ($precio*30)/100;
    }
    if ($this->getSintonizador_tdt()){
        $precio += 50;
    }
    return $precio;
  }
}
