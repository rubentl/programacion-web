<?php
spl_autoload_register(function ($nombre_clase) {
  include 'Clases/' . $nombre_clase . '.php';
});
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Práctica 14 ejercicio 3</title>
  </head>
  <body>
    <?php
    $aparatos = [
        new Television(100, 10, 'blanco', 'E', 52, true),
        new Television(80, 10, 'blanco', 'A'),
        new Lavadora(180, 60, 'negro', 'F', 10),
        new Television(50, 20, 'blanco', 'E', 32, true),
        new Television(200, 5, 'azul', 'B'),
        new Lavadora(100, 80, 'negro', 'F', 10),
        new Lavadora(250, 75, 'rojo', 'C', 10),
        new Lavadora(100, 90, 'negro', 'F', 10),
        new Television(90, 10, 'gris', 'A', 52, false),
        new Television(100, 10, 'blanco', 'E', 52, true),
    ];
    $precios = array_map(function($obj) {
      $tipo = ($obj instanceof Television) ? 'television' : 'lavadora';
      return [$tipo => $obj->precioFinal()];
    }, $aparatos);
    $total = 0;
    $total += televisores($precios);
    $total += lavadoras($precios);
    echo 'El precio total de los electrodomésticos es ' . $total;

    function televisores($precios) {
      echo 'Los precios de las televisiones son [';
      $total = electrodomesticos($precios, 'television');
      echo ']<br>';
      return $total;
    }

    function lavadoras($precios) {
      echo 'Los precios de las lavadoras son [';
      $total = electrodomesticos($precios, 'lavadora');
      echo ']<br>';
      return $total;
    }

    function electrodomesticos($precios, $nombre) {
      $total = 0;
      foreach ($precios as $valor) {
        foreach ($valor as $tipo => $precio) {
          $total += $precio;
          if ($tipo == $nombre) {
            echo $precio . ' ';
          }
        }
      }
      return $total;
    }
    ?>
  </body>
</html>
