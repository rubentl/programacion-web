<?php
spl_autoload_register(function($clase) {
  require_once 'Clases/' . $clase . '.php';
});
?>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>Práctica 14 ejercicio 1</title>
  </head>
  <body>
    <?php
    $yo = new Persona('Rubén', 48);
    $tu = new Empleado('David', 45, 2100);
    echo $yo->mostrar();
    echo '<br>';
    echo $tu->mostrar();
    ?>
  </body>
</html>
