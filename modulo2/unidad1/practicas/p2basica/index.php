<?php
    spl_autoload_register(function ($clase){
        require_once 'Clases/'. $clase .'.php';
    });
?>
    <!DOCTYPE html>

    <html>

    <head>
        <meta charset="UTF-8">
        <title>Práctica 2 Básica</title>
        <style>
            .contenedor {
                width: 500px;
                margin: 50px auto;
            }
            
            .total>span,
            .maxima {
                line-height: 3em;
                padding: 10px 5px;
                border: 1px solid black;
            }
        </style>
    </head>

    <body>
        <div class="contenedor">
            <?php
                $dados = [
                    1=>'imgs/1.svg',
                    2=>'imgs/2.svg',
                    3=>'imgs/3.svg',
                    4=>'imgs/4.svg',
                    5=>'imgs/5.svg',
                    6=>'imgs/6.svg'
                ];
                $juego = new Juego($dados);
                $juego->nuevaTirada(10);
                echo $juego->mostrar()->maxTirada();
            ?>
        </div>
    </body>

    </html>
