<?php

/**
 * Description of Dado
 *
 * @author ruben
 */
class Dado {

  private $url;
  private $valor;
  private $nombre;

  function __construct($url, $valor) {
    $this->setValor($valor)->setUrl($url)->setNombre();
  }

  public function getUrl() {
    return $this->url;
  }

  public function getValor() {
    return $this->valor;
  }

  public function setUrl($url) {
    $this->url = $url;
    return $this;
  }

  public function setValor($valor) {
    $this->valor = $valor;
    return $this;
  }

  public function getNombre() {
    return $this->nombre;
  }

  public function setNombre() {
    $this->nombre = 'dado_' . $this->getValor();
    return $this;
  }

  public function mostrar() {
    return '<img src="' . $this->getUrl() . '" alt="' . $this->getNombre() . '"/>';
  }

}
