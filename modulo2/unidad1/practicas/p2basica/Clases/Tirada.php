<?php

/**
 * Description of Tirada
 *
 * @author ruben
 */
class Tirada {

  private $tirada = [];
  private $total;

  function __construct($dados) {
    $this->setTirada($dados);
  }

  public function getTirada() {
    return $this->tirada;
  }

  public function setTirada($dados) {
    $ran = rand(1, 6);
    $this->tirada[] = new Dado($dados[$ran], $ran);
    $ran = rand(1, 6);
    $this->tirada[] = new Dado($dados[$ran], $ran);
    $this->setTotal();
    return $this;
  }

  public function getTotal() {
    return $this->total;
  }

  public function setTotal() {
    $this->total = $this->tirada[0]->getValor() + $this->tirada[1]->getValor();
    return $this;
  }

  public function mostrar() {
    $result = '<div><div class="dados">';
    $result .= $this->getTirada()[0]->mostrar() . $this->getTirada()[1]->mostrar();
    $result .= '</div><div class="total">Total: <span>';
    $result .= $this->getTotal() . '</span></div></div>';
    return $result;
  }

}
