<?php


/**
 * Description of Juego
 *
 * @author ruben
 */
class Juego {
  private $tiradas = [];
  private $dados;
  
  function __construct($dados) {
    $this->setDados($dados);
  }
  
  public function getTiradas() {
    return $this->tiradas;
  }

  public function getDados() {
    return $this->dados;
  }

  public function nuevaTirada($cuantas) {
    for ($i = 0; $i < $cuantas; $i++){
        $this->tiradas[] = new Tirada($this->dados);
    }
    return $this;
  }

  public function setDados($dados) {
    $this->dados = $dados;
    return $this;
  }

  public function maxTirada(){
    $arr = [];
    foreach ($this->getTiradas() as $tirada) {
        $arr[] = $tirada->getTotal();
    }
    return '<p class="maxima">La tirada mayor ha sido de: ' 
        . max($arr) . '</p>';
  }

  public function mostrar(){
    foreach ($this->getTiradas() as $tirada) {
        echo $tirada->mostrar();
    }
    return $this;
  }

}
