<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
  <head>
    <meta charset="UTF-8">
    <title>Práctica 2 Ejercicio 7</title>
    <style type="text/css">
      #Layer1{
        position: absolute;
        left: 90px;
        top: 115px;
        width: 323px;
        height: 504px;
        z-index: 1;
        padding: 10px;
        border: medium solid #0000ff;
      }
      #Layer2{
        position: absolute;
        left: 519px;
        top: 170px;
        width: 503px;
        height: 266px;
        z-index: 2;  
      }
    </style>
  </head>
  <body>
    <?php
    $dias_semana = array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');
    $colores = array("uno"=>'Rojo', "dos"=>'verde',"tres"=>'azul');
    ?>
    <div id="Layer1">
      <p>Los días de la semana son:</p>
      <?php
      echo "<p>$dias_semana[0]</p>";
      echo "<p>$dias_semana[1]</p>";
      echo "<p>$dias_semana[2]</p>";
      echo "<p>$dias_semana[3]</p>";
      echo "<p>$dias_semana[4]</p>";
      echo "<p>$dias_semana[5]</p>";
      echo "<p>$dias_semana[6]</p>";
      ?>
    </div>
    <div id="Layer2">
      <p>Los colores son:</p>
      <?php
      echo "<p>$colores[uno]</p>";
      echo "<p>$colores[dos]</p>";
      echo "<p>" , $colores['tres'] , "</p>";
      ?>
    </div>
  </body>
</html>
