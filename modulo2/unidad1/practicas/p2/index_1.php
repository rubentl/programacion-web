<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
  <head>
    <meta charset="UTF-8">
    <title>Práctica 2 Ejercicio 1</title>
  </head>
  <body>
    <?php
// Cremos tres variables
$cadena1 = 'Pasa'; //Esta es una variable de tipo cadena.
$cadena2 = 'tiempos'; // Esta es otra variable de tipo cadena.
$cadena3 = $cadena1 . $cadena2; // La tercera cadena contiene la unión de las otras dos.
echo '<p>' , $cadena3 , '</p>';
    ?>
  </body>
</html>
