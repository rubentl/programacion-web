<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
  <head>
    <meta charset="UTF-8">
    <title>Práctica 2 Ejercicio 8</title>
    <style type="text/css">
      #Layer1{
        position: absolute;
        left: 90px;
        top: 115px;
        width: 323px;
        height: 504px;
        z-index: 1;
        padding: 10px;
        border: medium solid #0000ff;
      }
      #Layer2{
        position: absolute;
        left: 519px;
        top: 170px;
        width: 503px;
        height: 266px;
        z-index: 2;  
      }
    </style>
  </head>
  <body>
    <?php
    $dias_semana = array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');
    $colores = array("uno" => 'Rojo', "dos" => 'verde', "tres" => 'azul');
    ?>
    <div id="Layer1">
      <p>Los días de la semana son:</p>
      <?php
      for ($i = 0; $i < 7; $i++) {
        echo "<p>$dias_semana[$i]</p>";
      }
      ?>
    </div>
    <div id="Layer2">
      <p>Los colores son:</p>
      <?php
      foreach ($colores as $value) {
        echo "<p>$value</p>";
      }
      ?>
    </div>
  </body>
</html>
