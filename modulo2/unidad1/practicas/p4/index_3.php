<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>Práctica 4 Ejercicio 3</title>
  </head>
  <body>
    <div>
      <p>Crea un array con 10 números. debe calcularse a través de php:</p>
      <ul>
        <li>Media aritmética</li>
        <li>Moda</li>
        <li>Mediana</li>
        <li>Desviación típica</li>
      </ul>
  </div>
    <?php

    function conDiv($mensaje) {
      return '<div>' . $mensaje . '</div>';
    }

    function media($arr) {
      return array_sum($arr) / count($arr);
    }

    function moda($arr) {
      $orden = array_count_values($arr);
      arsort($orden);
      return array_keys($orden)[0];
    }

    function mediana($arr) {
      sort($arr);
      $idx = round(count($arr) / 2);
      return $arr[$idx];
    }

    function desviacion($arr) {
      $media = media($arr);
      $carry = 0.0;
      foreach ($arr as $val) {
        $d = ((double) $val) - $media;
        $carry += $d * $d;
      }
      return sqrt($carry / count($arr));
    }

    $numeros = [];
    for ($i = 0; $i < 10; $i++) {
      array_push($numeros, rand(1, 50));
    }
    echo "<div>El array es: ";
    var_dump($numeros);
    echo "</div>";
    echo conDiv('La media aritmética es: ' . media($numeros));
    echo conDiv("La moda es: " . moda($numeros));
    echo conDiv("La mediana es: " . mediana($numeros));
    echo conDiv("La desviación es: " . desviacion($numeros));
    ?>
  </body>
</html>
