<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>Práctica 4 Ejercicio 2</title>
  </head>
  <body>
    <div>
      <p>Realiza una web que nos prmita introducir una frase y debe contar 
        su número de vocales</p>
    </div>
     <form action="index_2.php" method="get">
        <p>
          <label for="frase"></label>
          <input id="frase" type="text" name="frase" 
                 placeholder="Escribe una frase" size="100">
          <input type="submit" value="enviar" name="enviar">
        </p>
      </form>
    
    <?php
    if (isset($_REQUEST["enviar"])):
      $frase = $_REQUEST['frase'];
      $vocales = 0;
      for($i=0;$i<strlen($frase);$i++){
        switch (strtolower($frase[$i])) {
          case "a":
          case "e":
          case "i":
          case "o":
          case "u":
            $vocales++;
        }
      }
    ?>
    <div>
      <p>El número de vocales de la frase que has introducido</p>
      <p><?=$frase;?></p>
      <p>es: <?=$vocales;?></p>
    </div>
    <?php endif ?>
  </body>
</html>
