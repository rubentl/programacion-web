<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>Práctica 4 Ejercicio 4</title>
  </head>
  <body>
    <div>
      <p>Cálculo del área y perímetro de un triángulo dados sus lados, 
        mediante la fórmula de Herón. Note que si el radial del área es negativo, 
        los lados proporcionados no forman un triángulo</p>
    </div>
    <?php
/**
 * 
 * @return type
 */
    function alea() {
      return rand(5, 20);
    }
/**
 * 
 * @param type $men
 * @return type
 */
    function conDiv($men) {
      return '<div><p>' . $men . '</p></div>';
    }

    $ladoA = alea();
    $ladoB = alea();
    $ladoC = alea();

    $perim = ($ladoA + $ladoB + $ladoC) / 2;
    $area = sqrt($perim * ($perim - $ladoA) * ($perim - $ladoB) * ($perim - $ladoC));

    echo conDiv("Dados los lados $ladoA, $ladoB y $ladoC");
    echo conDiv("El área con la fórmula Herón es: $area");
    ?>
  </body>
</html>
