<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>Práctica 4 ejercicio 1</title>
  </head>
  <body>
    <div>
      <p>Realizar una web que lea tres números y los imprima en orden creciente 
        e indica si fueron introducidos en ese orden</p>
    </div>
    <?php
    if (!key_exists('enviar', $_POST)) {
      ?>
      <form action="index_1.php" method="post">
        <p>
          <label for="numeros"></label>
          <input id="numeros" type="text" name="numeros" 
                 placeholder="tres números separados por espacios" size="100">
          <input type="submit" value="enviar" name="enviar">
        </p>
      </form>
      <?php
    } else {
      $numeros = explode(" ", $_POST['numeros']);
      $error = FALSE;
      foreach ($numeros as $key => $value) {
        if (is_numeric($value)) {
          $numeros[$key] = intval($value);
        } else {
          $error = TRUE;
          break;
        }
      }
      if ($error){
        echo "Por favor, sólo números";
      } else{
        $original = $numeros;
        sort($numeros, SORT_NUMERIC);
        foreach ($numeros as $value) {
          echo  "<br>" . $value;
        }
        echo "<br>Los números están introducidos en";
        if ($original == $numeros){
          echo " orden.";
        }else{
          echo " desorden.";
        }
      }
    }
    ?>
  </body>
</html>
