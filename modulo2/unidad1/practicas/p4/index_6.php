<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
<?php
if (!key_exists('enviar', $_REQUEST)) {
?>
      <form action="index_6.php" method="get">
        <p>
          <label for="numero"></label>
          <input id="numero" type="text" name="numero" 
                 placeholder="Dame un número y te doy su tabla de multiplicar" size="60">
          <input type="submit" value="enviar" name="enviar">
        </p>
      </form>
<?php
} else {
    if (empty($_REQUEST["numero"])){
        $num = rand(1, 100);
    }else{
        $num = intval($_REQUEST["numero"]);
    }
    $formato = '%d x %2d = %4d <br>';
    echo "tabla de multiplicar del número $num <br>";
    for ($i = 0; $i <= 10; $i++) {
        echo str_replace(' ', '&nbsp;', sprintf($formato, $num, $i, ($num*$i)));
    }
}
?>
  </body>
</html>
