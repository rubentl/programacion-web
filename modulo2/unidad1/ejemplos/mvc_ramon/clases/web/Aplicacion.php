<?php

/*
  Ejemplo 2016
 */

namespace clases\web;

/**
 * Description of Aplicacion
 *
 * @author ramon
 */
class Aplicacion {
    private $controlador;
    private $accion;
    private $valores;

    public function __construct() {
        //require_once "/config/constantes.php";
        $this->setControladorAccion();
        $this->setValores();
        $this->llamarControlador();
    }

    public function setControladorAccion() {
        $parametros = $_SERVER["PHP_SELF"];
        if ($parametros == $_SERVER["SCRIPT_NAME"]) {
            $this->controlador = "site";
            $this->accion = "index";
        } else {
            $longitud = strlen($_SERVER["SCRIPT_NAME"]);
            $parametros = substr($parametros, $longitud);
            $parametros = substr($parametros, strpos($parametros, "/") + 1);
            $parametros = explode("/", $parametros);
            $this->controlador = $parametros[0];
            $this->accion = $parametros[1];
        }
    }

    public function getControlador() {
        return $this->controlador;
    }

    public function getAccion() {
        return $this->accion;
    }

    public function llamarControlador() {
        require_once "controladores/" . $this->getControlador() . "Controlador.php";
        /*
         * Autoejecute la accion correspondiente
         */
        $accion = $this->getAccion() . "Accion";
        $accion($this);
    }

    public function setValores() {
        if ($_GET) {
            $this->valores = $_GET;
        } else {
            $this->valores = "";
        }
    }

    public function getValores() {
        return $this->valores;
    }

    public static function estilos($ruta) {
        $salida = '<style type="text/css">';
        $salida.= file_get_contents($ruta);
        $salida.='</style>';
        return $salida;
    }
    
 
    
    
}
