<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
  "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Documento sin t&iacute;tulo</title>
  </head>

  <body>  
    <?php
    $variable = 10;
    ?>
    El valor de la variable es
    <?php
    echo $variable;
    echo "<br>";
    echo "La variable contiene el valor $variable";
    echo "<br>";
    echo 'La variable contiene el valor $variable';
    ?>

    Voy a utilizar un array <br>
    <?php
    $meses[0] = "Enero";
    $meses[1] = "Febrero";
    echo $meses[0] . '---' . $meses[1];
    echo "<br>$meses[0]---$meses[1]";
    ?>
    <br>
    Voy a utilizar otro array
    <?php
    $dias[] = "Lunes";
    $dias[] = "Martes";
    for ($c = 0; $c < 2; $c++) {
      echo "$dias[$c],";
    }
    // Vamos a crear otro array
    $comidas = array("macarrones", "lentejas", "patatas");
    for ($c = 0; $c < 3; $c++) {
      echo "$comidas[$c],";
    }

    $altura = array("Norma" => 165, "Pepe" => 185, "Ana" => 175);
    //Voy a imprimir el primer elemento del array asociativo
    echo "<br>" . $altura["Norma"];
    echo "<br>$altura[Norma]";

    foreach ($altura as $valor) {
      echo "<br>$valor";
    }

    foreach ($altura as $indice => $valor) {
      echo "<br>$indice=$valor";
    }
    foreach ($GLOBALS as $idx => $valor){
      echo "<br>::=> [$idx]=>";
      if (is_array($valor)){
        foreach ($valor as $index => $subvalor){
          echo "<br>-------------> [$index]=> $subvalor";
        }
      }else{
        echo "<br>$valor";
      }
    }
    ?>


  </body>
</html>
