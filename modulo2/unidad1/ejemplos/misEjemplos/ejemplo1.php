<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
    <?php
    $a = 10;
    echo "La variable \$a vale: $a <br>";
    echo "La variable \$a es de tipo: " . gettype($a);
    unset($a);
    ?>
    <?php
    $variable1 = TRUE;
    echo "<br>" . gettype($variable1);
    settype($variable1, "integer");
    echo "<br>" . gettype($variable1) . "<br>";
    echo "Valor de \$variable1: $variable1";
    ?>
    <?php
    $a = 1;
    $b = 1.2;
    $c = "Hola";
    $d = [2, 3, 4, 5];
    echo "<div>La variable \$a: vale $a y es de tipo " . gettype($a) . "</div>";
    echo "<div>Toda la información sobre la variable</div>";
    var_dump($a);
    echo "<div>La variable \$a: vale $b y es de tipo " . gettype($b) . "</div>";
    echo "<div>Toda la información sobre la variable</div>";
    var_dump($b);
    echo "<div>La variable \$a: vale $c y es de tipo " . gettype($c) . "</div>";
    echo "<div>Toda la información sobre la variable</div>";
    var_dump($c);
    echo "<div>La variable \$a: vale $d y es de tipo " . gettype($d) . "</div>";
    echo "<div>Toda la información sobre la variable</div>";
    var_dump($d);
    ?>
    <?php
    $variable1 = "Normal";
    $variable2 = <<<EOT
            Esto es texto en formato heredoc
            de varias líneas<br>
            Formato heredoc.
EOT;
    $variable3 = <<<BLAS
            Este es otro texto en formato heredoc<br>
            dentro de la variable \$variable3.
BLAS;
    echo $variable1;
    ?>
    <div>
      <?php
      echo $variable2;
      ?>
    </div>
    <div>
      <?= $variable3 ?>
    </div>
  </body>
</html>
