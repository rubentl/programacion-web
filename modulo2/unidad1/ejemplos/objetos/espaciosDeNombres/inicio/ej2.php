<?php

namespace uno;

function sumar() {
    return "sumar_uno";
}

function restar() {
    return "restar_uno";
}

namespace dos;

function sumar() {
    return "sumar_dos";
}

function restar() {
    return "restar_dos";
}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
<?php
    echo sumar() . "<br>";

    namespace uno;
    echo sumar() . "<br>";

?>
    </body>
</html>
