<?php

/**
 * Description of Cadena
 *
 * @author ruben
 */
class Cadena {
    private $texto;
    private $contador;
    function __construct($txt='Hola mundo'){
        $this->setTexto($txt);
    }
    public function setTexto($txt){
        $this->texto = $txt;
        $this->contador = false;
        return $this;
    }
    public function getTexto(){
        return $this->texto;
    }
    public function caracterInicio(){
        return $this->getTexto()[0];
    }
    public function caracterFinal(){
        $idx = strlen($this->getTexto());
        return $this->getTexto()[$idx-1];
    }
    public function longitudVocales(){
        if (empty($this->contador)){
            $this->contador = 0;
            for ($i=0;$i<strlen($this->getTexto());$i++){
                switch (strtolower($this->getTexto()[$i])){
                case "a":
                case "e":
                case "i":
                case "o":
                case "u":
                    $this->contador++;
                }
            }
        }
        return $this->contador;
    }
}
