<?php
spl_autoload_register(
        function ($clase) {
  require_once "Clases/" . $clase . ".php";
}
);
?>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
    <?php
    $texto = new Frase('Don Quijote de la Mancha');
    echo 'La cadena "' . $texto->getTexto() . '" contiene:<br>';
    echo 'Número de caracteres:' . $texto->getCaracteres() . '<br>';
    echo 'Número de vocales:' . $texto->getVocales() . '<br>';
    echo 'Número de palabras:' . $texto->getPalabras() . '<br>';
    $texto->setTexto("Casillas");
    echo $texto->getTexto() . ' tiene ' . $texto->getVocales() . ' vocales<br>';
    $palabra = new Palabra("Hola");
    echo $palabra->getTexto() . ' tiene ' . $palabra->getVocales() . ' vocales<br>';
    $frase = new Frase("Hola");
    echo $frase->getTexto() . ' tiene ' . $frase->getPalabras() . ' palabras<br>';
    ?>
  </body>
</html>
