<?php

/**
 * Description of Palabra
 *
 * @author ruben
 */
class Palabra {

  private $texto;
  private $n_vocales;
  private $n_caracteres;

  function __construct($txt = 'Hola') {
    $this->setTexto($txt);
  }

  public function setTexto($txt) {
    $this->texto = $txt;
    $this->caracteres();
    $this->vocales();
    return $this;
  }

  public function getTexto() {
    return $this->texto;
  }

  public function getCaracteres() {
    return $this->n_caracteres;
  }

  public function getVocales() {
    return $this->n_vocales;
  }

  protected function caracteres() {
    $this->n_caracteres = strlen($this->getTexto());
    return $this->n_caracteres;
  }

  protected function vocales() {
    $this->n_vocales = 0;
    for ($i = 0; $i < $this->getCaracteres(); $i++) {
      switch (strtolower($this->getTexto()[$i])) {
        case "a":
        case "e":
        case "i":
        case "o":
        case "u":
          $this->n_vocales++;
      }
    }
  }

}
