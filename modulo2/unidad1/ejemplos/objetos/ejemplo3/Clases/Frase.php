<?php

/**
 * Description of Frase
 *
 * @author ruben
 */
class Frase extends Palabra {

  private $n_palabras;

  public function setTexto($txt) {
    parent::setTexto($txt);
    $this->palabras();
    return $this;
  }

  public function getPalabras() {
    return $this->n_palabras;
  }

  public function palabras() {
    $arr = explode(" ", $this->getTexto());
    $this->n_palabras = count($arr);
  }

}
