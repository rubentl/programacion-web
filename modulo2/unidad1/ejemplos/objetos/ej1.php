<?php require_once 'cosa.php'?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>ej1</title>
  </head>
  <body>
<?php
$objeto = new Cosa("verde", "alto", 200, 180);
echo '<br>' . $objeto->color;
echo '<br>' . Cosa::$fondo;
echo '<br>' . $objeto->getAltura();
$objeto1 = new Cosa("rojo", "alto", 200, 180);
echo '<br>' . $objeto1::$fondo;
Cosa::setFondo(45);
echo '<br>' . Cosa::$fondo;
echo '<br>' . $objeto::$fondo;
echo '<br>' . $objeto1::$fondo;
?>
  </body>
</html>
