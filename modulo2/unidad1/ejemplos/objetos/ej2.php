<?php require_once "Cadena.php" ?>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
<?php
$texto = new Cadena('Don Quijote de la Mancha');
echo 'La cadena ' . $texto->getTexto() . ' contiene -><br>';
echo 'Caracter de inicio:' . $texto->caracterInicio().'<br>';
echo 'Caracter de final:' . $texto->caracterFinal().'<br>';
echo 'Número de vocales:'. $texto->longitudVocales().'<br>';
$texto->setTexto("Casillas");
echo $texto->getTexto() . ' tiene ' . $texto->longitudVocales() . ' vocales';
?>
  </body>
</html>
