<?php
session_start();
spl_autoload_register(function($clase) {
  require_once $clase . '.php';
});
?>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title>Ejemplo de Mysqli</title>
  </head>
  <body>
    <?php

    use clases\Conexion;

    //$conexion = new mysqli('127.0.0.1', 'root', '', 'evento');
    //if ($conexion->connect_error) {
    //  die('Error de Conexión (' . $mysqli->connect_errno . ') '
    //          . $mysqli->connect_error);
    //}
    //$query = 'select * from alumnos';
    //$result = $conexion->query($query);
    //while ($value = $result->fetch_assoc()) {
    //  var_dump($value);
    //}
    //while ($value = $result->fetch_object('clases\Alumnos')) {
    //  var_dump($value);
    //}
    //$conexion->close();
    $conexion = new Conexion(['database'=>'evento']);
    var_dump($conexion->salida);
    $conexion->consulta('select * from alumnos');
    var_dump($conexion->salida);
    $conexion->baseDatos('muebles');
    $conexion->consulta('select * from muebles');
    var_dump($conexion->salida);
    ?>
  </body>
</html>
