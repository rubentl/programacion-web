<?php

namespace clases;

/**
 * Description of Conexion
 *
 * @author ruben
 */
class Conexion {

  private $enlace;
  private $resultado;
  public $salida;

  function __construct($param = []) {
    $default = ['host' => '127.0.0.1',
        'user' => 'root',
        'pass' => '',
        'database' => 'evento'];
    $tmp_param = array_merge($default, $param);
    $this->enlace = new \mysqli($tmp_param['host'], $tmp_param['user'], $tmp_param['pass'], $tmp_param['database']);
    $this->consulta('show databases');
  }

  public function consulta($query) {
    $this->resultado = $this->enlace->query($query);
    if (gettype($this->resultado) == "object") {
      $this->crearSalida();
    }
  }

  public function crearSalida() {
    $this->salida = $this->resultado->fetch_all();
  }

  public function baseDatos($baseDatos) {
    $this->enlace->select_db($baseDatos);
  }

}
