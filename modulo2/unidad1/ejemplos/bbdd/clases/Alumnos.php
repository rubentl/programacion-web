<?php

namespace clases;

/**
 * Description of Alumnos
 *
 * @author ruben
 */
class Alumnos {

  public $id_alumno;
  public $nombre;
  public $apellidos;
  public $email;

  public function __toString() {
    return 'El registro es ' . $this->id_alumno;
  }

}
