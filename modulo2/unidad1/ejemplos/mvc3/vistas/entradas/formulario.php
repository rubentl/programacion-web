<div class="jumbotron">
<div class="container">
  <form class="form-horizontal" name="unico" method="get" 
        action="<?= $this->crearRuta(["accion" => "modificar", "controlador" => "entradas"]) ?>">
        <div class="form-group">
      <label for="iId" class="control-label col-sm-2">Id</label>
      <div class="col-sm-10"><input readonly type="text" class="form-control" id="iId" name="id" value="<?= $this->datos["id"]?>"></div>
    </div>
    <div class="form-group">
      <label for="iTitulo" class="control-label col-sm-2">Título</label>
      <div class="col-sm-10"><input type="text" class="form-control" id="iTitulo" name="titulo" value="<?= $this->datos["titulo"]?>"></div>
    </div>
    <div class="form-group">
      <label for="iTexto" class="control-label col-sm-2">Texto</label>
      <div class="col-sm-10"><input type="text" class="form-control" id="iTexto" name="texto" value="<?= $this->datos["texto"]?>"></div>
    </div>
         <button type="submit" class="btn btn-primary col-sm-2 col-sm-offset-10">Insertar</button>
  </form>
</div></div>

