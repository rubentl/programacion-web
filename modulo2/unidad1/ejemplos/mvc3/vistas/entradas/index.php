<div class="jumbotron">
<div class="container">
  <form class="form-horizontal" name="unico" method="get" 
        action="<?= $this->crearRuta(["accion" => "modificar", "controlador" => "entradas"]) ?>">
    <div class="form-group">
      <label for="iTitulo" class="control-label col-sm-2">Título</label>
      <div class="col-sm-10"><input type="text" class="form-control" id="iTitulo" name="titulo" ></div>
    </div>
    <div class="form-group">
      <label for="iTexto" class="control-label col-sm-2">Texto</label>
      <div class="col-sm-10"><input type="text" class="form-control" id="iTexto" name="texto"></div>
    </div>
    <button type="submit" class="btn btn-primary col-sm-2 col-sm-offset-10">Insertar</button>
  </form>
</div>
</div>

<?php

echo '<div class="listado">';
foreach ($this->contenidoArray as $indice => $registro) {

    echo "<ul>";
    foreach ($registro as $campo => $valor) {
        echo "<li>$campo: $valor</li>";
    }
    echo '<li><a href="' . $this->crearRuta(["accion" => "editar", 
        "controlador" => "entradas",
        "parametros" => "id=" . $registro['id']]) . '"><img src=" ' . $this->urlBase . '/imgs/editar.png"></a></li>';
    echo '<li><a href="' . $this->crearRuta(["accion" => "eliminar", 
         "controlador" => "entradas",
        "parametros" => "id=" . $registro['id']]) . '"><img src=" ' . $this->urlBase . '/imgs/eliminar.png"></a></li></ul>';
}
echo "</div>";
