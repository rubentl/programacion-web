<div class="jumbotron">
  <div class="container">
    <form class="form-horizontal" name="unico" enctype="multipart/form-data" method="post" 
          action="<?= $this->crearRuta(["accion" => "modificar", "controlador" => "usuarios"]) ?>">
      <div class="form-group">
        <label for="iNombre" class="control-label col-sm-2">Nombre</label>
        <div class="col-sm-10"><input type="text" class="form-control" id="iNombre" name="Nombre"></div>
      </div>
      <div class="form-group">
        <label for="iEmail" class="control-label col-sm-2">Email</label>
        <div class="col-sm-10"><input type="email" class="form-control" id="iEmail" name="Email"></div>
      </div>
      <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
      <div class="form-group">
        <label for="iFoto" class="control-label col-sm-2">Foto</label>
        <div class="col-sm-10">
          <input type="file" class="form-control" id="iFoto" name="Foto">
        </div>
      </div>
      <button type="submit" class="btn btn-primary col-sm-2 col-sm-offset-10">Insertar</button>
    </form>
  </div>
</div>
