<div class="container">
<form name="unico" enctype="multipart/form-data" method="post" action="<?= $this->crearRuta(["accion" => "modificar", "controlador" => "usuarios"]) ?>">
  <div class="form-group">
    <label for="iId">Id</label>
    <input readonly type="text" class="form-control" id="iId" value="<?= $this->datos["id"] ?>">
  </div>
  <div class="form-group">
    <label for="iNombre">Nombre</label>
    <input type="text" class="form-control" id="iNombre" value="<?= $this->datos["nombre"] ?>">
  </div>
  <div class="form-group">
    <label for="iEmail">Email</label>
    <input type="email" class="form-control" id="iEmail" value="<?= $this->datos["email"] ?>">
  </div>
  <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
  <div class="form-group">
    <img src="<?= $this->urlBase . $this->datos["foto"] ?>" />
    <label for="iFoto">Foto</label>
    <input type="file" class="form-control" id="iFoto">
  </div>
  <button type="submit" class="btn btn-default">Insertar</button>
</form>
</div>


