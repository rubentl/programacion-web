<?php

echo '<div class="jumbotron">';
echo '<div class="listado"><form name="usuarios-form" method="get" action="'
 . $this->crearRuta(["controlador" => "usuarios", "accion" => "eliminar"]) . '" >';
echo "<li></li><li></li><li><a class=\"btn btn-primary\" href=\"" . $this->crearRuta([
    "controlador" => "usuarios",
    "accion" => "nuevo"]) . "\">Nuevo Usuario</a></li><li></li><li></li><li><button type=\"submit\" class=\"btn btn-primary\">Borrar</button></li>";
foreach ($this->contenidoArray as $indice => $registro) {

  echo "<ul>";
  foreach ($registro as $campo => $valor) {
    if ($campo == 'foto') {
      echo "<li><img src=\"" . $this->urlBase . $valor . "\"></li>";
    } else {
      echo "<li>$campo: $valor</li>";
    }
  }
  echo '<li><a href="' . $this->crearRuta([
      "accion" => "editar",
      "controlador" => "usuarios",
      "parametros" => "id=" . $registro['id']]) . '"><img src=" ' . $this->urlBase . '/imgs/editar.png"></a></li>';
  echo '<li><div class="form-check">
  <input class="form-check-input position-static" type="checkbox" name="' . $registro['id'] . '" value="borrar">
</div></li></ul>';
}
echo "</form></div></div>";
