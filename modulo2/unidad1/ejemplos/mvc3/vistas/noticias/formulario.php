<div class="jumbotron">
<div class="container">
  <form class="form-horizontal" name="unico" method="get" 
        action="<?= $this->crearRuta(["accion" => "modificar", "controlador" => "noticias"]) ?>">
        <div class="form-group">
      <label for="iId" class="control-label col-sm-2">Id</label>
      <div class="col-sm-10"><input readonly type="text" class="form-control" id="iId" name="id" value="<?= $this->datos["id"]?>"></div>
    </div>
    <div class="form-group">
      <label for="iTitulo" class="control-label col-sm-2">Título</label>
      <div class="col-sm-10"><input type="text" class="form-control" id="iTitulo" name="Titulo" value="<?= $this->datos["Titulo"]?>"></div>
    </div>
    <div class="form-group">
      <label for="iTextoCorto" class="control-label col-sm-2">TextoCorto</label>
      <div class="col-sm-10"><input type="text" class="form-control" id="iTextoCorto" name="TextoCorto" value="<?= $this->datos["TextoCorto"]?>"></div>
    </div>
        <div class="form-group">
      <label for="iTextoLargo" class="control-label col-sm-2">TextoLargo</label>
      <div class="col-sm-10"><input type="text" class="form-control" id="iTextoLargo" name="TextoLargo" value="<?= $this->datos["TextoLargo"]?>"></div>
    </div>
            <div class="form-group">
      <label for="iFecha" class="control-label col-sm-2">Fecha</label>
      <div class="col-sm-10"><input type="date" class="form-control" id="iFecha" name="Fecha" value="<?= $this->datos["Fecha"]?>"></div>
    </div>
    <button type="submit" class="btn btn-primary col-sm-2 col-sm-offset-10">Insertar</button>
  </form>
</div></div>


