<div class="jumbotron">
<div class="container">
  <form class="form-horizontal" name="unico" method="get" 
        action="<?= $this->crearRuta(["accion" => "modificar", "controlador" => "noticias"]) ?>">
    <div class="form-group">
      <label for="iTitulo" class="control-label col-sm-2">Título</label>
      <div class="col-sm-10"><input type="text" class="form-control" id="iTitulo" name="Titulo" ></div>
    </div>
    <div class="form-group">
      <label for="iTextoCorto" class="control-label col-sm-2">TextoCorto</label>
      <div class="col-sm-10"><input type="text" class="form-control" id="iTextoCorto" name="TextoCorto"></div>
    </div>
        <div class="form-group">
      <label for="iTextoLargo" class="control-label col-sm-2">TextoLargo</label>
      <div class="col-sm-10"><input type="text" class="form-control" id="iTextoLargo" name="TextoLargo"></div>
    </div>
            <div class="form-group">
      <label for="iFecha" class="control-label col-sm-2">Fecha</label>
      <div class="col-sm-10"><input type="date" class="form-control" id="iFecha" name="Fecha"></div>
    </div>
    <button type="submit" class="btn btn-primary col-sm-2 col-sm-offset-10">Insertar</button>
  </form>
</div></div>

<?php

echo '<div class="listado">';
foreach ($this->contenidoArray as $indice => $registro) {

    echo "<ul>";
    foreach ($registro as $campo => $valor) {
        echo "<li>$campo: $valor</li>";
    }
    echo '<li><a href="' . $this->crearRuta(["accion" => "editar",
        "controlador" => "noticias",
        "parametros" => "id=" . $registro['id']]) . '"><img src=" ' . $this->urlBase . '/imgs/editar.png"></a></li>';
    echo '<li><a href="' . $this->crearRuta(["accion" => "eliminar", 
        "controlador" => "noticias",
        "parametros" => "id=" . $registro['id']]) . '"><img src=" ' . $this->urlBase . '/imgs/eliminar.png"></a></li></ul>';
}
echo "</div>";
