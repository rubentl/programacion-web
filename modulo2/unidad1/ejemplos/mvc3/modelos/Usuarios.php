<?php

namespace modelos;

/**
 * Description of Usuarios
 *
 * @author ruben
 */
class Usuarios extends Modelo{
    public $nombre;
    public $email;
    public $foto;
    public $id;
    protected function campos(){
        return "nombre,email,foto";
    }
    protected function primary(){
        return "id";
    }
}
