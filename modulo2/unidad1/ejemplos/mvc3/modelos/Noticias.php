<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modelos;

/**
 * Description of Noticias
 *
 * @author ruben
 */
class Noticias extends Modelo {

  public $Titulo;
  public $TextoLargo;
  public $Fecha;
  public $TextoCorto;
  public $id;

  protected function campos() {
    return "Titulo,TextoCorto,TextoLargo,Fecha";
  }

  protected function primary() {
    return "id";
  }

}
