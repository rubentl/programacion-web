<?php

namespace controladores;

class usuariosController extends Controller {

  public function listarAccion($datos) {

    $modelo1 = new \modelos\Usuarios();
    $datos = $modelo1->todos();
    $this->render([
        "vista" => "usuarios/index",
        "contenidoArray" => $datos,
        "pie" => "Listado de Usuarios",
        "activo" => "Usuarios"
    ]);
  }

  public function editarAccion($datos) {
    $modelo = new \modelos\Usuarios();
    $datos = $modelo->listado1(["condicion" => "id=" . $datos["id"]]);
    $this->render([
        "vista" => "usuarios/formulario",
        "datos" => $datos,
        "pie" => "Modificar un dato",
        "activo" => "Usuarios"
    ]);
  }

  public function nuevoAccion($datos) {
    $modelo = new \modelos\Usuarios();
    $this->render([
        "vista" => "usuarios/nuevo",
        "datos" => $modelo,
        "pie" => "Un nuevo usuario",
        "activo" => "Usuarios"
    ]);
  }

  public function modificarAccion($datos) {
    $modelo = new \modelos\Usuarios();
    if (isset($datos["Id"])) {
      $modelo->id = $datos["Id"];
    }
    $modelo->nombre = $datos["Nombre"];
    $modelo->email = $datos["Email"];
    $foto_tmp = $_FILES["Foto"]["tmp_name"];
    $foto_nombre = basename($_FILES["Foto"]["name"]);
    $foto_destino = 'imgs/usuarios/' . $foto_nombre;
    move_uploaded_file($foto_tmp, $foto_destino);
    $modelo->foto = '/' . $foto_destino;
    if (isset($datos["Id"])) {
      $modelo->save();
    } else {
      $modelo->insert();
    }
    $this->listarAccion($datos);
  }

  public function eliminarAccion($datos) {
    $modelo = new \modelos\Usuarios();
    foreach ($datos as $key => $value) {
      $modelo->id = $key;
      $modelo->eliminar();
    }
    $this->listarAccion($datos);
  }

}
