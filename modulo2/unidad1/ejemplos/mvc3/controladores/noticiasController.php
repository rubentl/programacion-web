<?php

namespace controladores;

/**
 * Description of noticiasController
 *
 * @author ruben
 */
class noticiasController extends Controller {

  public function portadaAccion($datos) {
    $modelo = new \modelos\Noticias();
    $datos = $modelo->listado1(["condicion" => "1"]);
    $this->render([
        "vista" => "noticias/portada",
        "datos" => $datos,
        "pie" => "Portada de Noticias",
        "activo" => "Inicio"
    ]);
  }

  public function listarAccion($datos) {

    $modelo1 = new \modelos\Noticias();
    $datos = $modelo1->todos();
    $this->render([
        "vista" => "noticias/index",
        "contenidoArray" => $datos,
        "pie" => "Listado de Noticias",
        "activo" => "Noticias"
    ]);
  }

  public function editarAccion($datos) {
    $modelo = new \modelos\Noticias();
    $datos = $modelo->listado1(["condicion" => "id=" . $datos["id"]]);
    $this->render([
        "vista" => "noticias/formulario",
        "datos" => $datos,
        "pie" => "Modificar un dato",
        "activo" => "Noticias"
    ]);
  }

  public function modificarAccion($datos) {
    $modelo = new \modelos\Noticias();
    if (isset($datos["id"])) {
      $modelo->id = $datos["id"];
    }
    $modelo->Titulo = $datos["Titulo"];
    $modelo->TextoLargo = $datos["TextoLargo"];
    $modelo->TextoCorto = $datos["TextoCorto"];
    $modelo->Fecha = $datos["Fecha"];
    if (isset($datos["id"])) {
      $modelo->save();
    } else {
      $modelo->insert();
    }
    $this->listarAccion($datos);
  }

  public function eliminarAccion($datos) {
    $modelo = new \modelos\Noticias();
    $modelo->id = $datos["id"];
    $modelo->eliminar();
    $this->listarAccion($datos);
  }

}
