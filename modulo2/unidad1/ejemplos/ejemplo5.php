<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
  "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>Documento sin t&iacute;tulo</title>
    <style>
      .tabla{
        display:table;
        width: 500px;
        margin: 30px auto;
      }
      .fila{
        display:table-row;
      }
      .fila:nth-child(odd){
        background-color: #eee;
      }
      .celda{
        display:table-cell;
        text-align: center;
        line-height: 50px;
      }
    </style>
  </head>
  <body>
    <div class="tabla">
      <?php
      $number = 1;
      for ($f = 0; $f < 10; $f++) {
        echo "<div class=\"fila\">";
        for ($c = 0; $c < 5; $c++) {
          echo "<div class=\"celda\">$number</div>";
          $number++;
        }
        echo "</div>";
      }
      ?>
    </div>
  </body>
</html>
