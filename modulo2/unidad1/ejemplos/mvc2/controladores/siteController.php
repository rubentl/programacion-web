<?php

namespace controladores;

class siteController extends Controller {

  public function indexAccion($datos) {
    $this->aplicacion->conexion->ejecutar("Select * from entradas where tipos = 'Menú'");
    $this->render([
        "vista" => "index",
        "pie" => "Estamos probando la tabla index",
        "contenido" => $this->aplicacion->conexion,
        "menu" => (new \clases\Menu([
    "Inicio" => $this->crearRuta(["accion" => "index"]),
    "Entradas" => $this->crearRuta(["accion" => "entradas"])
        ], "Inicio"))->html()
    ]);
  }

  public function entradasAccion($datos) {
    $this->aplicacion->conexion->ejecutar("Select * from entradas where tipos = 'Noticias'");
    $this->render([
        "vista" => "formulario",
        "pie" => "Listado de entradas",
        "contenido" => $this->aplicacion->conexion,
        "menu" => (new \clases\Menu([
    "Inicio" => $this->crearRuta(["accion" => "index"]),
    "Entradas" => $this->crearRuta(["accion" => "entradas"])
        ], "Entradas"))->html()
    ]);
  }

}
