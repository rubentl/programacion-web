<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
  "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>Documento sin t&iacute;tulo</title>
  </head>

  <body>
    <table width="80%" border="0">
      <tr>
        <td>
          <?= "Esta es la primera celda"; ?>
        </td>
        <td>
          <?= date("d"); ?>
        </td>
      </tr>
      <tr>
        <td>
          <?= date("d/m/y"); ?>
        </td>
        <td>
          <?= date("d") . " del " . date("y"); ?>
        </td>
      </tr>
    </table>
  </body>
</html>
