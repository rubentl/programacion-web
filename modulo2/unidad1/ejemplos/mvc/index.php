<?php
session_start();
spl_autoload_register(function($clase) {
  require_once $clase . '.php';
});
?>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
    <?php
    use clases\web\Aplicacion;
    $app = new Aplicacion();
    ?>
  </body>
</html>
