<?php

namespace clases;

/**
 * Description of Noticias
 *
 * @author ruben
 */

class Noticias {
  
protected $datos;


  function __construct($consulta) {
    while ($registro = $consulta->fetch_object("\clases\Modelo")){
      $this->datos[] = $registro;
    };
  }

  public function html() {
    $result = $this->estilos();
    $result .= "<div>";
    $result .= $this->columna(4);
    $result .= "</div>";
    return $result;
  }

  private function titulo($texto) {
    return "<h2>$texto</h2>";
  }

  private function contenido($texto) {
    return "<p>$texto</p>";
  }

  private function noticia($titulo, $noticia) {
    return "<div class=\"noticia\">"
            . $this->titulo($titulo)
            . $this->contenido($noticia)
            . "</div>";
  }

  private function columna($numero_noticias) {
    $result = "<div class=\"columna\">";
    for ($i = 0; $i < $numero_noticias; $i++) {
      $result .= $this->datos[$i]->html();
    }
    $result .= "</div>";
    return $result;
  }

  private function estilos() {
    return "<style type=\"text/css\"> .columna{width:300px; height:300px;display:inline-block;} "
    . ".noticia{text-align:center;margin-bottom:20px} </style>";
  }

}

class Modelo {
  public function html(){
    $result = '<div class="noticia">';
    $result .= '<h2>' . $this->titulo . '</h2>';
    $result .= '<p>' . $this->texto . '</p>';
    $result .= '</div>';
    return $result;
  }
}
