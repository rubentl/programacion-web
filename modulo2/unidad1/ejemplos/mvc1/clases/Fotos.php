<?php

namespace clases;

/**
 * Description of Fotos
 *
 * @author ruben
 */
class Fotos {

  protected $fotos;

  function __construct($fotos) {
    $this->fotos = $fotos;
  }

  public function html() {
    $columnas = count($this->fotos) / 2;
    $index = 0;
    $result = $this->estilos();
    $result .= "<div>";
    for ($i = 0; $i < $columnas; $i++) {
      $result .= $this->columna([$this->fotos[$index], $this->fotos[$index + 1]]);
      $index += 2;
    }
    $result .= "</div>";
    return $result;
  }

  private function titulo($texto) {
    return "<h2>$texto</h2>";
  }

  private function foto($foto) {
    return "<p><img src=\"$foto\" ></p>";
  }

  private function noticia($titulo, $foto) {
    return "<div class=\"noticia\">"
            . $this->titulo($titulo)
            . $this->foto($foto)
            . "</div>";
  }

  private function columna($datos) {
    $result = "<div class=\"columna\">";
    for ($i = 0; $i < count($datos); $i++) {
      $result .= $this->noticia($datos[$i]['titulo'], $datos[$i]['foto']);
    }
    $result .= "</div>";
    return $result;
  }

  private function estilos() {
    return "<style> .columna{width:300px; height:300px;display:inline-block;} "
            . ".noticia img{width:50%;}"
            . ".noticia{text-align:center;margin-bottom:50px} </style>";
  }

}
