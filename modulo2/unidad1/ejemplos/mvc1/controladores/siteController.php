<?php

namespace controladores;

class siteController extends Controller {

  public function indexAccion($objeto) {
    $this->render([
        "vista" => "index",
        "pie" => "Estás en el Inicio",
        "menu" => (new \clases\Menu([
    "Inicio" => $this->crearRuta(["accion" => "index"]),
    "Noticias" => $this->crearRuta(["accion" => "noticias"]),
    "Fotos" => $this->crearRuta(["accion" => "fotos"]),
    "Contacto" => $this->crearRuta(["accion" => "contacto"])
        ], "Inicio"))->html()
    ]);
  }

  public function noticiasAccion($objeto) {
    $query = "SELECT titulo, texto from entradas where tipos = 'Noticias' limit 4";
    if ($consulta = $this->query($objeto->database, $query)) {
      $this->render([
          "vista" => "noticias",
          "pie" => "Estás en las noticias",
          "contenido" => (new \clases\Noticias($consulta))->html(),
          "menu" => (new \clases\Menu([
      "Inicio" => $this->crearRuta(["accion" => "index"]),
      "Noticias" => $this->crearRuta(["accion" => "noticias"]),
      "Fotos" => $this->crearRuta(["accion" => "fotos"]),
      "Contacto" => $this->crearRuta(["accion" => "contacto"])
          ], "Noticias"))->html()
      ]);
      $consulta->close();
    }
  }

  public function fotosAccion($objeto) {
    $this->render([
        "vista" => "fotos",
        "pie" => "Estás en las fotos",
        "contenido" => (new \clases\Fotos([
    ['titulo' => 'bolas', 'foto' => $this->urlBase . '/imgs/bolas.png'],
    ['titulo' => 'árbol', 'foto' => $this->urlBase . '/imgs/arbol.jpg'],
    ['titulo' => 'calcetín', 'foto' => $this->urlBase . '/imgs/calcetin.jpg'],
    ['titulo' => 'cartel', 'foto' => $this->urlBase . '/imgs/cartel.jpg']
        ]))->html(),
        "menu" => (new \clases\Menu([
    "Inicio" => $this->crearRuta(["accion" => "index"]),
    "Noticias" => $this->crearRuta(["accion" => "noticias"]),
    "Fotos" => $this->crearRuta(["accion" => "fotos"]),
    "Contacto" => $this->crearRuta(["accion" => "contacto"])
        ], "Fotos"))->html()
    ]);
  }

  public function contactoAccion($objeto) {
    $this->render([
        "vista" => "contacto",
        "pie" => "Estás en el formulario de contacto",
        "menu" => (new \clases\Menu([
    "Inicio" => $this->crearRuta(["accion" => "index"]),
    "Noticias" => $this->crearRuta(["accion" => "noticias"]),
    "Fotos" => $this->crearRuta(["accion" => "fotos"]),
    "Contacto" => $this->crearRuta(["accion" => "contacto"])
        ], "Contacto"))->html()
    ]);
  }

  public function listarAccion($objeto) {
    $this->render([
        "vista" => "listar",
        "contenido" => $objeto->getValores(),
        "menu" => (new \clases\Menu([
    "Inicio" => $this->crearRuta(["accion" => "index"]),
    "Noticias" => $this->crearRuta(["accion" => "noticias"]),
    "Fotos" => $this->crearRuta(["accion" => "fotos"]),
    "Contacto" => $this->crearRuta(["accion" => "contacto"])
        ], "Contacto"))->html()
    ]);
  }

}
