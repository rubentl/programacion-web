window.addEventListener("load", function(){
    document.querySelector("form>button").addEventListener("click", function(evt){

        var correo = document.querySelector("#correo");
        var valor = correo.value;
        var indice = valor.indexOf("@");

        evt.preventDefault();

        function error(){
            correo.style.color = "red";
            correo.value = "ERROR: El correo debe ser válido";
        };

        if (indice === -1){
            error();
        }else if (valor.indexOf(".", indice) === -1){
            error();
        } else{
            document.querySelector("form").submit();
        }
    })

})
