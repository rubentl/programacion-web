window.addEventListener("load", function() {

    var nombre = {
        el: document.querySelector(".nombre"),
        padre: document.querySelector(".contenedor"),
        timerId: null,
        setXY: function(x, y) {
            this.el.style.top = y + "px";
            this.el.style.left = x + "px";
        },
        random: function() {
            this.ancho = this.el.clientWidth;
            this.alto = this.el.clientHeight;
            var ancho = this.padre.clientWidth;
            var alto = this.padre.clientHeight;
            var x = (Math.random() * ancho | 0) + this.ancho;
            var y = (Math.random() * alto | 0) + this.alto;
            if (x > ancho) x = ancho - this.ancho;
            if (y > alto) y = alto - this.alto;
            return {
                "x": x,
                "y": y
            };
        },
        nuevoXY: function(_this) {
            var valores = _this.random();
            _this.setXY(valores.x, valores.y);
        },
        parar: function(_this) {
            clearInterval(_this.timerId);
        },
        iniciar: function() {
            this.timerId = setInterval(this.nuevoXY, 2000, this);
            setTimeout(this.parar, 1000 * 60, this);
        }
    };

    nombre.iniciar();

    document.body.addEventListener("keydown", function(evt) {
        switch (evt.which) {
            case 37:
            case 38:
            case 39:
            case 40:
                nombre.parar(nombre);
        };
        document.body.addEventListener("mousemove", function(evt) {
            nombre.setXY(evt.clientX-nombre.ancho, evt.clientY-nombre.alto);
        });
    });
});
