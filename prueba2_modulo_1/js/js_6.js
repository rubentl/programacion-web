window.addEventListener("load", function() {

    var pinpon = {
        ctx: document.querySelector("#pinpon").getContext("2d")
    };
    pinpon.mitadX = pinpon.ctx.canvas.width / 2;
    pinpon.mitadY = pinpon.ctx.canvas.height / 2;

    var tenis = {
        ctx: document.querySelector("#tenis").getContext("2d")
    };
    tenis.mitadX = tenis.ctx.canvas.width / 2;
    tenis.mitadY = tenis.ctx.canvas.height / 2;

    var diana = {
        ctx: document.querySelector("#diana").getContext("2d")
    };
    diana.mitadX = diana.ctx.canvas.width / 2;
    diana.mitadY = diana.ctx.canvas.height / 2;

    function circulo(obj) {
        obj.ctx.beginPath();
        obj.ctx.fillStyle = obj.color;
        obj.ctx.arc(obj.x, obj.y, obj.r, 0, 2 * Math.PI);
        obj.ctx.fill();
    };

    var exis = [10,80,150,220,290];
    var colors = ["#000", "#333", "#666", "#999", "#ccc"];
    for (var i = 0; i < 5; i++) {
        circulo({
            ctx: pinpon.ctx,
            x: exis[i],
            y: pinpon.mitadY,
            r: 8,
            color: colors[i]
        });
    };

    colors = ["#ccc", "#666", "#000","#666", "#ccc"]
    for (var j = 0; j < 5; j++){
        circulo({
            ctx: tenis.ctx,
            x: tenis.mitadX,
            y: 50 * (j+1),
            r: 25,
            color: colors[j]
        })
    };

    for (var k = 0; k < 3; k++){
        circulo({
            ctx: diana.ctx,
            x: diana.mitadX,
            y: diana.mitadY,
            r: 185 - (45 * (k+1)),
            color: "#000"
        });
        circulo({
            ctx: diana.ctx,
            x: diana.mitadX,
            y: diana.mitadY,
            r: 165 - (45 * (k+1)),
            color: "#fff"
        });
    };
});
