window.addEventListener("load", function(){

    var files = [
        "imgs/mafalda_1.jpg",
        "imgs/mafalda_2.jpg",
        "imgs/mafalda_3.jpg"
    ];

    function Imagenes(o){
        var imgs = new Array(o.length);
        var indice = 0;
        this.anterior = function(){
            indice = (indice == 0) ? 2 : indice -1;
            return imgs[indice].src;
        }
        this.siguiente = function(){
            indice = (indice == 2) ? 0 : indice +1;
            return imgs[indice].src;
        }
        o.forEach(function(val, idx){
            imgs[idx] = new Image();
            imgs[idx].src = val;
        });
    };

    var botAnterior = document.querySelector("#anterior");
    var botSiguiente = document.querySelector("#siguiente");
    var laImagen = document.querySelector("#laImagen");
    var img = new Imagenes(files);

    botAnterior.addEventListener("click", function(){
        laImagen.src = img.anterior();
    })
    botSiguiente.addEventListener("click", function(){
        laImagen.src = img.siguiente();
    })


})
